## Internet Banking

### Original task:
System **"Bank Payments"**.
**Customer** can have one or several banking **Accounts** (deposit, credit). Access to **Account** can be obtained after 
entering login and password. **Customer** can do bank transfers, pay bills, see general information (account balance, 
last operations, validity period). For **Credit Accounts** information is also available on the credit limit, current debt,
the amount of accrued interest, loan interest. For the **Deposit account** - deposit amount, interest rate, replenishment 
history. **Customer** can submit a request for opening a credit account if it is absent. **Administrator** confirms 
opening of the account, considering the size of deposit and validity period.


#

### Prerequisites
    1. JDK 1.8 or higher
    2. Apache Maven 3.3 or higher
    3. MySQL / MySQL-fork 5.7 or higher
    4. Apache Tomcat 8.0 or higher
        (in config must be set: deployOnStartup)

### Installation
    1. Download project from BitBucket
    2. Unpack .zip
    3. Create user in MySQL and grant rights to it - execute script `src/main/sql/mysql_user.sql`. You can specify 
    another name/password, then you need to specify them in 'src/main/resources/config/database.properties' or later in 
    `%WEB_APP%/WEB-INF/classes/config/database.properties`.
    4. Create database, script `src/main/sql/db_structure.sql`. 
    5. Fill database with init data, script `src/main/sql/init_admin.sql`.
    
    For tests:
       Repeate paragraphs 3-5 with sqripts from `src/test/sql`
      
    6. cd to project root directory and execute command mvn clean verify
    7. Copy artifact-file `i-banking.war` from `target` directory to `%TOMCAT%\webapps`

### Running
    1. Start Tomcat server by running the script `startup.{sh|bat}` in `%TOMCAT%\bin\`.
    2. After server start application will be available by URL http://localhost:{port}/
        ( {port} - port configured for Tomcat, default - 8080 )
    3. Database is empty, you need to fill it with data.
    4. For administrative access use credentials from file `init_admin.sql`.
    6. To stop server run script `shutdown.{sh|bat}` in `%TOMCAT%\bin\`.
