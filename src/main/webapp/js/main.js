function fillDatePickers (locale) {
    var validity = document.getElementById("approved_validity").value;

    $("input.datepicker").datepicker($.datepicker.regional[locale])
        .datepicker("option", "altFormat", "yy-mm-dd");
    $("#approved_validity_show").datepicker("option", "altField", "#approved_validity");
    if (validity === "") {
        $("#approved_validity_show").datepicker("setDate", new Date());
    } else {
        $("#approved_validity_show").datepicker("setDate", $.datepicker.parseDate( "yy-mm-dd", validity));
    }
};