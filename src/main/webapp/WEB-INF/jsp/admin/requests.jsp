<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="pageTitle" value="pages.admin.requests.title" scope="request"/>
<jsp:include page="/WEB-INF/jsp/includes/html_header.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_messages.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_errors.jsp"/>
<c:choose>
    <c:when test="${empty requests}">
        <p><fmt:message bundle="${views}" key="pages.admin.requests.noNewRequests"/></p>
    </c:when>
    <c:otherwise>
        <p><fmt:message bundle="${views}" key="pages.admin.requests.newRequestsList"/></p>
        <br>
        <table id="requests">
            <tr>
                <th class="theader"><fmt:message bundle="${views}" key="request.number"/></th>
                <th class="theader"><fmt:message bundle="${views}" key="request.creationDateTime"/></th>
                <th class="theader"><fmt:message bundle="${views}" key="request.status"/></th>
                <th class="theader"><fmt:message bundle="${views}" key="request.amount"/></th>
                <th class="theader"><fmt:message bundle="${views}" key="request.term"/></th>
            </tr>
            <c:forEach var="request" items="${requests}">
                <tr>
                    <td class="number"><a href="./requests?id=${request.id}">${request.id}</a></td>
                    <fmt:parseDate value="${request.creationDateTime}" pattern="yyyy-MM-dd'T'HH:mm:ss" var="creationDateTime" type="both" />
                    <td class="date"><fmt:formatDate value="${creationDateTime}" type="both"  dateStyle="short"/></td>
                    <td class="string"><fmt:message bundle="${views}" key="${request.status.statusString}"/></td>
                    <td class="number">${request.amount}</td>
                    <td class="string">${request.term.months}</td>
                </tr>
            </c:forEach>
        </table>
        <br>
    </c:otherwise>
</c:choose>
<jsp:include page="/WEB-INF/jsp/includes/html_footer.jsp" />