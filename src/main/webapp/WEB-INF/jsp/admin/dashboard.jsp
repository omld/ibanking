<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="pageTitle" value="pages.admin.dashboard.title" scope="request"/>
<jsp:include page="/WEB-INF/jsp/includes/html_header.jsp"/>
<c:if test="${not empty message}">
    <p class="message"><fmt:message bundle="${messages}" key="${message}"/></p>
</c:if>
<c:if test="${not empty error}">
    <p class="error"><fmt:message bundle="${errors}" key="${error}"/></p>
</c:if>
<p><fmt:message bundle="${views}" key="pages.admin.dashboard.welcome"/></p>
<jsp:include page="/WEB-INF/jsp/includes/html_footer.jsp" />