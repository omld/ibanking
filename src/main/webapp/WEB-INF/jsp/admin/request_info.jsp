<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="pageTitle" value="pages.admin.requests.info.title" scope="request"/>
<jsp:include page="/WEB-INF/jsp/includes/html_header.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_messages.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_errors.jsp"/>
<p><fmt:message bundle="${views}" key="pages.admin.requests.info.title"/></p>
<table id="request_info">
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="request.number"/></td>
        <td class="number">${requestInfo.id}</td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="request.creationDateTime"/></td>
        <fmt:parseDate value="${requestInfo.creationDateTime}" pattern="yyyy-MM-dd'T'HH:mm:ss" var="creationDateTime" type="both" />
        <td class="date"><fmt:formatDate value="${creationDateTime}" type="both"  dateStyle="short"/></td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="request.status"/></td>
        <td class="string"><fmt:message bundle="${views}" key="${requestInfo.status.statusString}"/></td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="request.amount"/></td>
        <td class="number">${requestInfo.amount}</td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="request.term"/></td>
        <td class="string">${requestInfo.term.months}</td>
    </tr>
</table>
<%--<br>--%>
<div id="client">
    <p><fmt:message bundle="${views}" key="pages.admin.requests.info.client"/></p>
    <table id="client_info">
        <tr>
            <td class="string"><fmt:message bundle="${views}" key="client.fullName"/></td>
            <td class="string">${requestInfo.client.fullName()}</td>
        </tr>
        <tr>
            <td class="string"><fmt:message bundle="${views}" key="client.taxNumber"/></td>
            <td class="string">${requestInfo.client.taxNumber}</td>
        </tr>
    </table>
    <p><fmt:message bundle="${views}" key="pages.admin.requests.info.clientAccounts"/></p>
    <table id="accounts_info">
        <tr>
            <th class="theader"><fmt:message bundle="${views}" key="accounts.number"/></th>
            <th class="theader"><fmt:message bundle="${views}" key="accounts.type"/></th>
            <th class="theader"><fmt:message bundle="${views}" key="accounts.balance"/></th>
            <th class="theader"><fmt:message bundle="${views}" key="accounts.validity"/></th>
            <th class="theader"><fmt:message bundle="${views}" key="accounts.interestRate"/></th>
        </tr>
        <c:forEach var="account" items="${requestInfo.client.bankAccounts}">
        <tr>
            <td class="string">${account.number}</td>
            <c:if test="${account['class'].simpleName == 'DepositAccount'}" >
                <td class="string"><fmt:message bundle="${views}" key="accounts.depositAccount"/></td>
            </c:if>
            <c:if test="${account['class'].simpleName == 'CreditAccount'}">
                <td class="string"><fmt:message bundle="${views}" key="accounts.creditAccount"/></td>
            </c:if>
            <td class="number">${account.balance}</td>
            <td class="date">${account.validityDate}</td>
            <td class="number">${account.interestRate}</td>
        </tr>
        </c:forEach>
    </table>
</div>
<br>
<div id="forms" class="flex-row" >
    <form id="approve_form" class="flex-column" action = "./request-process" method="POST">
        <fieldset>
            <div class="form-row">
                <div class="label">
                    <label><fmt:message bundle="${views}" key="request.approvedAmount"/></label>
                </div>
                <input type="text" id="approved_amount" name="approved_amount" placeholder="0.00" pattern="^\d+(?:\.\d{1,2})?$" value="${requestInfo.amount}" required <c:if test="${requestInfo.status != 'NEW'}">disabled</c:if> >
            </div>
            <div class="form-row">
                <div class="label">
                    <label><fmt:message bundle="${views}" key="request.approvedValidity"/></label>
                </div>
                <input type="text" class="datepicker" id="approved_validity_show" required  <c:if test="${requestInfo.status != 'NEW'}">disabled</c:if> >
                <input type="hidden" class="hidepicker" id="approved_validity" name="approved_validity" value=""  required >
            </div>
        </fieldset>
        <c:if test="${requestInfo.status == 'NEW'}">
            <div class="action">
                <input type="hidden" name="id" value="${requestInfo.id}">
                <input type="hidden" name="suffix" value="approve">
                <input type="submit" value="<fmt:message bundle="${views}" key="pages.admin.requests.button.approve"/>" >
            </div>
        </c:if>
    </form>
    <form id="reject_form" class="flex-column" action = "./request-process" method="POST">
    <c:if test="${requestInfo.status == 'NEW'}">
        <div class="action">
            <input type="hidden" name="id" value="${requestInfo.id}">
            <input type="hidden" name="suffix" value="reject">
            <input type="submit" value="<fmt:message bundle="${views}" key="pages.admin.requests.button.reject"/>">
        </div>
    </c:if>
    </form>
</div>
<script>
    $(document).ready(fillDatePickers("${locale}"));
</script>
<jsp:include page="/WEB-INF/jsp/includes/html_footer.jsp" />