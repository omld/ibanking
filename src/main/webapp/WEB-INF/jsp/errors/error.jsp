<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="pageTitle" value="pages.errors.error.title" scope="request"/>
<jsp:include page="/WEB-INF/jsp/includes/html_header.jsp"/>
    <h2><fmt:message bundle="${views}" key="pages.errors.error.header"/></h2>
    <fmt:message bundle="${views}" key="pages.errors.error.error"/>
    <br>
    <a href="${pageContext.request.contextPath}/"><fmt:message bundle="${views}" key="pages.errors.return"/></a>
<jsp:include page="/WEB-INF/jsp/includes/html_footer.jsp" />
