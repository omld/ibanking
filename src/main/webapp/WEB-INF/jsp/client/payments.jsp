<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="pageTitle" value="pages.client.payments.title" scope="request"/>
<jsp:include page="/WEB-INF/jsp/includes/html_header.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_messages.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_errors.jsp"/>
<c:if test="${empty payComplete}">
    <c:if test="${not empty accounts}">
    <form id="payment" class="flex-column" action = "./payments" method="POST">
        <p><fmt:message bundle="${views}" key="pages.client.payments.byDetails"/></p>
        <c:if test="${not empty invoice}">
            <c:set var="benefName" value="${invoice.beneficiaryName}" />
            <c:set var="benefCode" value="${invoice.beneficiaryCode}" />
            <c:set var="benefBank" value="${invoice.bankCode}" />
            <c:set var="benefAcc" value="${invoice.bankAccount}" />
            <c:set var="reference" value="${invoice.description}" />
            <c:set var="amount" value="${invoice.amount}" />
            <c:set var="fee" value="${fee}" />
        </c:if>
        <fieldset>
            <div class="form-row">
                <div class="label">
                    <label><fmt:message bundle="${views}" key="pages.client.payments.selectAccount"/></label>
                </div>
                <select class="accounts" name="account" id="account_id" required <c:if test="${not empty fee}">disabled</c:if>>
                <c:choose>
                    <c:when test="${empty fee}">
                        <c:forEach var="account" items="${accounts}">
                            <c:if test="${account['class'].simpleName == 'DepositAccount'}">
                                <fmt:message bundle="${views}" key="accounts.depositAccount" var="accType"/>
                            </c:if>
                            <c:if test="${account['class'].simpleName == 'CreditAccount'}">
                                <fmt:message bundle="${views}" key="accounts.creditAccount" var="accType"/>
                            </c:if>
                            <option value="${account.id}">${account.number} (${accType}): ${account.balance}</option>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${account['class'].simpleName == 'DepositAccount'}">
                            <fmt:message bundle="${views}" key="accounts.depositAccount" var="accType"/>
                        </c:if>
                        <c:if test="${account['class'].simpleName == 'CreditAccount'}">
                            <fmt:message bundle="${views}" key="accounts.creditAccount" var="accType"/>
                        </c:if>
                        <option value="${account.id}">${account.number} (${accType}): ${account.balance}</option>
                    </c:otherwise>
                </c:choose>
                </select>
            </div>
            <div class="form-row">
                <div class="label">
                    <label><fmt:message bundle="${views}" key="payment.beneficiary.name"/></label>
                </div>
                <input type="text" id="beneficiary_name" name="beneficiary_name" value="<c:out value="${benefName}"/>" required <c:if test="${not empty fee}">disabled</c:if> >
            </div>
            <div class="form-row">
                <div class="label">
                    <label><fmt:message bundle="${views}" key="payment.beneficiary.code"/></label>
                </div>
                <input type="text" id="beneficiary_code" name="beneficiary_code" pattern='[0-9]{8,10}' value="${benefCode}" required <c:if test="${not empty fee}">disabled</c:if> >
            </div>
            <div class="form-row">
                <div class="label">
                    <label><fmt:message bundle="${views}" key="payment.beneficiary.bankCode"/></label>
                </div>
                <input type="text" id="beneficiary_bank" name="beneficiary_bank" pattern='[0-9]{6}' value="${benefBank}" required <c:if test="${not empty fee}">disabled</c:if> >
            </div>
            <div class="form-row">
                <div class="label">
                    <label><fmt:message bundle="${views}" key="payment.beneficiary.account"/></label>
                </div>
                <input type="text" id="beneficiary_account" name="beneficiary_account" pattern='[0-9]{10,19}' value="${benefAcc}" required <c:if test="${not empty fee}">disabled</c:if> >
            </div>
            <div class="form-row">
                <div class="label">
                    <label><fmt:message bundle="${views}" key="payment.reference"/></label>
                </div>
                <textarea id="description" name="description" required <c:if test="${not empty fee}">disabled</c:if> >${reference}</textarea>
            </div>
            <div class="form-row">
                <div class="label">
                    <label><fmt:message bundle="${views}" key="payment.amount"/></label>
                </div>
                <input type="text" id="amount" name="amount" placeholder="0.00" pattern="^\d+(?:\.\d{1,2})?$" value="${amount}" required <c:if test="${not empty fee}">disabled</c:if> >
            </div>
        </fieldset>
        <c:if test="${not empty fee}">
            <div class="form-row fee">
                <div class="label">
                    <label><fmt:message bundle="${views}" key="payment.fee"/></label>
                </div>
                <input type="text" id="fee" name="fee" placeholder="0.00" pattern="^\d+(?:\.\d{1,2})?$" value="${fee}" disabled>
            </div>
        </c:if>
        <div class="action">
            <c:choose>
                <c:when test="${empty fee}" >
                    <input type="hidden" name="suffix" value="verify">
                    <input type="submit" value="<fmt:message bundle="${views}" key="pages.client.payments.button.continue"/>">
                </c:when>
                <c:otherwise>
                    <input type="hidden" name="suffix" value="pay">
                    <input type="submit" value="<fmt:message bundle="${views}" key="pages.client.payments.button.pay"/>">
                </c:otherwise>
            </c:choose>
        </div>
    </form>
    </c:if>
</c:if>
<jsp:include page="/WEB-INF/jsp/includes/html_footer.jsp" />