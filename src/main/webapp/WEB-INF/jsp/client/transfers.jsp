<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="pageTitle" value="pages.client.transfers.title" scope="request"/>
<jsp:include page="/WEB-INF/jsp/includes/html_header.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_messages.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_errors.jsp"/>
<c:if test="${empty transComplete}">
    <c:if test="${not empty accounts}">
    <form id="transfer_inner" class="flex-column" action = "./transfers" method="POST">
        <div class="form-row">
            <p><fmt:message bundle="${views}" key="pages.client.transfers.inner"/></p>
        </div>
        <fieldset>
            <div class="form-row">
                <div class="account-column" id="sender">
                    <label for="account_out"><fmt:message bundle="${views}" key="pages.client.transfers.selectAccountOut"/></label>
                    <br>
                    <select class="accounts" name="account_out" id="account_out" required <c:if test="${not empty fee}">disabled</c:if>>
                        <c:choose>
                        <c:when test="${empty fee}">
                            <c:forEach var="accOut" items="${accounts}">
                                <c:if test="${accOut['class'].simpleName == 'DepositAccount'}">
                                    <fmt:message bundle="${views}" key="accounts.depositAccount" var="accountOutType"/>
                                </c:if>
                                <c:if test="${accOut['class'].simpleName == 'CreditAccount'}">
                                    <fmt:message bundle="${views}" key="accounts.creditAccount" var="accountOutType"/>
                                </c:if>
                                <option value="${accOut.id}">${accOut.number} (${accountOutType}): ${accOut.balance}</option>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${accountOut['class'].simpleName == 'DepositAccount'}">
                                <fmt:message bundle="${views}" key="accounts.depositAccount" var="accountOutType"/>
                            </c:if>
                            <c:if test="${accountOut['class'].simpleName == 'CreditAccount'}">
                                <fmt:message bundle="${views}" key="accounts.creditAccount" var="accountOutType"/>
                            </c:if>
                            <option value="${accountOut.id}">${accountOut.number} (${accountOutType}): ${accountOut.balance}</option>
                        </c:otherwise>
                        </c:choose>
                    </select>
                </div>
                <div class="account-column" id="beneficiary">
                    <label for="account_in"><fmt:message bundle="${views}" key="pages.client.transfers.selectAccountIn"/></label>
                    <br>
                    <select class="accounts" name="account_in" id="account_in" required <c:if test="${not empty fee}">disabled</c:if>>
                        <c:choose>
                            <c:when test="${empty fee}">
                                <c:forEach var="accIn" items="${accounts}">
                                    <c:if test="${accIn['class'].simpleName == 'DepositAccount'}">
                                        <fmt:message bundle="${views}" key="accounts.depositAccount" var="accountInType"/>
                                    </c:if>
                                    <c:if test="${accIn['class'].simpleName == 'CreditAccount'}">
                                        <fmt:message bundle="${views}" key="accounts.creditAccount" var="accountInType"/>
                                    </c:if>
                                    <option value="${accIn.id}">${accIn.number} (${accountInType}): ${accIn.balance}</option>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${accountIn['class'].simpleName == 'DepositAccount'}">
                                    <fmt:message bundle="${views}" key="accounts.depositAccount" var="accountInType"/>
                                </c:if>
                                <c:if test="${accountIn['class'].simpleName == 'CreditAccount'}">
                                    <fmt:message bundle="${views}" key="accounts.creditAccount" var="accountInType"/>
                                </c:if>
                                <option value="${accountIn.id}">${accountIn.number} (${accountInType}): ${accountIn.balance}</option>
                            </c:otherwise>
                        </c:choose>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="label">
                    <label><fmt:message bundle="${views}" key="transfer.amount"/></label>
                </div>
                <input type="text" id="amount" name="amount" placeholder="0.00" pattern="^\d+(?:\.\d{1,2})?$" value="${amount}" required <c:if test="${not empty fee}">disabled</c:if> >
            </div>
        </fieldset>
        <c:if test="${not empty fee}">
            <div class="form-row">
                <div class="label">
                    <label><fmt:message bundle="${views}" key="transfer.fee"/></label>
                </div>
                <input type="text" id="fee" name="fee" placeholder="0.00" pattern="^\d+(?:\.\d{1,2})?$" value="${fee}" disabled>
            </div>
        </c:if>
        <div class="action">
            <c:choose>
                <c:when test="${empty fee}" >
                    <input type="hidden" name="suffix" value="verify">
                    <input type="submit" value="<fmt:message bundle="${views}" key="pages.client.transfers.button.continue"/>">
                </c:when>
                <c:otherwise>
                    <input type="hidden" name="suffix" value="process">
                    <input type="submit" value="<fmt:message bundle="${views}" key="pages.client.transfers.button.transfer"/>">
                </c:otherwise>
            </c:choose>
        </div>
    </form>
    </c:if>
</c:if>
<jsp:include page="/WEB-INF/jsp/includes/html_footer.jsp" />