<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<table id="transactions">
    <tr>
        <th class="theader"><fmt:message bundle="${views}" key="transaction.number"/></th>
        <th class="theader"><fmt:message bundle="${views}" key="pages.client.transaction.date"/></th>
        <th class="theader"><fmt:message bundle="${views}" key="transaction.beneficery"/></th>
        <th class="theader"><fmt:message bundle="${views}" key="transaction.status"/></th>
        <th class="theader"><fmt:message bundle="${views}" key="transaction.amount"/></th>
    </tr>
<c:forEach var="transaction" items="${account.transactions}">
    <tr>
        <td class="number"><a href="./transaction?id=${transaction.id}&account=${account.id}">${transaction.id}</a></td>
        <fmt:parseDate value="${transaction.creationDateTime}" pattern="yyyy-MM-dd'T'HH:mm:ss" var="creationDateTime" type="both" />
        <td class="date"><fmt:formatDate value="${creationDateTime}" type="both"  dateStyle="short"/></td>
        <td class="string">${transaction.beneficiaryName}</td>
        <td class="string">
            <fmt:message bundle="${views}" key="${transaction.status.statusString}"/>
        </td>
        <td class="number">${transaction.amount}</td>
    </tr>
</c:forEach>
</table>