<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="pageTitle" value="pages.client.transaction.title" scope="request"/>
<jsp:include page="/WEB-INF/jsp/includes/html_header.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_errors.jsp"/>
<p><a href="./accounts?id=${param.account}"><fmt:message bundle="${views}" key="pages.client.transaction.return"/></a></p>
<br>
<p><fmt:message bundle="${views}" key="pages.client.transaction.title"/></p>
<br>
<table id="transaction_info">
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="transaction.number"/></td>
        <td class="number">${transaction.id}</td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="transaction.creationTime"/></td>
        <fmt:parseDate value="${transaction.creationDateTime}" pattern="yyyy-MM-dd'T'HH:mm:ss" var="creationDateTime" type="both" />
        <td class="date"><fmt:formatDate value="${creationDateTime}" type="both"  dateStyle="short"/></td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="transaction.executionTime"/></td>
        <fmt:parseDate value="${transaction.executionDateTime}" pattern="yyyy-MM-dd'T'HH:mm:ss" var="executionDateTime" type="both" />
        <td class="date"><fmt:formatDate value="${executionDateTime}" type="both"  dateStyle="short"/></td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="transaction.sender"/></td>
        <td class="string">${transaction.senderName}</td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="transaction.beneficery"/></td>
        <td class="string">${transaction.beneficiaryName}</td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="transaction.status"/></td>
        <td class="string"><fmt:message bundle="${views}" key="${transaction.status.statusString}"/></td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="transaction.amount"/></td>
        <td class="number">${transaction.amount}</td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="transaction.fee"/></td>
        <td class="number">${transaction.fee}</td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="transaction.description"/></td>
        <td class="string">${transaction.description}</td>
    </tr>
</table>
<br>
<jsp:include page="/WEB-INF/jsp/includes/html_footer.jsp" />