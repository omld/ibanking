<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="pageTitle" value="pages.client.requests.title" scope="request"/>
<jsp:include page="/WEB-INF/jsp/includes/html_header.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_messages.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_errors.jsp"/>
<c:choose>
    <c:when test="${(not empty creditPresent) && (creditPresent == true)}">
        <p><fmt:message bundle="${views}" key="pages.client.requests.creditPresent"/></p>
    </c:when>
    <c:otherwise>
        <p><fmt:message bundle="${views}" key="pages.client.requests.requestForm"/></p>
        <form id="payment" class="flex-column" action = "./requests" method="POST">
            <fieldset>
                <div class="form-row">
                    <div class="label">
                        <label><fmt:message bundle="${views}" key="request.amount"/></label>
                    </div>
                    <input type="text" id="amount" name="amount" required >
                </div>
                <div class="form-row">
                    <div class="label">
                        <label><fmt:message bundle="${views}" key="request.term"/></label>
                    </div>
                    <input type="number" id="term" name="term" min="0" step="1" value="24" required >
                </div>
            </fieldset>
            <div class="action">
                <input type="submit" value="<fmt:message bundle="${views}" key="pages.client.requests.button.submit"/>">
            </div>
        </form>
    </c:otherwise>
</c:choose>
<hr>
<c:choose>
    <c:when test="${empty requests}">
        <p><fmt:message bundle="${views}" key="pages.client.requests.noRequests"/></p>
    </c:when>
    <c:otherwise>
        <p><fmt:message bundle="${views}" key="pages.client.requests.requestsList"/></p>
        <br>
        <table id="requests">
            <tr>
                <th class="theader"><fmt:message bundle="${views}" key="request.creationDateTime"/></th>
                <th class="theader"><fmt:message bundle="${views}" key="request.status"/></th>
                <th class="theader"><fmt:message bundle="${views}" key="request.amount"/></th>
                <th class="theader"><fmt:message bundle="${views}" key="request.term"/></th>
                <th class="theader"><fmt:message bundle="${views}" key="request.approvedAmount"/></th>
                <th class="theader"><fmt:message bundle="${views}" key="request.approvedValidity"/></th>
            </tr>
            <c:forEach var="request" items="${requests}">
                <tr>
                    <fmt:parseDate value="${request.creationDateTime}" pattern="yyyy-MM-dd'T'HH:mm:ss" var="creationDateTime" type="both" />
                    <td class="date"><fmt:formatDate value="${creationDateTime}" type="both"  dateStyle="medium"/></td>
                    <td class="string"><fmt:message bundle="${views}" key="${request.status.statusString}"/></td>
                    <td class="number">${request.amount}</td>
                    <td class="string">${request.term.months}</td>
                    <td class="number">${request.approvedAmount}</td>
                    <fmt:parseDate value="${request.approvedValidity}" pattern="yyyy-MM-dd" var="approvedValidity" />
                    <td class="date"><fmt:formatDate value="${approvedValidity}" type="date"  dateStyle="medium"/></td>
                </tr>
            </c:forEach>
        </table>
        <br>
    </c:otherwise>
</c:choose>
<jsp:include page="/WEB-INF/jsp/includes/html_footer.jsp" />