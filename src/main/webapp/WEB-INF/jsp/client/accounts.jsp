<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="pageTitle" value="pages.client.accounts.title" scope="request"/>
<jsp:include page="/WEB-INF/jsp/includes/html_header.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_messages.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_errors.jsp"/>
<c:if test="${not empty accounts}">
    <p><fmt:message bundle="${views}" key="pages.client.accounts.accountsList"/></p>
    <br>
    <table id="accounts">
        <tr>
            <th class="theader"><fmt:message bundle="${views}" key="accounts.number"/></th>
            <th class="theader"><fmt:message bundle="${views}" key="accounts.balance"/></th>
            <th class="theader"><fmt:message bundle="${views}" key="accounts.type"/></th>
        </tr>
    <c:forEach var="account" items="${accounts}">
        <tr>
            <td class="string"><a href="?id=${account.id}">${account.number}</a></td>
            <td class="number">${account.balance}</td>
            <c:if test="${account['class'].simpleName == 'DepositAccount'}">
                <td class="string"><fmt:message bundle="${views}" key="accounts.depositAccount"/></td>
            </c:if>
            <c:if test="${account['class'].simpleName == 'CreditAccount'}">
                <td class="string"><fmt:message bundle="${views}" key="accounts.creditAccount"/></td>
            </c:if>
        </tr>
    </c:forEach>
    </table>
    <br>
</c:if>
<jsp:include page="/WEB-INF/jsp/includes/html_footer.jsp" />