<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="pageTitle" value="pages.client.accounts.deposit.title" scope="request"/>
<jsp:include page="/WEB-INF/jsp/includes/html_header.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_errors.jsp"/>
<p><fmt:message bundle="${views}" key="pages.client.accounts.deposit.title"/></p>
<br>
<table id="account_info">
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="accounts.number"/></td>
        <td class="string">${account.number}</td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="accounts.balance"/></td>
        <td class="number">${account.balance}</td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="accounts.validity"/></td>
        <td class="date">${account.validityDate}</td>
    </tr>
    <tr>
        <td class="string"><fmt:message bundle="${views}" key="accounts.interestRate"/></td>
        <td class="number">${account.interestRate}</td>
    </tr>
</table>
<br>

<p><fmt:message bundle="${views}" key="pages.client.accounts.replenishHistory"/></p>
<c:if test="${not empty account.transactions}">
    <jsp:include page="transactions_block.jsp" />
</c:if>
<br>
<jsp:include page="/WEB-INF/jsp/includes/html_footer.jsp" />