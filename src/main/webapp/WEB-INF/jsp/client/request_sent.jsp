<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="pageTitle" value="pages.client.requestSent.title" scope="request"/>
<jsp:include page="/WEB-INF/jsp/includes/html_header.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_errors.jsp"/>
<c:if test="${not empty requestNumber}">
    <p><fmt:message bundle="${views}" key="pages.client.requestSent.created"/>${requestNumber}</p>
</c:if>
<jsp:include page="/WEB-INF/jsp/includes/html_footer.jsp" />