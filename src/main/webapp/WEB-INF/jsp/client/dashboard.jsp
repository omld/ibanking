<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="pageTitle" value="pages.client.dashboard.title" scope="request"/>
<jsp:include page="/WEB-INF/jsp/includes/html_header.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_messages.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_errors.jsp"/>
<p><fmt:message bundle="${views}" key="pages.client.dashboard.dear"/> ${user.client.fullName()} !</p>
<p><fmt:message bundle="${views}" key="pages.client.dashboard.welcome"/></p>
<jsp:include page="/WEB-INF/jsp/includes/html_footer.jsp" />