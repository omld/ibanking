<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="pageTitle" value="pages.login.title" scope="request"/>
<jsp:include page="/WEB-INF/jsp/includes/html_header.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_messages.jsp"/>
<jsp:include page="/WEB-INF/jsp/includes/_errors.jsp"/>
<div class='signform'>
    <h2 class="hreg"><fmt:message bundle="${views}" key="pages.login.form.title"/></h2>
    <form id='logf' action='${contextpath}/login' method='post'>
        <div class='field-wrap'>
            <label><fmt:message bundle="${views}" key="pages.login.form.email"/></label>
            <input class="sign" id="uemail" name='u_email' type='email' pattern="(([A-Za-z][A-Za-z\d\-_]{0,25})|([A-Za-z][A-Za-z\d\-_]{0,20}[\.][A-Za-z\d\-_]{1,20}))@([A-Za-z]{1,30}((([A-Za-z\d\-]{0,30}[A-Za-z\d]{0,30})|([A-Za-z\d]{0,30})[\.]){0,30}[A-Za-z]{2,15}))" required autocomplete='off'/>
        </div>
        <div class='field-wrap'>
            <label><fmt:message bundle="${views}" key="pages.login.form.password"/></label>
            <input class="sign" id="upass" name='u_pass' type='password' required autocomplete='off'/>
        </div>
        <button class='button button-block'><fmt:message bundle="${views}" key="pages.login.form.button.login"/></button>
    </form>
</div> <%-- /signform --%>
<jsp:include page="/WEB-INF/jsp/includes/html_footer.jsp" />