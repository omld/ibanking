<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${not empty messagesList}">
    <p>
    <c:forEach var="message" items="${messagesList}">
        <fmt:message bundle="${messages}" key="${message}"/><br>
    </c:forEach>
    </p>
</c:if>