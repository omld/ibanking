<%@ page contentType="text/html;charset=UTF-8" language="java"  pageEncoding="UTF-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="/WEB-INF/jsp/includes/_i18n.jsp"/>
<c:set var="contextpath" value="${pageContext.request.contextPath}" scope="request" />
<c:set var="copyyear" value="2018" scope="request" />
<fmt:message bundle="${views}" key="${pageTitle}" var="pageTitle" scope="request"/>
<fmt:message bundle="${views}" key="app.title" var="appTitle" scope="request" />
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>${pageTitle} | ${appTitle}</title>
    <link rel="stylesheet" type="text/css" href="${contextpath}/css/styles.css">
    <link rel="stylesheet" href="${contextpath}/css/jquery-ui.min.css">
    <script src="${contextpath}/js/jquery.js"></script>
    <script src="${contextpath}/js/jquery-ui.min.js"></script>
    <script src="${contextpath}/js/main.js"></script>
    <script src="${contextpath}/js/i18n/datepicker-en.js"></script>
    <script src="${contextpath}/js/i18n/datepicker-uk.js"></script>
</head>
<body>
<div id="main-container" class="flex-column">
    <jsp:include page="/WEB-INF/jsp/includes/header.jsp" />
    <jsp:include page="/WEB-INF/jsp/includes/menu.jsp" />
    <div id="main-content">