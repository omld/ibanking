<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div id="footer" class="flex-column">
    <hr width="100%">
    <p class="copy"><fmt:message bundle="${views}" key="app.copy"><fmt:param>${copyyear}</fmt:param></fmt:message></p>
</div>