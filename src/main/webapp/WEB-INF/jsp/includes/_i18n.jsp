<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="defaultLocale" value="${empty initParam.defaultLocale ? 'en-US' : initParam.defaultLocale}" scope="request"/>
<c:set var="locale" value="${empty sessionScope.locale ? defaultLocale : sessionScope.locale }" scope="request"/>
<fmt:setLocale value="${locale}" scope="request"/>
<fmt:setBundle basename="localization/views" var="views" scope="request"/>
<fmt:setBundle basename="localization/errors" var="errors" scope="request"/>
<fmt:setBundle basename="localization/messages" var="messages" scope="request"/>