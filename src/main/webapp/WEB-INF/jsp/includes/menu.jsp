<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div id="menu">
    <hr>
    <c:if test="${not empty user}">
    <div id="main-menu" class="menu flex-row">
        <div>
            <c:if test="${user.role == 'ADMIN'}">
                <a class="menu-item" href="${contextpath}/.."><fmt:message bundle="${views}" key="menu.main"/></a>
                | <a class="menu-item" href="./requests"><fmt:message bundle="${views}" key="menu.admin.requests"/></a>
            </c:if>
            <c:if test="${user.role == 'CLIENT'}">
                <a class="menu-item" href="${contextpath}/.."><fmt:message bundle="${views}" key="menu.main"/></a>
                | <a class="menu-item" href="./accounts"><fmt:message bundle="${views}" key="menu.client.accounts"/></a>
                | <a class="menu-item" href="./payments"><fmt:message bundle="${views}" key="menu.client.payments"/></a>
                | <a class="menu-item" href="./transfers"><fmt:message bundle="${views}" key="menu.client.transfers"/></a>
                | <a class="menu-item" href="./requests"><fmt:message bundle="${views}" key="menu.client.requests"/></a>
            </c:if>
        </div>
        <div id="login">
            <a href="${contextpath}/logout"><fmt:message bundle="${views}" key="menu.logout"/></a>
        </div>
    </div>
    <hr>
    </c:if>
</div>