<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${not empty errorsList}">
    <p class="error">
        <c:forEach var="error" items="${errorsList}">
            <fmt:message bundle="${errors}" key="${error}"/><br>
        </c:forEach>
    </p>
</c:if>