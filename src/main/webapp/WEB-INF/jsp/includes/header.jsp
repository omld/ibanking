<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div id="header" class="flex-column">
    <div id="title"><h1>${appTitle}</h1></div>
    <div id="lang_chooser">
        <c:forTokens items="${initParam.locales}" delims=";" var='lang'>
            <c:set var="act" value="${lang.split(\"=\")[1] == locale}" scope="page"/>
            <a <c:if test="${act}">class="active-lang"</c:if> href="${contextpath}/langChange?locale=${lang.split("=")[1]}" >${lang.split("=")[0]}</a>
        </c:forTokens>
    </div>
</div>