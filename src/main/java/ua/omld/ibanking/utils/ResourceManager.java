/*
 * ResourceManager.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Resource manager for internationalization
 */
public class ResourceManager {

    private static final Logger LOGGER = LogManager.getLogger();

    private static final String MESSAGES = "localization.messages";
    private static final String ERRORS = "localization.errors";
    private static final String VIEWS = "localization.views";

    private final static Map<String, ConcurrentHashMap<Locale, ResourceBundle> > resourceBundleMap
            = new ConcurrentHashMap<>();

    private ResourceManager() { }

    synchronized private static void insertBundle(String bundle, Locale locale) {

        if (!resourceBundleMap.containsKey(bundle)) {
            try {
                ConcurrentHashMap<Locale, ResourceBundle> localeResourceBundleMap = new ConcurrentHashMap<>();
                localeResourceBundleMap.put(locale, ResourceBundle.getBundle(bundle, locale));
                resourceBundleMap.putIfAbsent(bundle, localeResourceBundleMap);
            } catch (MissingResourceException mrEx) {
                LOGGER.error("Can`t find resource bundle: " + mrEx);
            }
        } else {
            ConcurrentHashMap<Locale, ResourceBundle> localeResourceBundleMap = resourceBundleMap.get(bundle);
            if (!localeResourceBundleMap.containsKey(locale) )
                localeResourceBundleMap.putIfAbsent(locale, ResourceBundle.getBundle(bundle, locale));
        }
    }

    /**
     * Returns localized version of message
     *
     * @param key message key
     * @param locale needed locale
     * @return localized version of message
     */
    public static String getMessage(String key, Locale locale) {
        return getResource(MESSAGES, key, locale);
    }

    /**
     * Returns localized version of error
     *
     * @param key error key
     * @param locale needed locale
     * @return localized version of error
     */
    public static String getError(String key, Locale locale) {
        return getResource(ERRORS, key, locale);
    }

    /**
     * Returns localized version of string
     *
     * @param key string key
     * @param locale needed locale
     * @return localized version of string
     */
    public static String getString(String key, Locale locale) {
        return getResource(VIEWS, key, locale);
    }

    private static String getResource(String bundle, String key, Locale locale) {

        if (!resourceBundleMap.containsKey(bundle) || !resourceBundleMap.get(bundle).containsKey(locale)) {
            insertBundle(bundle, locale);
        }
        if (resourceBundleMap.get(bundle).containsKey(locale)) {
            return resourceBundleMap.get(bundle).get(locale).getString(key);
        }
        return null;
    }
}