/*
 * ConfigurationManager.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Reads configuration from properties and returns needed data.
 */
public class ConfigurationManager {

    private static final Logger LOGGER = LogManager.getLogger();

    private static final String CONF_DIR = "config";
    private static final int DEFAULT_LAST_TRANSACTIONS = 10;

    private static Map<String, ResourceBundle> configurationsMap = new ConcurrentHashMap<>();

    private ConfigurationManager() {
        loadProperties();
    }

    private void loadProperties() {
        insertProperties("application");
        insertProperties("database");
    }

    synchronized private void insertProperties(String bundle) {
        String bundleFullName = CONF_DIR + "." + bundle;
        if (!configurationsMap.containsKey(bundle)) {
            try {
                ResourceBundle propertiesBundle = ResourceBundle.getBundle(bundleFullName);
                configurationsMap.putIfAbsent(bundle, propertiesBundle);
            } catch (MissingResourceException mrEx) {
                LOGGER.error("Can`t find properties file: " + mrEx);
            }
        }
    }

    private static class InstanceHolder {
        private static ConfigurationManager INSTANCE = new ConfigurationManager();
    }

    public static ConfigurationManager getInstance(){
        return InstanceHolder.INSTANCE;
    }

    private String getProperty(String bundle, String key) {
        try {
            if (!configurationsMap.containsKey(bundle)) {
                loadProperties();
            }
            return configurationsMap.get(bundle).getString(key);
        } catch (MissingResourceException mrEx) {
            LOGGER.error("Resource not found: " + mrEx.getLocalizedMessage() );
            return null;
        }
    }

    /**
     * Returns default user role configuration parameter.
     *
     * @return default user role
     */
    public String getBankCode() {
        return getProperty("application", "bankCode");
    }

    /**
     * Returns storage type configuration parameter.
     *
     * @return storage type
     */
    public String getStorageType() {
        return getProperty("application", "storage");
    }

    public int getLastTransactionsCount() {
        int key = DEFAULT_LAST_TRANSACTIONS;
        try {
            String keyValueString = getProperty("application", "lastTransactionsCount");
            key = Integer.valueOf(keyValueString);
        } catch (NumberFormatException nfEx) {
            LOGGER.error("Error read lastTransactionsCount.");
        }
        return key;
    }

    /**
     * Returns database connection url configuration parameter.
     *
     * @return database connection url
     */
    public String getDBUrl() {
        return getProperty("database", "url");
    }

    /**
     * Returns database user configuration parameter.
     *
     * @return database user
     */
    public String getDBUser() {
        return getProperty("database", "user");
    }

    /**
     * Returns database user password configuration parameter.
     *
     * @return database user password
     */
    public String getDBPassword() {
        return getProperty("database", "password");
    }

    /**
     * Returns database driver configuration parameter.
     *
     * @return database driver
     */
    public String getDBDriver() {
        return getProperty("database", "driver");
    }


}