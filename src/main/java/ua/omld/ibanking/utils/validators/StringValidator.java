/*
 * StringValidator.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.utils.validators;

import java.util.regex.Pattern;

/**
 * Checks string to match given pattern
 */
public class StringValidator {

    private Pattern pattern;

    public StringValidator(String patternStr) {
            setPattern(patternStr);
    }

    /**
     * Validate given string to patten.
     *
     * @param string string to check
     * @return true if string match pattern
     */
    public boolean validate(String string) {
        return string != null && pattern.matcher(string).matches();
    }

    protected boolean validatePartial(String string) {
        return string != null && pattern.matcher(string).find();
    }

    protected void setPattern(String patternStr) {
        if (patternStr == null) {
            throw new IllegalArgumentException("Pattern not initialized.");
        }
        this.pattern = Pattern.compile(patternStr);
    }
}
