/*
 * Patterns.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.utils.validators;

/**
 * Patterns for check user input
 */
public interface Patterns {

    String EMAIL = "(([A-Za-z][A-Za-z\\d\\-_]{0,25})|([A-Za-z][A-Za-z\\d\\-_]{0,20}[\\.][A-Za-z\\d\\-_]{1,20}))@([A-Za-z]{1,30}((([A-Za-z\\d\\-]{0,30}[A-Za-z\\d]{0,30})|([A-Za-z\\d]{0,30})[\\.]){0,30}[A-Za-z]{2,15}))";

    // whole password pattern
    String PASSWORD = "[\\w@#$:;,.?!*&\\-_+=]{6,20}";

    // patterns for bank operations
    String BANK_CODE = "[\\d]{6,6}";
    String BANK_ACCOUNT_NUMBER = "[\\d]{10,19}"; // inner bank account number
    String PERSON_CODE = "[\\d]{8,8}|[\\d]{10,10}"; // code for any (physical, juridical) code
    String PERSON_NAME = "[\\w\"\' А-Яа-яІіЇї]{3,100}";
    String INVOICE_DESCRIPTION = "[\\w\"\' А-Яа-яІіЇї@#$%:;,.?!*&\\-_+=()]{3,150}";

    String PERSON_TAX_NUMBER = "[\\d]{10,10}"; // physical person tax number
}
