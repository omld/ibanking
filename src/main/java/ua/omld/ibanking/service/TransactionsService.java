/*
 * TransactionsService.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service;

import ua.omld.ibanking.domain.*;
import ua.omld.ibanking.service.exception.SystemFaultException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

/**
 * Service interface to process transactions like bank operations
 */
public interface TransactionsService {

    Transaction getTransactionById(Long id, Long accountId) throws SystemFaultException;

    List<Transaction> getTransactionsByAccountForPeriod(BankAccount account, LocalDate from, LocalDate to) throws SystemFaultException;

    /**
     * Process actions for invoice payment
     * @param invoice invoice
     * @param account bank account to process payment with
     * @return true on success
     * @throws SystemFaultException if
     */
    boolean payInvoice(Invoice invoice, BankAccount account) throws SystemFaultException;

    /**
     * Checks given invoice information and return fee for according transaction.
     * @param invoice invoice to check
     * @param account bank from which invoice will be paid
     * @return fee for transaction
     */
    BigDecimal checkInvoiceAndGetFee(Invoice invoice, BankAccount account);

    /**
     * Checks if transfer can be processed and return fee for according transaction.
     * @param outcomeAccount bank account for withdrawal
     * @param incomeAccount bank account for replenish
     * @param amount amount of transfer
     * @return fee for transaction
     */
    BigDecimal checkTransferAndGetFee(BankAccount outcomeAccount, BankAccount incomeAccount, BigDecimal amount);

    /**
     * Process money transfer between Client's accounts
     * @param outcomeAccount outcome account
     * @param incomeAccount income account
     * @param amount amount of transfer
     * @return true on success
     */
    boolean transferMoney(BankAccount outcomeAccount, BankAccount incomeAccount, BigDecimal amount, Locale locale) throws SystemFaultException;

    /**
     * Process money transfer to foreign account
     * @param outcomeAccount client account
     * @param beneficiary beneficiary of transfer
     * @param amount amount of transfer
     * @return true on success
     */
    boolean transferMoneyToForeign(BankAccount outcomeAccount, TransactionPerson beneficiary, BigDecimal amount, Locale locale)throws SystemFaultException;
}
