/*
 * AccountRequestService.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service;

import ua.omld.ibanking.domain.Client;
import ua.omld.ibanking.domain.CreditAccountRequest;
import ua.omld.ibanking.service.exception.SystemFaultException;

import java.math.BigDecimal;
import java.time.Period;
import java.util.List;

public interface AccountRequestService {

    CreditAccountRequest makeRequestForCreditAccount(Client client, BigDecimal amount, Period term) throws SystemFaultException;

    CreditAccountRequest getRequestInfo(Long id) throws SystemFaultException;

    List<CreditAccountRequest> getNewRequests() throws SystemFaultException;

    List<CreditAccountRequest> getAllRequests() throws SystemFaultException;

    List<CreditAccountRequest> getAllRequestsForClient(Long clientId) throws SystemFaultException;

    void approveRequestAndCreateCreditAccount(CreditAccountRequest request) throws SystemFaultException;

    void rejectRequestForCreditAccount(CreditAccountRequest request);
}
