/*
 * BankAccountServiceImpl.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.domain.*;
import ua.omld.ibanking.service.BankAccountService;
import ua.omld.ibanking.service.exception.BankAccountNotFoundException;
import ua.omld.ibanking.storage.connection.ConnectionWrapper;
import ua.omld.ibanking.storage.dao.*;
import ua.omld.ibanking.storage.exception.DAOException;

import java.util.List;

public class BankAccountServiceImpl extends ApplicationService implements BankAccountService {

    private static final Logger LOGGER = LogManager.getLogger();

    private BankAccountServiceImpl() {
    }

    BankAccountServiceImpl(DAOFactory daoFactory) {
        super(daoFactory);
    }

    private static class InstanceHolder {

        private static BankAccountServiceImpl INSTANCE = new BankAccountServiceImpl();
    }

    public static BankAccountServiceImpl getInstance(){
        return InstanceHolder.INSTANCE;
    }

    @Override
    public List<BankAccount> findAccountsByClient(Client client) {

        try (BankAccountDAO bankAccountDAO = getDAOFactory().createBankAccountDAO()) {
            LOGGER.trace(bankAccountDAO);
            List<BankAccount> foundAccounts = bankAccountDAO.findBankAccountsByClientId(client.getId());

            if (foundAccounts.isEmpty()) {
                throw new BankAccountNotFoundException();
            }
            return foundAccounts;
        } catch (DAOException | NullPointerException ex) {
            LOGGER.error(ex.getMessage());
            throw new BankAccountNotFoundException();
        }
    }

    @Override
    public BankAccount getBankAccountInfoById(Long id) {

        try (ConnectionWrapper connectionWrapper = getDAOFactory().getConnection()) {
            BankAccountDAO bankAccountDAO = getDAOFactory().createBankAccountDAO(connectionWrapper);
            BankAccount foundBankAccount = bankAccountDAO.findById(id);
            fillBankAccountWithLastTransactions(foundBankAccount, connectionWrapper);

            return foundBankAccount;
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new BankAccountNotFoundException();
        }
    }

    private BankAccount fillBankAccountWithLastTransactions(BankAccount bankAccount, ConnectionWrapper connectionWrapper)
            throws DAOException {

        ClientDAO clientDAO = getDAOFactory().createClientDAO(connectionWrapper);
        TransactionDAO transactionDAO = getDAOFactory().createTransactionDAO(connectionWrapper);
        Client client = clientDAO.findByBankAccountId(bankAccount.getId());
        List<Transaction> lastTransactions = transactionDAO.getLastTransactionsByAccountId(bankAccount.getId(),
                getConfigManager().getLastTransactionsCount());
        lastTransactions.forEach(tr -> {
            if ( determineTransactionDirection(tr, client.getTaxNumber(), bankAccount.getNumber())
                    == TransactionDirection.OUTPUT ) {
                tr.setAmount(tr.getAmount().negate());
            }
        });
        bankAccount.setTransactions(lastTransactions);
        return bankAccount;
    }


    /**
     * Determines direction of bank transaction according to given person and account number.
     *
     * @param transaction checked transaction
     * @param personCode person's code for collation
     * @param bankAccountNo number of bank account for collation
     * @return direction of bank transaction
     */
    private TransactionDirection determineTransactionDirection(Transaction transaction, String personCode,
                                                              String bankAccountNo) {

        TransactionDirection result;
        if ( transaction.getSender().getPersonCode().equals(personCode)
                && transaction.getSender().getBankAccountNumber().equals(bankAccountNo) ) {
            result = TransactionDirection.OUTPUT;
        } else {
            result = TransactionDirection.INPUT;
        }
        return result;
    }
}
