/*
 * TransactionsServiceImpl.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service.impl;

import ua.omld.ibanking.domain.*;
import ua.omld.ibanking.service.exception.TransactionException;
import ua.omld.ibanking.service.TransactionsService;
import ua.omld.ibanking.service.exception.IllegalPeriodException;
import ua.omld.ibanking.service.exception.SystemFaultException;
import ua.omld.ibanking.storage.connection.ConnectionWrapper;
import ua.omld.ibanking.storage.dao.*;
import ua.omld.ibanking.storage.exception.DAOException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.utils.ResourceManager;

public class TransactionsServiceImpl extends ApplicationService implements TransactionsService {

    private static final long MAX_MONTHS_FOR_TRANSACTIONS_LIST = 3;
    private static final BigDecimal INNER_FEE = new BigDecimal(0.0);
    private static final BigDecimal OUTER_FEE = new BigDecimal(5.0);

    private static final String TRANSFER_INNER_DESCRIPTION = "transaction.description.transferInner";
    private static final String TRANSFER_OUTER_DESCRIPTION = "transaction.description.transferOuter";

    private static final Logger LOGGER = LogManager.getLogger();

    private TransactionsServiceImpl() {
    }

    TransactionsServiceImpl(DAOFactory daoFactory) {
        super(daoFactory);
    }

    private static class InstanceHolder {
        private static TransactionsServiceImpl INSTANCE = new TransactionsServiceImpl();
    }

    public static TransactionsServiceImpl getInstance(){
        return InstanceHolder.INSTANCE;
    }

    @Override
    public Transaction getTransactionById(Long id, Long accountId) throws SystemFaultException {

        try (TransactionDAO transactionDAO = getDAOFactory().createTransactionDAO()) {
            Transaction transaction = transactionDAO.findById(id);
            return transaction;
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new SystemFaultException(daoEx.getMessage());
        }
    }

    @Override
    public List<Transaction> getTransactionsByAccountForPeriod(BankAccount account, LocalDate fromDate, LocalDate toDate)
            throws SystemFaultException {

        if (checkPeriodIllegal(fromDate, toDate)) {
            throw new IllegalPeriodException();
        }

        try (TransactionDAO transactionDAO = getDAOFactory().createTransactionDAO()) {
            List<Transaction> transactions = transactionDAO.getTransactionsByAccountIdForPeriod(account.getId(), fromDate, toDate);
            return transactions;
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new SystemFaultException(daoEx.getMessage());
        }
    }

    private boolean checkPeriodIllegal(LocalDate fromDate, LocalDate toDate) {

        return (toDate.isAfter(LocalDate.now()) || fromDate.isAfter(LocalDate.now()))
                || fromDate.plusMonths(MAX_MONTHS_FOR_TRANSACTIONS_LIST).isBefore(toDate);
    }

    @Override
    public boolean payInvoice(Invoice invoice, BankAccount account)
            throws SystemFaultException {

        try (ConnectionWrapper connectionWrapper = getDAOFactory().getConnection()) {
            connectionWrapper.startTransaction();
            BankAccount storedBankAccount = getStoredBankAccount(account, connectionWrapper);
            BigDecimal totalAmount = invoice.getAmount();
            totalAmount = totalAmount.add(determineFee(invoice.getBeneficiary(), invoice.getAmount()));
            if ( storedBankAccount.isAmountValidForWithdrawal(totalAmount) ) {
                Transaction transaction = buildTransactionForInvoice(invoice, storedBankAccount, connectionWrapper);
                if (processOutcomeTransaction(storedBankAccount, transaction, connectionWrapper)) {
                    connectionWrapper.commitTransaction();
                    return true;
                }
            }
            connectionWrapper.rollbackTransaction();
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new SystemFaultException(daoEx.getMessage());
        }

        return false;
    }

    private BankAccount getStoredBankAccount(BankAccount account, ConnectionWrapper connectionWrapper)
            throws DAOException {

        BankAccountDAO bankAccountDAO = getDAOFactory().createBankAccountDAO(connectionWrapper);
        return bankAccountDAO.findById(account.getId());
    }

    private BigDecimal determineFee(TransactionPerson beneficiary, BigDecimal amount) {

        BigDecimal fee = OUTER_FEE;
        String bankCode = getConfigManager().getBankCode();
        if ( beneficiary.getBankCode().equals(bankCode) ) {
            fee = INNER_FEE;
        }
        return fee;
    }

    private Transaction buildTransactionForInvoice(Invoice invoice, BankAccount outcomeAccount, ConnectionWrapper connectionWrapper)
            throws DAOException {

            return buildTransactionGeneral(invoice, outcomeAccount, TransactionType.PAYMENT, connectionWrapper);
    }

    private Transaction buildTransactionGeneral(Invoice incomeInfo, BankAccount outcomeAccount, TransactionType type,
                                                ConnectionWrapper connectionWrapper)
            throws DAOException {

        TransactionPerson sender = buildTransactionPerson(outcomeAccount, connectionWrapper);

        BigDecimal fee = determineFee(incomeInfo.getBeneficiary(), incomeInfo.getAmount());

        Transaction newTransaction = Transaction.getBuilder()
                .setCreationDateTime(LocalDateTime.now())
                .setStatus(TransactionStatus.CREATED)
                .setType(type)
                .setSender(sender)
                .setBeneficiary(incomeInfo.getBeneficiary())
                .setAmount(incomeInfo.getAmount())
                .setFee(fee)
                .setDescription(incomeInfo.getDescription())
                .build();

        TransactionDAO transactionDAO = getDAOFactory().createTransactionDAO(connectionWrapper);
        Long id = transactionDAO.create(newTransaction);
        newTransaction.setId(id);

        return newTransaction;
    }

    private TransactionPerson buildTransactionPerson(BankAccount account, ConnectionWrapper connectionWrapper)
            throws DAOException {

        ClientDAO clientDAO = getDAOFactory().createClientDAO(connectionWrapper);
        Client client = clientDAO.findByBankAccountId(account.getId());

        TransactionPerson person = new TransactionPerson();
        person.setBankAccountNumber(account.getNumber());
        person.setBankCode(getConfigManager().getBankCode());
        person.setName(client.fullName());
        person.setPersonCode(client.getTaxNumber());
        return person;
    }

    private boolean processOutcomeTransaction(BankAccount bankAccount, Transaction transaction,
                                              ConnectionWrapper connectionWrapper) throws DAOException {

        boolean result = false;
        if ( bankAccount.withdraw( transaction.getAmount().add(transaction.getFee()) ) ) {
            transaction.setStatus(TransactionStatus.ONCHECK);
            bankAccount.addOperation(transaction);
            if (updateAccount(bankAccount, connectionWrapper)) {
                TransactionDAO transactionDAO = getDAOFactory().createTransactionDAO(connectionWrapper);
                result = transactionDAO.assignToBankAccount(transaction.getId(), bankAccount.getId());
                result = result & transactionDAO.update(transaction);
            }
        }
        return result;
    }

    private boolean updateAccount(BankAccount account, ConnectionWrapper connectionWrapper)
            throws DAOException {

        BankAccountDAO bankAccountDAO = getDAOFactory().createBankAccountDAO(connectionWrapper);
        return bankAccountDAO.update(account);
    }

    @Override
    public BigDecimal checkInvoiceAndGetFee(Invoice invoice, BankAccount account) {

        try (ConnectionWrapper connectionWrapper = getDAOFactory().getConnection()) {
            BankAccount storedBankAccount = getStoredBankAccount(account, connectionWrapper);
            BigDecimal totalAmount = invoice.getAmount();
            BigDecimal fee = determineFee(invoice.getBeneficiary(), invoice.getAmount());
            totalAmount = totalAmount.add(fee);
            if ( storedBankAccount.isAmountValidForWithdrawal(totalAmount) ) {
                return fee;
            }
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
        }
        return null;
    }

    @Override
    public BigDecimal checkTransferAndGetFee(BankAccount outcomeAccount, BankAccount incomeAccount, BigDecimal amount) {

        try (ConnectionWrapper connectionWrapper = getDAOFactory().getConnection()) {
            BankAccount storedOutcomeAccount = getStoredBankAccount(outcomeAccount, connectionWrapper);
            BankAccount storedIncomeAccount = getStoredBankAccount(incomeAccount, connectionWrapper);
            TransactionPerson beneficiary = buildTransactionPerson(storedIncomeAccount, connectionWrapper);

            BigDecimal total = amount;
            BigDecimal fee = determineFee(beneficiary, amount);
            total = total.add(fee);
            if ( storedOutcomeAccount.isAmountValidForWithdrawal(total)) {
                return fee;
            }
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
        }
        return null;
    }

    @Override
    public boolean transferMoney(BankAccount outcomeAccount, BankAccount incomeAccount, BigDecimal amount, Locale locale)
            throws SystemFaultException {

        try (ConnectionWrapper connectionWrapper = getDAOFactory().getConnection()) {
            connectionWrapper.startTransaction();
            BankAccount storedOutcomeAccount = getStoredBankAccount(outcomeAccount, connectionWrapper);
            BankAccount storedIncomeAccount = getStoredBankAccount(incomeAccount, connectionWrapper);
            TransactionPerson beneficiary = buildTransactionPerson(storedIncomeAccount, connectionWrapper);

            BigDecimal total = determineFee(beneficiary, amount);
            total = total.add(amount);
            if ( storedOutcomeAccount.isAmountValidForWithdrawal(total) ) {
                String transactionDescription = ResourceManager.getString(TRANSFER_INNER_DESCRIPTION, locale);
                Transaction transaction = buildTransactionForTransfer(storedOutcomeAccount, beneficiary, amount,
                        transactionDescription, connectionWrapper);
                if ( processOutcomeTransaction(storedOutcomeAccount, transaction, connectionWrapper) ) {
                    if ( processIncomeTransaction(storedIncomeAccount, transaction, connectionWrapper) ) {
                        connectionWrapper.commitTransaction();
                        return true;
                    }
                    if (returnAmount(storedOutcomeAccount, transaction, connectionWrapper)) {
                        connectionWrapper.commitTransaction();
                    }
                }
            }
            connectionWrapper.rollbackTransaction();
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new SystemFaultException(daoEx.getMessage());
        }
        return false;
    }

    private Transaction buildTransactionForTransfer(BankAccount outcomeAccount, TransactionPerson beneficiary, BigDecimal amount,
                                                    String transferDescription, ConnectionWrapper connectionWrapper)
            throws DAOException {

        Invoice incomeInfo = new Invoice();
        incomeInfo.setAmount(amount);
        incomeInfo.setBeneficiary(beneficiary);
        incomeInfo.setDescription(transferDescription);
        return buildTransactionGeneral(incomeInfo, outcomeAccount, TransactionType.TRANSFER, connectionWrapper);
    }

    private boolean processIncomeTransaction(BankAccount bankAccount, Transaction transaction,
                                             ConnectionWrapper connectionWrapper) throws DAOException {

        boolean result = false;
        if ( bankAccount.replenish(transaction.getAmount()) ) {
            bankAccount.addOperation(transaction);
            if (updateAccount(bankAccount, connectionWrapper)) {
                transaction.setExecutionDateTime(LocalDateTime.now());
                transaction.setStatus(TransactionStatus.COMPLETED);
                TransactionDAO transactionDAO = getDAOFactory().createTransactionDAO(connectionWrapper);
                result = transactionDAO.assignToBankAccount(transaction.getId(), bankAccount.getId());
                result = result & transactionDAO.update(transaction);
            }
        }
        return result;
    }

    private boolean returnAmount(BankAccount bankAccount, Transaction transaction, ConnectionWrapper connectionWrapper)
            throws DAOException {

        boolean result = false;
        if ( bankAccount.replenish(transaction.getAmount()) ) {
            if (updateAccount(bankAccount, connectionWrapper)) {
                transaction.setExecutionDateTime(LocalDateTime.now());
                transaction.setStatus(TransactionStatus.DECLINED);
                TransactionDAO transactionDAO = getDAOFactory().createTransactionDAO(connectionWrapper);
                result = transactionDAO.update(transaction);
            }
        }
        return result;
    }

    @Override
    public boolean transferMoneyToForeign(BankAccount outcomeAccount, TransactionPerson beneficiary, BigDecimal amount, Locale locale)
            throws SystemFaultException {

        String transferDescription = String.format(
                ResourceManager.getString(TRANSFER_OUTER_DESCRIPTION, locale), beneficiary.getBankAccountNumber()
        );
        try (ConnectionWrapper connectionWrapper = getDAOFactory().getConnection()) {
            connectionWrapper.startTransaction();
            BankAccount storedOutcomeAccount = getStoredBankAccount(outcomeAccount, connectionWrapper);

            BigDecimal totalOutcome = determineFee(beneficiary, amount);
            totalOutcome = totalOutcome.add(amount);
            if ( storedOutcomeAccount.isAmountValidForWithdrawal(totalOutcome) ) {
                Transaction transaction = buildTransactionForTransfer(storedOutcomeAccount, beneficiary, amount,
                        transferDescription, connectionWrapper);
                if (processOutcomeTransaction(storedOutcomeAccount, transaction, connectionWrapper)) {
                    connectionWrapper.commitTransaction();
                    return true;
                }
            }
            connectionWrapper.rollbackTransaction();
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new SystemFaultException(daoEx.getMessage());
        }
        return false;
    }
}
