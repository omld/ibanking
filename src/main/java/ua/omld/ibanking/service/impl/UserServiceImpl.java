/*
 * UserServiceImpl.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.domain.Client;
import ua.omld.ibanking.domain.Role;
import ua.omld.ibanking.domain.User;
import ua.omld.ibanking.service.UserService;
import ua.omld.ibanking.service.exception.IncorrectCredentialsException;
import ua.omld.ibanking.service.exception.SystemFaultException;
import ua.omld.ibanking.service.utils.PasswordUtils;
import ua.omld.ibanking.storage.connection.ConnectionWrapper;
import ua.omld.ibanking.storage.dao.ClientDAO;
import ua.omld.ibanking.storage.dao.DAOFactory;
import ua.omld.ibanking.storage.dao.UserDAO;
import ua.omld.ibanking.storage.exception.DAOException;

import java.security.NoSuchAlgorithmException;

public class UserServiceImpl extends ApplicationService implements UserService {

    private static final Logger LOGGER = LogManager.getLogger();

    private UserServiceImpl() {
    }

    UserServiceImpl(DAOFactory daoFactory) {
        super(daoFactory);
    }

    private static class InstanceHolder {
        private static UserServiceImpl INSTANCE = new UserServiceImpl();
    }

    public static UserServiceImpl getInstance(){
        return InstanceHolder.INSTANCE;
    }

    /**
     * Finds User by email and password
     *
     * @param email email of User, must not be null
     * @param password password of User, must not be null
     * @return found User
     */
    @Override
    public User loginUser(String email, String password) throws SystemFaultException {

        try (ConnectionWrapper connectionWrapper = getDAOFactory().getConnection()) {
            User user = findUserByEmail(email, connectionWrapper);
            if (user == null) {
                throw new IncorrectCredentialsException();
            }
            checkPassword(user, password);
            setClientForUser(user, connectionWrapper);
            return user;
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new IncorrectCredentialsException();
        } catch (NoSuchAlgorithmException nsaEx) {
            LOGGER.error("This system does not provide hashing ability: " + nsaEx.getMessage());
            throw new SystemFaultException();
        }
    }

    private User findUserByEmail (String email, ConnectionWrapper connectionWrapper) throws DAOException {

        UserDAO userDAO = getDAOFactory().createUserDAO(connectionWrapper);
        return userDAO.findByEmail(email);
    }

    private void checkPassword (User user, String password) throws NoSuchAlgorithmException {

        String encryptedPassword = PasswordUtils.hashPassword(password, user.getEmail(), user.getPasswordChangeDateTime());
        if (!user.getPassword().equals(encryptedPassword)) {
            throw new IncorrectCredentialsException();
        }
    }

    private void setClientForUser(User user, ConnectionWrapper connectionWrapper) throws DAOException {

        if (user.getRole() == Role.CLIENT) {
            ClientDAO clientDAO = getDAOFactory().createClientDAO(connectionWrapper);
            Client client = clientDAO.findByUserId(user.getId());
            user.setClient(client);
        }
    }

    /**
     * Updates user information.
     *
     * @param user user, must not be null
     * @return true if user updated
     */
    @Override
    public User updateUserInfo(User user) throws SystemFaultException {

        User returnUser;
        try (ConnectionWrapper connectionWrapper = getDAOFactory().getConnection()) {
            connectionWrapper.startTransaction();

            User foundUser = findUserByEmail(user.getEmail(), connectionWrapper);
            if ( areUsersSimilar(foundUser, user) && updateUser(user, connectionWrapper) ) {
                returnUser = user;
            } else {
                returnUser = foundUser;
            }

            connectionWrapper.commitTransaction();
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new SystemFaultException();
        }
        return returnUser;
    }

    private boolean areUsersSimilar (User storedUser, User updatedUser) {
        return storedUser.getId().equals(updatedUser.getId())
                && storedUser.getEmail().equals(updatedUser.getEmail())
                && storedUser.getRegistrationDate().equals(updatedUser.getRegistrationDate())
                && storedUser.getRole().equals(updatedUser.getRole());
    }

    private boolean updateUser (User user, ConnectionWrapper connectionWrapper) throws DAOException {
        UserDAO userDAO = getDAOFactory().createUserDAO(connectionWrapper);
        return userDAO.update(user);
    }

}
