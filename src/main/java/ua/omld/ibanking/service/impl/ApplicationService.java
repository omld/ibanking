/*
 * ApplicationService.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service.impl;

import ua.omld.ibanking.storage.dao.DAOFactory;
import ua.omld.ibanking.storage.exception.StorageFactoryException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.utils.ConfigurationManager;

/**
 * Abstract class that provide common functionality for all services
 */
public abstract class ApplicationService {

    private static final Logger LOGGER = LogManager.getLogger();

    private DAOFactory daoFactory;
    private ConfigurationManager configManager;

    {
        configManager = ConfigurationManager.getInstance();
    }

    ApplicationService() {
        try {
            daoFactory = DAOFactory.getDAOFactory(configManager.getStorageType());
        } catch (StorageFactoryException sfEx) {
            LOGGER.error("Error get storage Factory: " + sfEx);
            throw new RuntimeException(sfEx);
        }
    }

    ApplicationService(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    protected DAOFactory getDAOFactory() {
        return daoFactory;
    }

    protected ConfigurationManager getConfigManager() {
        return configManager;
    }
}
