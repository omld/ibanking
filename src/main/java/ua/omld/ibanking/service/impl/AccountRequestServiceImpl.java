/*
 * AccountRequestServiceImpl.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service.impl;

import ua.omld.ibanking.domain.*;
import ua.omld.ibanking.service.AccountRequestService;
import ua.omld.ibanking.service.exception.*;
import ua.omld.ibanking.storage.connection.ConnectionWrapper;
import ua.omld.ibanking.storage.dao.*;
import ua.omld.ibanking.storage.exception.DAOException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AccountRequestServiceImpl extends ApplicationService implements AccountRequestService {

    private static final BigDecimal MAX_CREDIT_WITHOUT_DEPOSIT = new BigDecimal(5000.0);
    private static final BigDecimal CURRENT_INTEREST_RATE = new BigDecimal(18.0);
    private static final int STANDARD_VALID_PERIOD_YEARS = 2;
    private static final int DEPOSIT_MULTIPLIER_FOR_CREDIT = 3;

    private static final Logger LOGGER = LogManager.getLogger();

    private AccountRequestServiceImpl() {
    }

    AccountRequestServiceImpl(DAOFactory daoFactory) {

        super(daoFactory);
    }

    private static class InstanceHolder {

        private static AccountRequestServiceImpl INSTANCE = new AccountRequestServiceImpl();
    }

    public static AccountRequestServiceImpl getInstance(){

        return InstanceHolder.INSTANCE;
    }


    @Override
    public CreditAccountRequest makeRequestForCreditAccount(Client client, BigDecimal amount, Period term)
            throws SystemFaultException {

        try (ConnectionWrapper connectionWrapper = getDAOFactory().getConnection()) {
            connectionWrapper.startTransaction();
            if ( isClientHasCreditAccount(client, connectionWrapper) ) {
                throw new CreditAccountExistException();
            }

            CreditAccountRequest request = buildRequest(client, amount, term);
            CreditAccountRequestDAO creditAccountRequestDAO = getDAOFactory().createCreditAccountRequestDAO(connectionWrapper);
            Long id = creditAccountRequestDAO.create(request);
            connectionWrapper.commitTransaction();
            request.setId(id);

            return request;
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new SystemFaultException(daoEx.getMessage());
        }
    }

    private boolean isClientHasCreditAccount(Client client, ConnectionWrapper connectionWrapper) throws DAOException {

        BankAccountDAO bankAccountDAO = getDAOFactory().createBankAccountDAO(connectionWrapper);
        List<BankAccount> foundAccounts = bankAccountDAO.findBankAccountsByClientId(client.getId());
        boolean creditAccountPresent = foundAccounts.parallelStream().anyMatch(ac -> ac instanceof CreditAccount);
        return creditAccountPresent;
    }

    CreditAccountRequest buildRequest(Client client, BigDecimal amount, Period term) {

        CreditAccountRequest request = new CreditAccountRequest();
        request.setClient(client);
        request.setAmount(amount);
        request.setCreationDateTime(LocalDateTime.now());
        request.setTerm(term);

        return request;
    }

    @Override
    public CreditAccountRequest getRequestInfo(Long id) throws SystemFaultException {

        try (ConnectionWrapper connectionWrapper = getDAOFactory().getConnection()) {
            CreditAccountRequestDAO creditAccountRequestDAO =
                    getDAOFactory().createCreditAccountRequestDAO(connectionWrapper);
            connectionWrapper.startTransaction();
            CreditAccountRequest request = creditAccountRequestDAO.findById(id);

            ClientDAO clientDAO = getDAOFactory().createClientDAO(connectionWrapper);
            Client client = clientDAO.findById(request.getClient().getId());

            BankAccountDAO bankAccountDAO = getDAOFactory().createBankAccountDAO(connectionWrapper);
            List<BankAccount> accounts = bankAccountDAO.findBankAccountsByClientId(request.getClient().getId());
            connectionWrapper.commitTransaction();

            client.setBankAccounts(accounts);
            request.setClient(client);
            return request;
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new SystemFaultException(daoEx.getMessage());
        }
    }

    @Override
    public List<CreditAccountRequest> getNewRequests() throws SystemFaultException {

        try (CreditAccountRequestDAO creditAccountRequestDAO = getDAOFactory().createCreditAccountRequestDAO()) {
            List<CreditAccountRequest> requests = creditAccountRequestDAO.findNewRequests();

            return requests;
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new SystemFaultException(daoEx.getMessage());
        }
    }

    @Override
    public List<CreditAccountRequest> getAllRequests() throws SystemFaultException {

        try (CreditAccountRequestDAO creditAccountRequestDAO = getDAOFactory().createCreditAccountRequestDAO()) {
            List<CreditAccountRequest> requests = creditAccountRequestDAO.getAll();

            return requests;
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new SystemFaultException(daoEx.getMessage());
        }
    }

    @Override
    public List<CreditAccountRequest> getAllRequestsForClient(Long clientId) throws SystemFaultException {

        try (CreditAccountRequestDAO creditAccountRequestDAO = getDAOFactory().createCreditAccountRequestDAO()) {
            List<CreditAccountRequest> requests = creditAccountRequestDAO.findAllRequestsByClientId(clientId);

            return requests;
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new SystemFaultException(daoEx.getMessage());
        }
    }

    @Override
    public void approveRequestAndCreateCreditAccount(CreditAccountRequest request)
            throws SystemFaultException {

        try (ConnectionWrapper connectionWrapper = getDAOFactory().getConnection()) {
            connectionWrapper.startTransaction();
            if (isClientHasCreditAccount(request.getClient(), connectionWrapper)) {
                throw new CreditAccountExistException();
            }
            if (!isRequestDataSatisfiedRequirements(request, connectionWrapper)) {
                throw new CreditRequestIllegalException();
            }
            acceptRequest(request, connectionWrapper);

            CreditAccount creditAccount = createCreditAccountForRequest(request, connectionWrapper);
            if (!assignBankAccountToClient(request.getClient(), creditAccount, connectionWrapper)) {
                connectionWrapper.rollbackTransaction();
                throw new SystemFaultException();
            }
            connectionWrapper.commitTransaction();
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
            throw new SystemFaultException(daoEx.getMessage());
        }
    }

    private boolean isRequestDataSatisfiedRequirements(CreditAccountRequest accountRequest, ConnectionWrapper connectionWrapper)
            throws DAOException {

        BankAccountDAO bankAccountDAO = getDAOFactory().createBankAccountDAO(connectionWrapper);
        List<BankAccount> foundAccounts = bankAccountDAO.findBankAccountsByClientId(accountRequest.getClient().getId());
        List<DepositAccount> deposits = foundAccounts.parallelStream()
                .filter(acc -> acc instanceof DepositAccount)
                .map(acc-> ((DepositAccount) acc))
                .collect(Collectors.toList());

        BigDecimal permittedAmount = calcPermittedAmount(deposits);
        LocalDate permittedValidity = calcPermittedValidity(deposits);

        return (accountRequest.getApprovedAmount().compareTo(permittedAmount) <= 0)
            && accountRequest.getApprovedValidity().isBefore(permittedValidity);

    }

    private BigDecimal calcPermittedAmount(List<DepositAccount> deposits) {

        BigDecimal permittedAmount = MAX_CREDIT_WITHOUT_DEPOSIT;
        BigDecimal totalDepositsAmount = BigDecimal.ZERO;
        if (!deposits.isEmpty()) {
            for (DepositAccount curDeposit : deposits) {
                totalDepositsAmount = totalDepositsAmount.add(curDeposit.getDepositAmount());
            }
            BigDecimal amountForDeposits = totalDepositsAmount.multiply (
                    new BigDecimal(DEPOSIT_MULTIPLIER_FOR_CREDIT) );
            if (amountForDeposits.compareTo(permittedAmount) > 0) {
                permittedAmount = amountForDeposits;
            }
        }
        return permittedAmount;
    }

    private LocalDate calcPermittedValidity(List<DepositAccount> deposits) {

        LocalDate maxValidity = LocalDate.now().plusYears(STANDARD_VALID_PERIOD_YEARS);
        maxValidity = deposits.parallelStream()
                .map(DepositAccount::getValidityDate)
                .max(LocalDate::compareTo)
                .orElse(maxValidity);

        return maxValidity;
    }

    private void acceptRequest(CreditAccountRequest request, ConnectionWrapper connectionWrapper) throws DAOException {

        request.setStatus(CreditRequestStatus.ACCEPTED);
        CreditAccountRequestDAO requestDAO = getDAOFactory().createCreditAccountRequestDAO(connectionWrapper);
        requestDAO.update(request);
    }

    private CreditAccount createCreditAccountForRequest(CreditAccountRequest accountRequest, ConnectionWrapper connectionWrapper)
            throws DAOException {

        BankAccountDAO bankAccountDAO = getDAOFactory().createBankAccountDAO(connectionWrapper);
        CreditAccount newCreditAccount = CreditAccount.getBuilder()
                .setBalance(accountRequest.getApprovedAmount())
                .setCreditLimit(accountRequest.getApprovedAmount())
                .setValidityDate(accountRequest.getApprovedValidity())
                .setCurrentDebt(BigDecimal.ZERO)
                .setAmountOfAccruedInterest(BigDecimal.ZERO)
                .setInterestRate(CURRENT_INTEREST_RATE)
                .build();
        Long id = bankAccountDAO.create(newCreditAccount);
        newCreditAccount.setId(id);

        return newCreditAccount;
    }

    private boolean assignBankAccountToClient(Client client, CreditAccount creditAccount, ConnectionWrapper connectionWrapper)
            throws DAOException {

        BankAccountDAO bankAccountDAO = getDAOFactory().createBankAccountDAO(connectionWrapper);
        return bankAccountDAO.assignBankAccountToClient(creditAccount.getId(), client.getId());
    }

    @Override
    public void rejectRequestForCreditAccount(CreditAccountRequest request) {

        try (ConnectionWrapper connectionWrapper = getDAOFactory().getConnection()) {
            CreditAccountRequestDAO creditAccountRequestDAO = getDAOFactory().createCreditAccountRequestDAO(connectionWrapper);
            request.reject();
            connectionWrapper.startTransaction();
            creditAccountRequestDAO.update(request);
            connectionWrapper.commitTransaction();
        } catch (DAOException daoEx) {
            LOGGER.error(daoEx.getMessage());
        }
    }
}
