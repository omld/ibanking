/*
 * PasswordUtils.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * Non-instantiable helper class for passwords processing.
 */
public class PasswordUtils {

    private PasswordUtils() {
        throw new AssertionError();
    }

    /**
     * Produce hash of password according to given email and date.
     *
     * @param password user password
     * @param email user email
     * @param dateTime datetime for generating hash
     * @return hash of password
     * @throws NoSuchAlgorithmException throws exception if platform doesn't provides used digest algorithm
     */
    public static String hashPassword(String password, String email, LocalDateTime dateTime)
            throws NoSuchAlgorithmException {

        return hashString(saltAndPaperPassword(password, email, dateTime));
    }

    private static String saltAndPaperPassword(String password, String email, LocalDateTime dateTime)
            throws NoSuchAlgorithmException {

        String salt = hashString(email);
        String paper = hashString(Long.toString(dateTime.toEpochSecond(ZoneOffset.UTC)));
        return salt + password + paper;
    }

    private static String hashString(String string) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(string.getBytes());
        byte[] mdArray = md.digest();

        StringBuilder sb = new StringBuilder(mdArray.length * 2);
        for (byte b : mdArray) {
            int v = b & 0xff;
            if (v < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(v));
        }
        return sb.toString();
    }
}

