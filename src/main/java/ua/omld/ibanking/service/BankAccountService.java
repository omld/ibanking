/*
 * BankAccountService.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service;

import ua.omld.ibanking.domain.BankAccount;
import ua.omld.ibanking.domain.Client;

import java.util.List;

/**
 * Service interface to process operations with bank accounts
 */
public interface BankAccountService {

    List<BankAccount> findAccountsByClient(Client client);

    BankAccount getBankAccountInfoById(Long id);

}
