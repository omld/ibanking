/*
 * UserService.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service;

import ua.omld.ibanking.domain.User;
import ua.omld.ibanking.service.exception.SystemFaultException;

/**
 * Service interface to process users login operations
 */
public interface UserService {

    /**
     * Finds User by email and password.
     *
     * @param email email of User, must not be null
     * @param password password of User, must not be null
     * @return found User
     */
    User loginUser(String email, String password) throws SystemFaultException;

    /**
     * Updates user information.
     *
     * @param user user, must not be null
     * @return true if user updated
     */
    User updateUserInfo(User user) throws SystemFaultException;
}
