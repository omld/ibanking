/*
 * TransactionException.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service.exception;

/**
 * Represents exception which can be thrown during work with transactions.
 */
public class TransactionException extends Exception {

    public TransactionException(String message) {
        super(message);
    }
}
