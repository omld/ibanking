/*
 * SystemFaultException.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service.exception;

public class SystemFaultException extends Exception {

    public SystemFaultException() {
    }

    public SystemFaultException(String message) {
        super(message);
    }

    public SystemFaultException(String message, Throwable cause) {
        super(message, cause);
    }
}
