/*
 * CreditRequestIllegal.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service.exception;

public class CreditRequestIllegalException extends RuntimeException {
}
