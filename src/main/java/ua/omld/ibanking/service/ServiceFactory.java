/*
 * ServiceFactory.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service;

import ua.omld.ibanking.service.impl.*;

/**
 * Returns objects witch implements Service interfaces.
 */
public class ServiceFactory {

    private ServiceFactory() {
        throw new AssertionError();
    }

    public static UserService getUserService() {
        return UserServiceImpl.getInstance();
    }

    public static BankAccountService getBankAccountService () {
        return BankAccountServiceImpl.getInstance();
    }

    public static TransactionsService getTransactionsService () {
        return TransactionsServiceImpl.getInstance();
    }

    public static AccountRequestService getAccountRequestService () {
        return AccountRequestServiceImpl.getInstance();
    }
}
