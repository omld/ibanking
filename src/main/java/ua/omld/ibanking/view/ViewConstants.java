/*
 * ViewConstants.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.view;

public interface ViewConstants {

    interface JSP {

        String PATH = "/WEB-INF/jsp/";
        String P_CLIENT = PATH + "client/";
        String P_ADMIN = PATH + "admin/";
        String P_ERRORS = PATH + "errors/";

        String LOGIN = PATH + "login.jsp";

        interface Errors {

            String GLOBAL = P_ERRORS + "error.jsp";
            String E404 = P_ERRORS + "404.jsp";
            String E500 = GLOBAL;
        }

        interface Client {

            String DASHBOARD = P_CLIENT + "dashboard.jsp";
            String ACCOUNTS = P_CLIENT + "accounts.jsp";
            String CREDIT_INFO = P_CLIENT + "credit_info.jsp";
            String DEPOSIT_INFO = P_CLIENT + "deposit_info.jsp";
            String TRANSACTION_INFO = P_CLIENT + "transaction_info.jsp";
            String PAYMENTS = P_CLIENT + "payments.jsp";
            String TRANSFERS = P_CLIENT + "transfers.jsp";
            String REQUESTS = P_CLIENT + "requests.jsp";
            String REQUEST_SENT = P_CLIENT + "request_sent.jsp";
        }

        interface Admin {

            String DASHBOARD = P_ADMIN + "dashboard.jsp";
            String REQUESTS = P_ADMIN + "requests.jsp";
            String REQUEST_INFO = P_ADMIN + "request_info.jsp";
        }
    }

    interface URLPaths {

        // main zone
        String MAIN = "/";
        String LOGIN = "/login";
        String LOGOUT = "/logout";
        // resources
        String PREFIX_CSS = "/css";
        String PREFIX_JS = "/js";
        // errors
        String ERROR = "/error";
        String ERR404 = "/error404";

        String PREFIX_CLIENT = "/ibank";
        String PREFIX_ADMIN = "/admin";

        interface Client {

            String DASHBOARD = PREFIX_CLIENT + "/dashboard";
            String ACCOUNTS = PREFIX_CLIENT + "/accounts";
            String TRANSACTION = PREFIX_CLIENT + "/transaction";
            String PAYMENTS = PREFIX_CLIENT + "/payments";
            String TRANSFERS = PREFIX_CLIENT + "/transfers";
            String REQUESTS = PREFIX_CLIENT + "/requests";
        }

        interface Admin {

            String DASHBOARD = PREFIX_ADMIN + "/dashboard";
            String REQUESTS = PREFIX_ADMIN + "/requests";
            String REQUEST_PROCESS = PREFIX_ADMIN + "/request-process";
        }
    }

    interface Attributes {

        String ERRORS = "errorsList";
        String MESSAGES = "messagesList";
        String ERRORS_READ = "e_read";
        String MESSAGES_READ = "m_read";
        String LOCALE = "locale";
        String USER = "user";
        String ACCOUNTS = "accounts";
        String ACCOUNT = "account";
        String TRANSACTION = "transaction";
        String INVOICE = "invoice";
        String AMOUNT = "amount";
        String FEE = "fee";
        String ACCOUNT_OUT = "accountOut";
        String ACCOUNT_IN = "accountIn";
        String PAYMENT_COMPLETE = "payComplete";
        String TRANSFER_COMPLETE = "transComplete";
        String CREDIT_PRESENT = "creditPresent";
        String REQUESTS = "requests";
        String REQUEST_NUMBER = "requestNumber";
        String REQUEST_INFO = "requestInfo";
    }

    interface Parameters {

        String EMAIL = "u_email";
        String PASS = "u_pass";
        String LOCALE = Attributes.LOCALE;
        String ACTION = "suffix";
        String ID = "id";
        String ACCOUNT_ID = "account";
        String BENEFICIARY_NAME = "beneficiary_name";
        String BENEFICIARY_CODE = "beneficiary_code";
        String BENEFICIARY_BANK = "beneficiary_bank";
        String BENEFICIARY_ACCOUNT = "beneficiary_account";
        String DESCRIPTION = "description";
        String AMOUNT = "amount";
        String ACCOUNT_OUT_ID = "account_out";
        String ACCOUNT_IN_ID = "account_in";
        String TERM = "term";
        String APPROVED_AMOUNT = "approved_amount";
        String APPROVED_VALIDITY = "approved_validity";
    }

    interface Fields {

        String PAYMENT_VERIFY = "verify";
        String PAYMENT_PAY = "pay";
        String TRANSFER_VERIFY = "verify";
        String TRANSFER_PROCESS = "process";
        String REQUEST_APPROVE = "approve";
        String REQUEST_REJECT = "reject";
    }

    interface SysParameters {

        String DEFAULT_LOCALE =  "defaultLocale";
    }

    interface Errors {

        String BAD_LOGIN = "login.incorrect";
        String BAD_EMAIL = "login.badEmail";

        String BAD_REQUEST = "client.badRequest";

        String TRANSACTION_NOT_FOUND = "client.transaction.notFound";
        String ACCOUNT_NOT_FOUND = "client.accounts.notFound";
        String PAYMENT_ERROR = "client.payment.error";
        String BAD_BENEFICIARY_NAME = "client.payment.beneficiary.badName";
        String BAD_BENEFICIARY_CODE = "client.payment.beneficiary.badCode";
        String BAD_BANK_CODE = "client.payment.badBank";
        String BAD_ACCOUNT_NUMBER = "client.payment.badAccountNumber";
        String BAD_INVOICE_DESCRIPTION = "client.payment.badInvoiceDescription";
        String BAD_AMOUNT = "client.payment.badAmount";
        String TRANSFER_ERROR = "client.transfer.error";
    }

    interface Messages {

        String ACCOUNTS_ABSENT = "client.accounts.absent";
        String PAYMENT_COMPLETE = "client.payment.complete";
        String PAYMENT_PROHIBITED = "client.payment.prohibited";
        String TRANSFER_ONE_ACCOUNT = "client.transfer.onlyOneAccount";
        String TRANSFER_PROHIBITED = "client.transfer.prohibited";
        String TRANSFER_PROHIBITED_ONE_ACCOUNT = "client.transfer.prohibitedOneAccount";
        String TRANSFER_COMPLETE = "client.transfer.complete";
        String CREDIT_EXIST = "admin.request.creditExist";
        String REQUEST_PROHIBITED = "admin.request.prohibited";
        String REQUEST_APPROVED = "admin.request.approved";
        String REQUEST_REJECTED = "admin.request.rejected";
    }

}