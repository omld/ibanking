/*
 * DepositAccount.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * Represent bank account for deposit
 */
public class DepositAccount extends BankAccount {

    private DepositAccount() {
    }

    public BigDecimal getDepositAmount() {
        return getBalance();
    }


    @Override
    public boolean withdraw(BigDecimal amount) {
        return false;
    }

    @Override
    public boolean replenish(BigDecimal amount) {
        boolean result = false;
        if ( amount.compareTo(BigDecimal.ZERO) > 0 ) {
            setBalance(getBalance().add(amount));
            result = true;
        }
        return result;
    }

    @Override
    public boolean isAmountValidForWithdrawal(BigDecimal amount) {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        return (this == o) || (o != null) && super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "DepositAccount: " + super.toString();
    }

    public static DepositAccountBuilder getBuilder(){
        return new DepositAccountBuilder();
    }

    /**
     * Builds DepositAccount object
     */
    public static final class DepositAccountBuilder extends BankAccountBuilder<DepositAccountBuilder> {

        private DepositAccountBuilder() {
        }

        @Override
        protected DepositAccountBuilder self() {
            return this;
        }

        public DepositAccount build() {
            DepositAccount depositAccount = new DepositAccount();
            super.setBankAccount(depositAccount);
            super.build();
            return depositAccount;
        }
    }

}
