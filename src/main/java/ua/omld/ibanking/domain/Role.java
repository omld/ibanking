/*
 * Role.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

/**
 * Represents user role:
 *  CLIENT - bank client
 *  ADMIN - administrator of the system
 */
public enum Role {

    CLIENT, ADMIN
}
