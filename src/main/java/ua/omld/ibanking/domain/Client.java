/*
 * Client.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

import ua.omld.ibanking.utils.validators.Patterns;
import ua.omld.ibanking.utils.validators.StringValidator;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents client of the bank
 */
public class Client implements Entity<Long> {

    private Long id;
    private String surname;
    private String name;
    private String patronymic;
    private LocalDate birthDate;
    private String taxNumber;
    private List<BankAccount> bankAccounts;

    private Client() {
        bankAccounts = new ArrayList<>();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String fullName() {
        return surname + " " + name + " " + patronymic;
    }

    public String shortFullName() {
        return surname + " " + name.charAt(0) + ". " + patronymic.charAt(0) + ".";
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(List<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public void addBankAccount(BankAccount bankAccount) {
        this.bankAccounts.add(bankAccount);
    }

    public void removeBankAccount(BankAccount bankAccount) {
        this.bankAccounts.remove(bankAccount);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(id, client.id) &&
                Objects.equals(surname, client.surname) &&
                Objects.equals(name, client.name) &&
                Objects.equals(patronymic, client.patronymic) &&
                Objects.equals(birthDate, client.birthDate) &&
                Objects.equals(taxNumber, client.taxNumber);
    }

    @Override
    public int hashCode() {
        return this.getClass().hashCode()
                + Objects.hashCode(id)
                + 3 * Objects.hashCode(surname)
                + 5 * Objects.hashCode(name)
                + 7 * Objects.hashCode(patronymic)
                + 9 * Objects.hashCode(birthDate)
                + 11 * Objects.hashCode(taxNumber);
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", birthDate=" + birthDate +
                ", taxNumber='" + taxNumber + '\'' +
                '}';
    }

    public static ClientBuilder getBuilder(){
        return new ClientBuilder();
    }

    /**
     * Builds Client object
     */
    public static final class ClientBuilder {
        private Long id;
        private String surname;
        private String name;
        private String patronymic;
        private LocalDate birthDate;
        private String taxNumber;
        private List<BankAccount> bankAccounts;


        private ClientBuilder() {
        }

        public ClientBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public ClientBuilder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public ClientBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public ClientBuilder setPatronymic(String patronymic) {
            this.patronymic = patronymic;
            return this;
        }

        public ClientBuilder setBirthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public ClientBuilder setTaxNumber(String taxNumber) {
            StringValidator taxNumberValidator = new StringValidator(Patterns.PERSON_TAX_NUMBER);
            if (taxNumberValidator.validate(taxNumber)) {
                this.taxNumber = taxNumber;
            }
            return this;
        }

        public ClientBuilder setBankAccounts(List<BankAccount> bankAccounts) {
            this.bankAccounts = bankAccounts;
            return this;
        }

        public Client build() {
            Client client = new Client();
            client.id = id;
            client.surname = surname;
            client.name = name;
            client.patronymic = patronymic;
            client.birthDate = birthDate;
            client.taxNumber = taxNumber;
            client.bankAccounts = new ArrayList<>();
            if (bankAccounts != null)
                client.bankAccounts.addAll(bankAccounts);
            return client;
        }
    }
}