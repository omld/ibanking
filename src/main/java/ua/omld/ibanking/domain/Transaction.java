/*
 * Transaction.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Represent bank transactions:
 * - transfer;
 * - payment;
 */
public class Transaction implements Entity<Long> {

    private Long id;
    private LocalDateTime creationDateTime;
    private LocalDateTime executionDateTime;
    private BigDecimal amount;
    private BigDecimal fee;
    private TransactionStatus status;
    private TransactionType type;
    private TransactionPerson sender;
    private TransactionPerson beneficiary;
    private String description;

    public Transaction() {
        this.sender = new TransactionPerson();
        this.beneficiary = new TransactionPerson();
    }

    protected Transaction(Transaction that) {
        this();
        this.id = that.id;
        this.creationDateTime = that.creationDateTime;
        this.executionDateTime = that.executionDateTime;
        this.amount = that.amount;
        this.fee = that.fee;
        this.status = that.status;
        this.type = that.type;
        this.sender = that.sender;
        this.beneficiary = that.beneficiary;
        this.description = that.description;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public LocalDateTime getExecutionDateTime() {
        return executionDateTime;
    }

    public void setExecutionDateTime(LocalDateTime executionDateTime) {
        this.executionDateTime = executionDateTime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public void setSenderName(String name) {
        this.sender.name = name;
    }

    public String getSenderName() {
        return this.sender.name;
    }

    public void setSenderCode(String code) {
        this.sender.personCode = code;
    }

    public String getSenderCode() {
        return this.sender.personCode;
    }

    public void setSenderBankCode(String bankCode) {
        this.sender.bankCode = bankCode;
    }

    public String getSenderBankCode() {
        return this.sender.bankCode;
    }

    public void setSenderBankAccount(String bankAccountNumber) {
        this.sender.bankAccountNumber = bankAccountNumber;
    }

    public String getSenderBankAccountNumber() {
        return this.sender.bankAccountNumber;
    }

    public void setBeneficiaryName(String name) {
        this.beneficiary.name = name;
    }

    public String getBeneficiaryName() {
        return this.beneficiary.name;
    }

    public void setBeneficiaryCode(String code) {
        this.beneficiary.personCode = code;
    }

    public String getBeneficiaryCode() {
        return this.beneficiary.personCode;
    }

    public void setBeneficiaryBankCode(String bankCode) {
        this.beneficiary.bankCode = bankCode;
    }

    public String getBeneficiaryBankCode() {
        return this.beneficiary.bankCode;
    }

    public void setBeneficiaryBankAccount(String bankAccountNumber) {
        this.beneficiary.bankAccountNumber = bankAccountNumber;
    }

    public String getBeneficiaryBankAccountNumber() {
        return this.beneficiary.bankAccountNumber;
    }

    public TransactionPerson getSender() {
        return sender;
    }

    public void setSender(TransactionPerson sender) {
        this.sender = sender;
    }

    public TransactionPerson getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(TransactionPerson beneficiary) {
        this.beneficiary = beneficiary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(creationDateTime, that.creationDateTime) &&
                Objects.equals(executionDateTime, that.executionDateTime) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(fee, that.fee) &&
                status == that.status &&
                type == that.type &&
                Objects.equals(sender, that.sender) &&
                Objects.equals(beneficiary, that.beneficiary) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return this.getClass().hashCode()
                + Objects.hashCode(id)
                + 3 * Objects.hashCode(creationDateTime)
                + 5 * Objects.hashCode(executionDateTime)
                + 7 * Objects.hashCode(amount)
                + 9 * Objects.hashCode(fee)
                + 11 * Objects.hashCode(status)
                + 13 * Objects.hashCode(type)
                + 15 * Objects.hashCode(sender)
                + 17 * Objects.hashCode(beneficiary)
                + 19 * Objects.hashCode(description);
    }

    public static TransactionBuilder getBuilder(){
        return new TransactionBuilder();
    }

    /**
     * Builds Transaction object
     */
    public static final class TransactionBuilder {
        private Long id;
        private LocalDateTime creationDateTime;
        private LocalDateTime executionDateTime;
        private BigDecimal amount;
        private BigDecimal fee;
        private TransactionStatus status;
        private TransactionType type;
        private TransactionPerson sender;
        private TransactionPerson beneficiary;
        private String description;

        private TransactionBuilder() {
            sender = new TransactionPerson();
            beneficiary = new TransactionPerson();
        }

        public TransactionBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public TransactionBuilder setCreationDateTime(LocalDateTime creationDateTime) {
            this.creationDateTime = creationDateTime;
            return this;
        }

        public TransactionBuilder setExecutionDateTime(LocalDateTime executionDateTime) {
            this.executionDateTime = executionDateTime;
            return this;
        }

        public TransactionBuilder setAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public TransactionBuilder setFee(BigDecimal fee) {
            this.fee = fee;
            return this;
        }

        public TransactionBuilder setStatus(TransactionStatus status) {
            this.status = status;
            return this;
        }

        public TransactionBuilder setType(TransactionType type) {
            this.type = type;
            return this;
        }

        public TransactionBuilder setSender(TransactionPerson sender) {
            this.sender = sender;
            return this;
        }

        public TransactionBuilder setBeneficiary(TransactionPerson beneficiary) {
            this.beneficiary = beneficiary;
            return this;
        }

        public TransactionBuilder setSenderName(String name) {
            this.sender.name = name;
            return this;
        }

        public TransactionBuilder setSenderCode(String code) {
            this.sender.personCode = code;
            return this;
        }

        public TransactionBuilder setSenderBankCode(String bankCode) {
            this.sender.bankCode = bankCode;
            return this;
        }

        public TransactionBuilder setSenderBankAccount(String bankAccountNumber) {
            this.sender.bankAccountNumber = bankAccountNumber;
            return this;
        }

        public TransactionBuilder setBeneficiaryName(String name) {
            this.beneficiary.name = name;
            return this;
        }

        public TransactionBuilder setBeneficiaryCode(String code) {
            this.beneficiary.personCode = code;
            return this;
        }

        public TransactionBuilder setBeneficiaryBankCode(String bankCode) {
            this.beneficiary.bankCode = bankCode;
            return this;
        }

        public TransactionBuilder setBeneficiaryBankAccount(String bankAccountNumber) {
            this.beneficiary.bankAccountNumber = bankAccountNumber;
            return this;
        }

        public TransactionBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Transaction build() {
            Transaction transaction = new Transaction();
            transaction.id = id;
            transaction.creationDateTime = creationDateTime;
            transaction.executionDateTime = executionDateTime;
            transaction.amount = amount;
            transaction.fee = fee;
            transaction.status = status;
            transaction.type = type;
            transaction.sender = sender;
            transaction.beneficiary = beneficiary;
            transaction.description = description;
            return transaction;
        }
    }

}