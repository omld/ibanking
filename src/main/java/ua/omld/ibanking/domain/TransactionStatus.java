/*
 * TransactionStatus.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

/**
 * Represent status of bank transaction during processing.
 */
public enum TransactionStatus {

    CREATED("transaction.status.created"),
    ONCHECK("transaction.status.oncheck"),
    COMPLETED("transaction.status.completed"),
    DECLINED("transaction.status.declined");

    private String status;

    TransactionStatus(String status) {
        this.status = status;
    }

    public String getStatusString() {
        return status;
    }

}
