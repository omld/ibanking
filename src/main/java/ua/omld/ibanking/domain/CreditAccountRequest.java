/*
 * CreditAccountRequest.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Objects;

/**
 * Represent request from client for credit bank account
 */
public class CreditAccountRequest implements Entity<Long> {

    private Long id;
    private Client client;
    private BigDecimal amount;
    private BigDecimal approvedAmount;
    private LocalDateTime creationDateTime;
    private Period term;
    private LocalDate approvedValidity;
    private CreditRequestStatus status;

    public CreditAccountRequest() {
        this.creationDateTime = LocalDateTime.now();
        this.status = CreditRequestStatus.NEW;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(BigDecimal approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public Period getTerm() {
        return term;
    }

    public void setTerm(Period term) {
        this.term = term;
    }

    public LocalDate getApprovedValidity() {
        return approvedValidity;
    }

    public void setApprovedValidity(LocalDate approvedValidity) {
        this.approvedValidity = approvedValidity;
    }

    public CreditRequestStatus getStatus() {
        return status;
    }

    public void setStatus(CreditRequestStatus status) {
        this.status = status;
    }

    public boolean isNew() {
        return status == CreditRequestStatus.NEW;
    }

    public void approve(BigDecimal amount, LocalDate validity) {
        status = CreditRequestStatus.ACCEPTED;
        approvedAmount = amount;
        approvedValidity = validity;
    }

    public void reject() {
        status = CreditRequestStatus.REJECTED;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditAccountRequest that = (CreditAccountRequest) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(client, that.client) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(approvedAmount, that.approvedAmount) &&
                Objects.equals(creationDateTime, that.creationDateTime) &&
                Objects.equals(term, that.term) &&
                Objects.equals(approvedValidity, that.approvedValidity) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return this.getClass().hashCode()
                + Objects.hashCode(id)
                + 3 * Objects.hashCode(client)
                + 5 * Objects.hashCode(amount)
                + 7 * Objects.hashCode(approvedAmount)
                + 9 * Objects.hashCode(creationDateTime)
                + 11 * Objects.hashCode(term)
                + 13 * Objects.hashCode(approvedValidity)
                + 15 * Objects.hashCode(status);
    }

    @Override
    public String toString() {
        return "CreditAccountRequest{" +
                "id=" + id +
                ", client=" + client +
                ", amount=" + amount +
                ", approvedAmount=" + approvedAmount +
                ", creationDateTime=" + creationDateTime +
                ", term=" + term +
                ", approvedValidity=" + approvedValidity +
                ", status=" + status +
                '}';
    }

    public static CreditAccountRequestBuilder getBuilder(){
        return new CreditAccountRequestBuilder();
    }

    /**
     * Builds Transaction object
     */
    public static final class CreditAccountRequestBuilder {

        private Long id;
        private Client client;
        private BigDecimal amount;
        private BigDecimal approvedAmount;
        private LocalDateTime creationDateTime;
        private Period term;
        private LocalDate approvedValidity;
        private CreditRequestStatus status;

        private CreditAccountRequestBuilder() {
        }

        public CreditAccountRequestBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public CreditAccountRequestBuilder setClient(Client client) {
            this.client = client;
            return this;
        }

        public CreditAccountRequestBuilder setAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public CreditAccountRequestBuilder setApprovedAmount(BigDecimal approvedAmount) {
            this.approvedAmount = approvedAmount;
            return this;
        }

        public CreditAccountRequestBuilder setCreationDateTime(LocalDateTime creationDateTime) {
            this.creationDateTime = creationDateTime;
            return this;
        }

        public CreditAccountRequestBuilder setTerm(Period term) {
            this.term = term;
            return this;
        }

        public CreditAccountRequestBuilder setApprovedValidity(LocalDate approvedValidity) {
            this.approvedValidity = approvedValidity;
            return this;
        }

        public CreditAccountRequestBuilder setStatus(CreditRequestStatus status) {
            this.status = status;
            return this;
        }

        public CreditAccountRequest build() {
            CreditAccountRequest request = new CreditAccountRequest();
            request.id = id;
            request.client = client;
            request.amount = amount;
            request.approvedAmount = approvedAmount;
            request.creationDateTime = creationDateTime;
            request.term = term;
            request.approvedValidity = approvedValidity;
            request.status = status;
            return request;
        }
    }
}
