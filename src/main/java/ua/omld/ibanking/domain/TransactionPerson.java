/*
 * TransactionPerson.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

import java.util.Objects;

/**
 * Represents person info of bank transaction:
 * - juridical person;
 * - natural person;
 */
public class TransactionPerson {

    String name;
    String personCode;
    String bankAccountNumber;
    String bankCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersonCode() {
        return personCode;
    }

    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionPerson that = (TransactionPerson) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(personCode, that.personCode) &&
                Objects.equals(bankAccountNumber, that.bankAccountNumber) &&
                Objects.equals(bankCode, that.bankCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, personCode, bankAccountNumber, bankCode);
    }
}
