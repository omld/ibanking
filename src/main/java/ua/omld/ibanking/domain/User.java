/*
 * User.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Represents user login information
 */
public class User implements Entity<Long> {

    private Long id;
    private String email;
    private String password;
    private LocalDate registrationDate;
    private LocalDateTime passwordChangeDateTime;
    private Role role;
    private Client client;

    private User() {
        id = 0L;
        email = "";
        password = "";
        registrationDate = LocalDate.now();
        role = Role.CLIENT;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {

        this.password = password;
        setPasswordChangeDateTime(LocalDateTime.now());
    }

    private void setPasswordChangeDateTime(LocalDateTime passwordChangeDateTime) {
        this.passwordChangeDateTime = passwordChangeDateTime;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public LocalDateTime getPasswordChangeDateTime() {
        return passwordChangeDateTime;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(email, user.email) &&
                Objects.equals(password, user.password) &&
                Objects.equals(registrationDate, user.registrationDate) &&
                Objects.equals(passwordChangeDateTime, user.passwordChangeDateTime) &&
                Objects.equals(role, user.role) &&
                Objects.equals(client, user.client);
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return this.getClass().hashCode()
                + Objects.hashCode(id)
                + 3 * Objects.hashCode(email)
                + 5 * Objects.hashCode(password)
                + 7 * Objects.hashCode(registrationDate)
                + 9 * Objects.hashCode(passwordChangeDateTime)
                + 11 * Objects.hashCode(role)
                + 13 * Objects.hashCode(client);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", registrationDate=" + registrationDate +
                ", passwordChangeDateTime=" + passwordChangeDateTime +
                ", role=" + role +
                '}';
    }

    public static UserBuilder getBuilder(){
        return new UserBuilder();
    }

    /**
     * Builds User object
     */
    public static final class UserBuilder {
        private Long id;
        private String email;
        private String password;
        private LocalDate registrationDate;
        private LocalDateTime passwordChangeDateTime;
        private Role role;
        private Client client;

        private UserBuilder() {
        }

        public UserBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public UserBuilder setEmail(String email) {
            this.email = email;
            return this;
        }

        public UserBuilder setPassword(String password) {
            this.password = password;
            return this;
        }

        public UserBuilder setRegistrationDate(LocalDate registrationDate) {
            this.registrationDate = registrationDate;
            return this;
        }

        public UserBuilder setPasswordChangeDateTime(LocalDateTime passwordChangeDateTime) {
            this.passwordChangeDateTime = passwordChangeDateTime;
            return this;
        }

        public UserBuilder setRole(Role role) {
            this.role = role;
            return this;
        }

        public UserBuilder setClient(Client client) {
            this.client = client;
            return this;
        }

        public User build() {
            User user = new User();
            user.id = id;
            user.email = email;
            user.password = password;
            user.registrationDate = registrationDate;
            user.passwordChangeDateTime = passwordChangeDateTime;
            user.role = role;
            user.client = client;
            return user;
        }
    }
}
