/*
 * CreditRequestStatus.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

public enum CreditRequestStatus {

    NEW ("creditRequest.status.new"),
    ACCEPTED ("creditRequest.status.accepted"),
    REJECTED ("creditRequest.status.rejected");

    private String status;

    CreditRequestStatus(String status) {
        this.status = status;
    }

    public String getStatusString() {
        return status;
    }
}
