/*
 * BankAccount.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Base class for any bank account
 */
public abstract class BankAccount implements Entity<Long> {

    private static final String NUMBER_FORMAT = "%010d";

    private Long id;
    private BigDecimal balance;
    private BigDecimal interestRate;
    private LocalDate validityDate;
    private List<Transaction> transactions;

    protected BankAccount() {
        id = 0L;
        balance = BigDecimal.ZERO;
        interestRate = BigDecimal.ZERO;
        validityDate = LocalDate.MIN;
        transactions = new ArrayList<>();
    }

    protected BankAccount(Long id, BigDecimal interestRate, LocalDate validityDate) {
        this();
        this.id = id;
        this.interestRate = interestRate;
        this.validityDate = validityDate;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public LocalDate getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(LocalDate validityDate) {
        this.validityDate = validityDate;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public boolean addOperation(Transaction operation) {
        return this.transactions.add(operation);
    }

    public String getNumber() {
        return String.format(NUMBER_FORMAT, id);
    }

    /**
     * Put amount on account
     * @param amount amount
     * @return true on success
     */
    public abstract boolean replenish(BigDecimal amount);

    /**
     * Take amount from account
     * @param amount amount
     * @return true on success
     */
    public abstract boolean withdraw(BigDecimal amount);

    /**
     * Checks possibility to withdrawal given amount from account
     * @param amount amount for check
     * @return true if amount can be withdrawal
     */
    public abstract boolean isAmountValidForWithdrawal(BigDecimal amount);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BankAccount)) return false;
        if (getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(balance, that.balance) &&
                Objects.equals(interestRate, that.interestRate) &&
                Objects.equals(validityDate, that.validityDate);
    }

    @Override
    public int hashCode() {
        return this.getClass().hashCode()
                + Objects.hashCode(id)
                + 3 * Objects.hashCode(balance)
                + 5 * Objects.hashCode(interestRate)
                + 7 * Objects.hashCode(validityDate);
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "id=" + id +
                ", balance=" + balance +
                ", interestRate=" + interestRate +
                ", validityDate=" + validityDate +
                '}';
    }

    /**
     * Builds supertype BankAccount object
     */
    protected abstract static class BankAccountBuilder<T extends BankAccountBuilder<T>> {

        private BankAccount bankAccount;
        private Long id;
        private BigDecimal balance;
        private BigDecimal interestRate;
        private LocalDate validityDate;
        private List<Transaction> transactions;

        BankAccountBuilder() {
        }

        protected void setBankAccount(BankAccount bankAccount) {
            this.bankAccount = bankAccount;
        }

        public T setId(Long id) {
            if (id != null && id.compareTo(0L) <= 0) {
                throw new IllegalArgumentException("Bank account number must be positive.");
            }
            this.id = id;
            return self();
        }

        public T setBalance(BigDecimal balance) {
            this.balance = balance;
            return self();
        }

        public T setInterestRate(BigDecimal interestRate) {
            if (interestRate.compareTo(BigDecimal.ZERO) < 0) {
                throw new IllegalArgumentException("Interest rate must be non-negative.");
            }
            this.interestRate = interestRate;
            return self();
        }

        public T setValidityDate(LocalDate validityDate) {
            this.validityDate = validityDate;
            return self();
        }

        public T setTransactions(List<Transaction> transactions) {
            this.transactions = transactions;
            return self();
        }

        protected abstract T self();

        BankAccount build() {

            bankAccount.id = id;
            bankAccount.balance = balance;
            bankAccount.interestRate = interestRate;
            bankAccount.validityDate = validityDate;
            bankAccount.transactions = new ArrayList<>();
            if (transactions != null) {
                bankAccount.transactions.addAll(transactions);
            }
            return bankAccount;
        }
    }
}