/*
 * TransactionType.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

/**
 * Transaction type
 */
public enum TransactionType {

    TRANSFER, PAYMENT, REPLENISH, WITHDRAWAL
}
