/*
 * Entity.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

import java.io.Serializable;

/**
 * Provides interface for all Entities
 */
public interface Entity<ID extends Serializable> extends Serializable {
    /**
     * Returns ID of Entity
     * @return Entity ID
     */
    ID getId();

    /**
     * Sets ID of Entity
     * @param id Entity ID
     */
    void setId(final ID id);
}
