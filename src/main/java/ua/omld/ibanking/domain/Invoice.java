/*
 * Invoice.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

import java.math.BigDecimal;

public class Invoice {

    private TransactionPerson beneficiary;
    private BigDecimal amount;
    private String description;

    public Invoice() {
        beneficiary = new TransactionPerson();
    }

    public TransactionPerson getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(TransactionPerson beneficiary) {
        this.beneficiary = beneficiary;
    }

    public String getBeneficiaryName() {
        return beneficiary.name;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiary.name = beneficiaryName;
    }

    public String getBeneficiaryCode() {
        return beneficiary.personCode;
    }

    public void setBeneficiaryCode(String beneficiaryCode) {
        this.beneficiary.personCode = beneficiaryCode;
    }

    public String getBankCode() {
        return beneficiary.bankCode;
    }

    public void setBankCode(String bankCode) {
        this.beneficiary.bankCode = bankCode;
    }

    public String getBankAccount() {
        return beneficiary.bankAccountNumber;
    }

    public void setBankAccount(String bankAccount) {
        this.beneficiary.bankAccountNumber = bankAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
