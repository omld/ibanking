/*
 * CreditAccount.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * Represent credit bank account
 */
public class CreditAccount extends BankAccount {

    private BigDecimal creditLimit;
    private BigDecimal currentDebt;
    private BigDecimal amountOfAccruedInterest;

    private CreditAccount() {
    }

    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }

    public BigDecimal getCurrentDebt() {
        return currentDebt;
    }

    public void setCurrentDebt(BigDecimal currentDebt) {
        this.currentDebt = currentDebt;
    }

    public BigDecimal getAmountOfAccruedInterest() {
        return amountOfAccruedInterest;
    }

    public void setAmountOfAccruedInterest(BigDecimal amountOfAccruedInterest) {
        this.amountOfAccruedInterest = amountOfAccruedInterest;
    }

    @Override
    public boolean withdraw(BigDecimal amount) {
        if (isAmountValidForWithdrawal(amount)) {
            boolean noCredit = creditNotInUse();
            setBalance(getBalance().subtract(amount));
            if (noCredit) {
                BigDecimal toDebt = creditLimit.subtract(getBalance());
                if ( toDebt.compareTo(BigDecimal.ZERO) > 0 ) {
                    currentDebt = currentDebt.add(toDebt);
                }
            } else {
                currentDebt = currentDebt.add(amount);
            }
            return true;
        }
        return false;
    }

    private boolean creditNotInUse() {
        return getBalance().compareTo(creditLimit) > 0;
    }

    @Override
    public boolean replenish(BigDecimal amount) {
        BigDecimal oldDebt = currentDebt;
        BigDecimal oldInterest = amountOfAccruedInterest;
        BigDecimal oldBalance = getBalance();
        BigDecimal toBalance = payAccruedInterest(amount);

        currentDebt = currentDebt.subtract(toBalance);
        if (currentDebt.compareTo(BigDecimal.ZERO) < 0) {
            currentDebt = BigDecimal.ZERO;
        }
        setBalance(getBalance().add(toBalance));

        if ( checkBalance() ) {
            return true;
        }
        setBalance(oldBalance);
        currentDebt = oldDebt;
        amountOfAccruedInterest = oldInterest;
        return false;
    }

    private BigDecimal payAccruedInterest(BigDecimal amount) {
        BigDecimal result = amount;
        if (amountOfAccruedInterest.compareTo(BigDecimal.ZERO) > 0) {
            if (amount.compareTo(amountOfAccruedInterest) > 0) {
                result = amount.subtract(amountOfAccruedInterest);
                amountOfAccruedInterest = BigDecimal.ZERO;
            } else {
                result = BigDecimal.ZERO;
                amountOfAccruedInterest = amountOfAccruedInterest.subtract(amount);
            }
        }
        return result;
    }

    private boolean checkBalance() {
        return  getBalance().compareTo( creditLimit.subtract(currentDebt) ) >= 0;
    }

    @Override
    public boolean isAmountValidForWithdrawal(BigDecimal amount) {
        int compare = getBalance().compareTo(amount);
        return compare >= 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CreditAccount that = (CreditAccount) o;
        return Objects.equals(creditLimit, that.creditLimit) &&
                Objects.equals(currentDebt, that.currentDebt) &&
                Objects.equals(amountOfAccruedInterest, that.amountOfAccruedInterest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), creditLimit, currentDebt, amountOfAccruedInterest);
    }

    @Override
    public String toString() {
        return "CreditAccount: "+ super.toString() +
                ", {" +
                "creditLimit=" + creditLimit +
                ", currentDebt=" + currentDebt +
                ", amountOfAccruedInterest=" + amountOfAccruedInterest +
                "} " ;
    }

    public static CreditAccountBuilder getBuilder(){
        return new CreditAccountBuilder();
    }

    /**
     * Builds DepositAccount object
     */
    public static final class CreditAccountBuilder extends BankAccountBuilder<CreditAccountBuilder> {

        private BigDecimal creditLimit;
        private BigDecimal currentDebt;
        private BigDecimal amountOfAccruedInterest;

        private CreditAccountBuilder() {
            currentDebt = BigDecimal.ZERO;
            amountOfAccruedInterest = BigDecimal.ZERO;
        }

        public CreditAccountBuilder setCreditLimit(BigDecimal creditLimit) {
            this.creditLimit = creditLimit;
            return this;
        }

        public CreditAccountBuilder setCurrentDebt(BigDecimal currentDebt) {
            this.currentDebt = currentDebt;
            return this;
        }

        public CreditAccountBuilder setAmountOfAccruedInterest(BigDecimal amountOfAccruedInterest) {
            this.amountOfAccruedInterest = amountOfAccruedInterest;
            return this;
        }

        @Override
        protected CreditAccountBuilder self() {
            return this;
        }

        public CreditAccount build() {
            CreditAccount creditAccount = new CreditAccount();
            super.setBankAccount(creditAccount);
            super.build();
            creditAccount.creditLimit = creditLimit;
            creditAccount.currentDebt = currentDebt;
            creditAccount.amountOfAccruedInterest = amountOfAccruedInterest;
            return creditAccount;
        }
    }

}
