/*
 * TransactionDirection.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

/**
 * Transaction direction according to bank account
 */
public enum TransactionDirection {

    INPUT, OUTPUT
}
