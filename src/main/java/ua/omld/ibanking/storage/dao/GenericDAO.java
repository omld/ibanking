/*
 * GenericDAO.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao;

import ua.omld.ibanking.domain.Entity;
import ua.omld.ibanking.storage.exception.DAOException;

import java.io.Serializable;
import java.util.List;

/**
 * Provides interface for Generic DAO
 */
public interface GenericDAO<K extends Serializable, T extends Entity> extends AutoCloseable {

    /**
     * Inserts Entity to data storage.
     *
     * @param entity Entity object to create
     * @return stored object
     */
    K create(T entity) throws DAOException;

    /**
     * Deletes Entity with specified ID from data storage.
     *
     * @param ID ID of Entity object to delete
     * @return true if deleted
     */
    boolean deleteByID(K ID) throws DAOException ;

    /**
     * Updates Entity in data storage.
     *
     * @param entity Entity object to update
     * @return updated object
     */
    boolean update(T entity) throws DAOException ;

    /**
     * Finds Entity in data storage by ID.
     *
     * @param ID id of Entity to find
     * @return found Entity
     */
    T findById(K ID) throws DAOException ;

    /**
     * Returns all Entities of such type.
     *
     * @return list of found Entities
     */
    List<T> getAll() throws DAOException ;

    @Override
    void close();
}
