/*
 * MySqlTransactionDAO.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao.mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.domain.Transaction;
import ua.omld.ibanking.domain.TransactionStatus;
import ua.omld.ibanking.domain.TransactionType;
import ua.omld.ibanking.storage.dao.TransactionDAO;
import ua.omld.ibanking.storage.exception.DAOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static ua.omld.ibanking.storage.dao.jdbc.JDBCUtils.localDateTimeToSqlTimestamp;

public class MySqlTransactionDAO extends MySqlAbstractDAO<Long, Transaction> implements TransactionDAO {

    static Logger LOGGER = LogManager.getLogger();

    private static final String DEFAULT_SORT = "`CREATION_DATETIME` DESC";
    private static final String SQL_PRED_ACCOUNT_LIMIT = "ID IN (SELECT TRANSACTION_ID FROM `BankAccounts_Transactions` " +
            "WHERE `ACCOUNT_ID` = ?) " + SQL_ORDER + DEFAULT_SORT + SQL_LIMIT;
    private static final String SQL_PRED_ACCOUNT_PERIOD = "ID IN (SELECT TRANSACTION_ID FROM `BankAccounts_Transactions` " +
            "WHERE `ACCOUNT_ID` = ?) AND (`CREATION_DATETIME` >= ? ) AND (`CREATION_DATETIME` <= ? )"  +
            SQL_ORDER + DEFAULT_SORT;

    {
        TABLE = "Transactions";
        SQL_COLUMNS = new String[] { "CREATION_DATETIME", "EXECUTION_DATETIME", "AMOUNT", "FEE",
                "SENDER_NAME", "SENDER_CODE", "SENDER_BANK_CODE", "SENDER_BANK_ACCOUNT",
                "BENEFICIARY_NAME", "BENEFICIARY_CODE", "BENEFICIARY_BANK_CODE", "BENEFICIARY_BANK_ACCOUNT",
                "DESCRIPTION", "STATUS", "TYPE" };
        SQL_DEFAULT_SORT = DEFAULT_SORT;
    }

    MySqlTransactionDAO(Connection connection, boolean closeConnection) {
        super(connection, closeConnection);
    }

    @Override
    public List<Transaction> getLastTransactionsByAccountId(Long accountId, int count) throws DAOException {

        String sqlQuery = selectAllQuery() + SQL_WHERE + SQL_PRED_ACCOUNT_LIMIT;
        return  findByCriteria(sqlQuery, accountId, count);
    }

    @Override
    public List<Transaction> getTransactionsByAccountIdForPeriod(Long accountId, LocalDate from, LocalDate to) throws DAOException {

        String sqlQuery = selectAllQuery() + SQL_WHERE + SQL_PRED_ACCOUNT_PERIOD;
        return  findByCriteria(sqlQuery, accountId, from.atStartOfDay(), to.plusDays(1).atStartOfDay());
    }

    @Override
    public boolean assignToBankAccount(Long transactionId, Long accountId) throws DAOException {

        String updateQuery = SQL_INS + "`BankAccounts_Transactions` (`ACCOUNT_ID`, `TRANSACTION_ID`)" + SQL_VALUES + "(?, ?)";
        int affectedRows = updateByCriteria(updateQuery, accountId, transactionId);
        if (affectedRows != 1) {
            throw new DAOException("Error assign transaction (" + transactionId + ") to bank account (" + accountId + ")") ;
        }
        return true;
    }

    @Override
    protected Object[] getValuesArray(Transaction entity) {

        return new Object[] { localDateTimeToSqlTimestamp(entity.getCreationDateTime()),
                localDateTimeToSqlTimestamp(entity.getExecutionDateTime()), entity.getAmount(), entity.getFee(),
                entity.getSenderName(), entity.getSenderCode(), entity.getSenderBankCode(),
                entity.getSenderBankAccountNumber(), entity.getBeneficiaryName(), entity.getBeneficiaryCode(),
                entity.getBeneficiaryBankCode(), entity.getBeneficiaryBankAccountNumber(), entity.getDescription(),
                entity.getStatus().toString(), entity.getType().toString()
        };
    }

    @Override
    protected List<Transaction> parseResultSet(ResultSet rs) throws DAOException {

        List<Transaction> result = new ArrayList<>();
        try {
            while (rs.next()) {
                LocalDateTime executionDateTime = null;
                Timestamp executionTimestamp = rs.getTimestamp("EXECUTION_DATETIME");
                if (Objects.nonNull(executionTimestamp)) {
                    executionDateTime = executionTimestamp.toLocalDateTime();
                }
                result.add( Transaction.getBuilder()
                        .setId(rs.getLong("ID"))
                        .setCreationDateTime(rs.getTimestamp("CREATION_DATETIME").toLocalDateTime())
                        .setExecutionDateTime(executionDateTime)
                        .setAmount(rs.getBigDecimal("AMOUNT"))
                        .setFee(rs.getBigDecimal("FEE"))
                        .setSenderName(rs.getString("SENDER_NAME"))
                        .setSenderCode(rs.getString("SENDER_CODE"))
                        .setSenderBankCode(rs.getString("SENDER_BANK_CODE"))
                        .setSenderBankAccount(rs.getString("SENDER_BANK_ACCOUNT"))
                        .setBeneficiaryName(rs.getString("BENEFICIARY_NAME"))
                        .setBeneficiaryCode(rs.getString("BENEFICIARY_CODE"))
                        .setBeneficiaryBankCode(rs.getString("BENEFICIARY_BANK_CODE"))
                        .setBeneficiaryBankAccount(rs.getString("BENEFICIARY_BANK_ACCOUNT"))
                        .setDescription(rs.getString("DESCRIPTION"))
                        .setStatus(TransactionStatus.valueOf(rs.getString("STATUS")))
                        .setType(TransactionType.valueOf(rs.getString("TYPE")))
                        .build());
            }
        } catch (Exception e) {
            LOGGER.error("Error parsing Transaction result set: " + e.getMessage());
            throw new DAOException(e);
        }
        return result;
    }
}
