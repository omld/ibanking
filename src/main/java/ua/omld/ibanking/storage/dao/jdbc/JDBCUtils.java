/*
 * JDBCUtils.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao.jdbc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

/**
 * Utility class for JDBC operations.
 */
public final class JDBCUtils {

    private static final Logger LOGGER = LogManager.getLogger();

    private JDBCUtils() {}

    /**
     * Prepare sql statement from the the given SQL query, parameter values and using given connection.
     * Returns a PreparedStatement.
     *
     * @param connection The Connection to create the PreparedStatement from.
     * @param sql The SQL query to construct the PreparedStatement with.
     * @param returnGeneratedKeys Set whether to return generated keys or not.
     * @param values The parameter values to be set in the created PreparedStatement.
     * @throws SQLException If something fails during creating the PreparedStatement.
     */
    public static PreparedStatement prepareStatement (Connection connection, String sql, boolean returnGeneratedKeys,
                                                      Object... values) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(sql,
                returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
        setValues(statement, values);
        return statement;
    }

    /**
     * Set the given parameter values in the given PreparedStatement.
     *
     * @param statement The PreparedStatement to set the given parameter values in.
     * @param values The parameter values to be set in the created PreparedStatement.
     * @throws SQLException If something fails during setting the PreparedStatement values.
     */
    private static void setValues(PreparedStatement statement, Object... values) throws SQLException {
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                LOGGER.trace("Setting value " + i + " : " + values[i]);
                statement.setObject(i + 1, values[i]);
            }
        }
    }

    /**
     * Converts the given java.time.LocalDate to java.sql.Date.
     *
     * @param date The java.time.LocalDate to be converted to java.sql.Date.
     * @return The converted java.sql.Date.
     */
    public static Date localDateToSqlDate(java.time.LocalDate date) {
        return (date != null) ? Date.valueOf(date) : null;
    }

    /**
     * Converts the given java.time.LocalDateTime to java.sql.Date.
     *
     * @param dateTime The java.time.LocalDateTime to be converted to java.sql.Date.
     * @return The converted java.sql.Timestamp.
     */
    public static Timestamp localDateTimeToSqlTimestamp(java.time.LocalDateTime dateTime) {
        return (dateTime != null) ? Timestamp.valueOf(dateTime) : null;
    }
}