/*
 * MySqlUserDAO.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao.mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.domain.Role;
import ua.omld.ibanking.domain.User;
import ua.omld.ibanking.storage.dao.UserDAO;
import ua.omld.ibanking.storage.exception.DAOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import static ua.omld.ibanking.storage.dao.jdbc.JDBCUtils.localDateTimeToSqlTimestamp;
import static ua.omld.ibanking.storage.dao.jdbc.JDBCUtils.localDateToSqlDate;

public class MySqlUserDAO extends MySqlAbstractDAO<Long, User> implements UserDAO {

    static Logger LOGGER = LogManager.getLogger();

    private static final String SQL_PRED_EMAIL = "EMAIL = ?";

    {
        TABLE = "Users";
        SQL_COLUMNS = new String[] { "EMAIL", "PASSWORD", "REG_DATE", "PASS_CHANGE_DATETIME", "ROLE", "CLIENT_ID" };
    }

    MySqlUserDAO(Connection connection, boolean closeConnection) {
        super(connection, closeConnection);
    }

    @Override
    public User findByEmail(String email) throws DAOException {

        String sqlQuery = selectAllQuery() + SQL_WHERE + SQL_PRED_EMAIL;
        List<User> foundUsers = findByCriteria(sqlQuery, email);
        return extractSingleton(foundUsers);
    }

    @Override
    protected Object[] getValuesArray(User entity) {

        return new Object[] {entity.getEmail(), entity.getPassword(), localDateToSqlDate(entity.getRegistrationDate()),
                localDateTimeToSqlTimestamp(entity.getPasswordChangeDateTime()), entity.getRole().toString(),
                entity.getClient().getId()
        };
    }

    @Override
    protected List<User> parseResultSet(ResultSet rs) throws DAOException {

        List<User> result = new ArrayList<>();
        try {
            while (rs.next()) {
                result.add( User.getBuilder()
                        .setId(rs.getLong("ID"))
                        .setEmail(rs.getString("EMAIL"))
                        .setPassword(rs.getString("PASSWORD"))
                        .setRegistrationDate(rs.getDate("REG_DATE").toLocalDate())
                        .setPasswordChangeDateTime(rs.getTimestamp("PASS_CHANGE_DATETIME").toLocalDateTime())
                        .setRole(Role.valueOf(rs.getString("ROLE")))
                        .build());
            }
        } catch (Exception e) {
            LOGGER.error("Error parsing Users result set: " + e.getMessage());
            throw new DAOException(e);
        }
        return result;
    }
}
