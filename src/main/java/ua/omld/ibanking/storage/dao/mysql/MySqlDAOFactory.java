/*
 * MySqlDAOFactory.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao.mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.storage.connection.ConnectionWrapper;
import ua.omld.ibanking.storage.connection.jdbc.JDBCConnectionWrapper;
import ua.omld.ibanking.storage.connection.jdbc.JDBCConnector;
import ua.omld.ibanking.storage.dao.*;
import ua.omld.ibanking.storage.exception.DAOException;
import ua.omld.ibanking.storage.exception.StorageConnectorException;
import ua.omld.ibanking.storage.exception.StorageFactoryException;

import java.sql.Connection;

public class MySqlDAOFactory extends DAOFactory<Connection> {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String ERR_CONNECTOR = "Cannot initialize datasource.";

    private static volatile MySqlDAOFactory instance;

    private MySqlDAOFactory() throws StorageFactoryException {
        try {
            storageConnector = JDBCConnector.getInstance();
        } catch (StorageConnectorException scEx) {
            LOGGER.error(scEx.getMessage());
            throw new StorageFactoryException(ERR_CONNECTOR + " " + scEx.getLocalizedMessage());
        }
    }

    public static MySqlDAOFactory getInstance() throws StorageFactoryException {

        if (instance == null) {
            LOGGER.trace("MySqlDAOFactory: making instance.");
            synchronized (MySqlDAOFactory.class) {
                if (instance == null) {
                    instance = new MySqlDAOFactory();
                }
            }
        }
        return instance;
    }

    @Override
    public ConnectionWrapper getConnection() throws DAOException {
        try {
            return new JDBCConnectionWrapper(storageConnector.getConnection());
        } catch (StorageConnectorException scEx) {
            LOGGER.error("Can't get connection");
            throw new DAOException(scEx);
        }
    }

    @Override
    public BankAccountDAO createBankAccountDAO() {
        try {
            return new MySqlBankAccountDAO(storageConnector.getConnection(), true);
        } catch (StorageConnectorException scEx) {
            return null;
        }
    }

    @Override
    public BankAccountDAO createBankAccountDAO(ConnectionWrapper wrapper) {
        return new MySqlBankAccountDAO(extractConnection(wrapper), true);
    }

    @Override
    public ClientDAO createClientDAO() {
        try {
            return new MySqlClientDAO(storageConnector.getConnection(), true);
        } catch (StorageConnectorException scEx) {
            return null;
        }
    }

    @Override
    public ClientDAO createClientDAO(ConnectionWrapper wrapper) {
        return new MySqlClientDAO(extractConnection(wrapper), true);
    }

    @Override
    public CreditAccountRequestDAO createCreditAccountRequestDAO() {
        try {
            return new MySqlCreditAccountRequestDAO(storageConnector.getConnection(), true);
        } catch (StorageConnectorException scEx) {
            return null;
        }
    }

    @Override
    public CreditAccountRequestDAO createCreditAccountRequestDAO(ConnectionWrapper wrapper) {
        return new MySqlCreditAccountRequestDAO(extractConnection(wrapper), true);
    }

    @Override
    public UserDAO createUserDAO() {
        try {
            return new MySqlUserDAO(storageConnector.getConnection(), true);
        } catch (StorageConnectorException scEx) {
            return null;
        }
    }

    @Override
    public UserDAO createUserDAO(ConnectionWrapper wrapper) {
        return new MySqlUserDAO(extractConnection(wrapper), true);
    }

    @Override
    public TransactionDAO createTransactionDAO() {
        try {
            return new MySqlTransactionDAO(storageConnector.getConnection(), true);
        } catch (StorageConnectorException scEx) {
            return null;
        }
    }

    @Override
    public TransactionDAO createTransactionDAO(ConnectionWrapper wrapper) {
        return new MySqlTransactionDAO(extractConnection(wrapper), true);
    }

    private Connection extractConnection(ConnectionWrapper connectionWrapper) {

        JDBCConnectionWrapper jdbcConnectionWrapper = (JDBCConnectionWrapper) connectionWrapper;
        return jdbcConnectionWrapper.getConnection();
    }
}
