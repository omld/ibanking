/*
 * BankAccountDAO.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao;

import ua.omld.ibanking.domain.BankAccount;
import ua.omld.ibanking.storage.exception.DAOException;

import java.util.List;

public interface BankAccountDAO extends GenericDAO<Long, BankAccount> {

    List<BankAccount> findBankAccountsByClientId(Long clientId) throws DAOException;

    boolean assignBankAccountToClient(Long accountId, Long clientId) throws DAOException;
}
