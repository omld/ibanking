/*
 * JDBCConstants.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao.jdbc;

public interface JDBCConstants {
    interface Errors {
        String ENTITY_ID_NOT_NULL = "Entity is already created, the ID is not null.";
        String INSERT_ERROR = "Entity is not persisted: ID is not generated.";
        String DELETE_ERROR = "Delete error: affected records count: ";
        String UPDATE_ERROR = "Entity is not updated.";
        String DUPLICATE_ENTITY = "Duplicate Entities found.";
    }
}
