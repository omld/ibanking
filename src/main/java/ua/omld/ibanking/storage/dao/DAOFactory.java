/*
 * DAOFactory.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.storage.connection.ConnectionWrapper;
import ua.omld.ibanking.storage.connection.StorageConnector;
import ua.omld.ibanking.storage.dao.mysql.MySqlDAOFactory;
import ua.omld.ibanking.storage.exception.DAOException;
import ua.omld.ibanking.storage.exception.StorageConnectorException;
import ua.omld.ibanking.storage.exception.StorageFactoryException;

/**
 * Abstract factory for DAO.
 */
public abstract class DAOFactory<Context> {

    private static final String STORAGE_ERROR = "Storage type mismatch.";
    private static final Logger LOGGER = LogManager.getLogger();

    protected StorageConnector<Context> storageConnector;

    public abstract ConnectionWrapper getConnection() throws DAOException;

    public abstract BankAccountDAO createBankAccountDAO();

    public abstract BankAccountDAO createBankAccountDAO(ConnectionWrapper wrapper);

    public abstract ClientDAO createClientDAO();

    public abstract ClientDAO createClientDAO(ConnectionWrapper wrapper);

    public abstract CreditAccountRequestDAO createCreditAccountRequestDAO();

    public abstract CreditAccountRequestDAO createCreditAccountRequestDAO(ConnectionWrapper wrapper);

    public abstract UserDAO createUserDAO();

    public abstract UserDAO createUserDAO(ConnectionWrapper wrapper);

    public abstract TransactionDAO createTransactionDAO();

    public abstract TransactionDAO createTransactionDAO(ConnectionWrapper wrapper);

    /**
     * Enum for DAO types supported by the factory
     */
    public enum FactoryType {
        MYSQL
    }

    /**
     * Returns concrete DAO factory according to given factory type
     *
     * @return concrete DAOFactory
     */
    public static DAOFactory getDAOFactory(String factoryType) throws StorageFactoryException {

        FactoryType factorySwitch = FactoryType.valueOf(factoryType.toUpperCase());

        switch (factorySwitch) {
            case MYSQL:
                return MySqlDAOFactory.getInstance();
            default:
                LOGGER.error(STORAGE_ERROR + " - " + factoryType.toUpperCase());
                throw new StorageFactoryException(STORAGE_ERROR);
        }
    }
}