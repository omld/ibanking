/*
 * ClientDAO.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao;

import ua.omld.ibanking.domain.Client;
import ua.omld.ibanking.storage.exception.DAOException;

public interface ClientDAO extends GenericDAO<Long, Client> {

    Client findByUserId(Long userId) throws DAOException;

    Client findByBankAccountId(Long bankAccountId) throws DAOException;

    Client findByCreditRequestId(Long creditRequestId) throws DAOException;
}
