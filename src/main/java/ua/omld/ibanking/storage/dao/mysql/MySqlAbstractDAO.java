/*
 * MySqlAbstractDAO.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao.mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.domain.Entity;
import ua.omld.ibanking.storage.dao.GenericDAO;
import ua.omld.ibanking.storage.exception.ConnectionException;
import ua.omld.ibanking.storage.exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ua.omld.ibanking.storage.dao.jdbc.JDBCUtils.prepareStatement;
import static ua.omld.ibanking.storage.dao.jdbc.JDBCConstants.Errors.*;

public abstract class MySqlAbstractDAO<K extends Long, T extends Entity> implements GenericDAO<Long, T> {

    static Logger LOGGER = LogManager.getLogger();

    private static final String SQL_SEL = "SELECT ";
    private static final String SQL_FROM = " FROM ";
    private static final String SQL_DEL = "DELETE ";
    private static final String SQL_PRED_ID = "ID = ?";

    protected static final String SQL_INS = "INSERT INTO ";
    protected static final String SQL_VALUES = " VALUES ";
    protected static final String SQL_UPD = "UPDATE ";
    protected static final String SQL_SET = " SET ";
    protected static final String SQL_WHERE = " WHERE ";
    protected static final String SQL_ORDER = " ORDER BY ";
    protected static final String SQL_LIMIT = " LIMIT ?";
    protected static final String SQL_ID_COLUMN = "ID, ";

    protected String TABLE = "";
    protected String[] SQL_COLUMNS = {};
    protected String SQL_DEFAULT_SORT = "";

    protected Connection connection;
    private boolean closeConnection;

    MySqlAbstractDAO(Connection connection, boolean closeConnection) {
        this.connection = connection;
        this.closeConnection = closeConnection;
    }

    /**
     * Returns SQL query to get all records, without sort rules.
     * SELECT [column, column, ...] FROM [Table]
     */
    protected String selectAllQuery() {
        return SQL_SEL + columnsWithIdString() + SQL_FROM + TABLE;
    }

    private String columnsString() {
        String columns = Arrays.toString(SQL_COLUMNS);
        return columns.substring(1, columns.length()-1);
    }

    protected String columnsWithIdString() {
        return SQL_ID_COLUMN + columnsString();
    }

    /**
     * Returns SQL query to insert new record.
     * INSERT INTO [Table] ([column, column, ...]) VALUES (?, ?, ...);
     */
    protected String insertQuery() {
        return SQL_INS + TABLE + " (" + columnsString() + ")" + SQL_VALUES + insertValuesString();
    }

    private String insertValuesString() {
        StringBuilder valuesSB = new StringBuilder("(");
        for (int i = 0; i < SQL_COLUMNS.length-1; i++) {
            valuesSB.append("?, ");
        }
        valuesSB.append("?)");
        return valuesSB.toString();
    }

    /**
     * Returns SQL query to update existing record.
     * UPDATE [Table] SET [column = ?, column = ?, ...] WHERE id = ?;
     */
    protected String updateQuery() {
        return SQL_UPD + TABLE + SQL_SET + columnsUpdateString() + SQL_WHERE + SQL_PRED_ID;
    }

    private String columnsUpdateString() {
        StringBuilder columnsUpdateSB = new StringBuilder();
        for (String column: SQL_COLUMNS) {
            columnsUpdateSB.append(column).append(" = ?, ");
        }
        columnsUpdateSB.delete(columnsUpdateSB.length()-2, columnsUpdateSB.length());
        return columnsUpdateSB.toString();
    }

    /**
     * Returns SQL query to delete record from database.
     * <p/>
     * DELETE FROM [Table] WHERE id= ?;
     */
    public String deleteQuery() {
        return SQL_DEL + TABLE + SQL_WHERE + SQL_PRED_ID;
    }

    /**
     * Returns properties of Entity as array of objects that needed to be set in insert/update statement.
     *
     * @return properties of Entity mapped to array of objects
     */
    protected abstract Object[] getValuesArray(T entity);

    /**
     * Parses ResultSet and returns list of corresponding entities.
     */
    protected abstract List<T> parseResultSet(ResultSet rs) throws DAOException;

    @Override
    public Long create(T entity) throws DAOException {

        if (entity.getId() != null && !entity.getId().equals(0) ) {
            throw new IllegalArgumentException(ENTITY_ID_NOT_NULL);
        }
        String sql = insertQuery();
        Object[] entityValues = getValuesArray(entity);
        try (PreparedStatement statement = prepareStatement(connection, sql, true, entityValues)) {
            statement.executeUpdate();
            ResultSet idRS = statement.getGeneratedKeys();
            if (idRS.next()) {
                return idRS.getLong(1);
            }
            throw new DAOException(INSERT_ERROR);
        } catch (SQLException sqlEx) {
            getLOGGER().error("Create error: " + sqlEx.getLocalizedMessage() + ".   FULL SQL statement: " + sql);
            throw new DAOException(sqlEx);
        }
    }

    @Override
    public boolean deleteByID(Long id) throws DAOException {

        String sql = deleteQuery();
        try (PreparedStatement statement = prepareStatement(connection, sql, false, id)) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new DAOException(DELETE_ERROR + affectedRows);
            }
            return true;
        } catch (SQLException sqlEx) {
            getLOGGER().error("Delete error: " + sqlEx.getLocalizedMessage() + ".   FULL SQL statement: " + sql);
            throw new DAOException(sqlEx);
        }
    }

    @Override
    public boolean update(T entity) throws DAOException {

        String sql = updateQuery();
        ArrayList<Object> entityValues = new ArrayList<>(Arrays.asList(getValuesArray(entity)));
        entityValues.add(entity.getId());
        int affectedRows = updateByCriteria(sql, entityValues.toArray());
        if (affectedRows != 1) {
            throw new DAOException(UPDATE_ERROR + entity.toString());
        }
        return true;
    }

    /**
     * Makes update/insert query that match specified criteria without generating keys.
     *
     * @param sql SQL-Statement
     * @param criteria update/insert criteria
     * @return count of updated records
     */
    protected int updateByCriteria(String sql, Object ... criteria) throws DAOException {
        int count = -1;
        try (PreparedStatement statement = prepareStatement(connection, sql, false, criteria)) {
            count = statement.executeUpdate();
        } catch (SQLException sqlEx) {
            getLOGGER().error("Update by criteria error: " + sqlEx.getLocalizedMessage() + ".   FULL SQL statement: " + sql);
            throw new DAOException(sqlEx);
        }
        return count;
    }

    @Override
    public T findById(Long id) throws DAOException {

        String sql = selectAllQuery() + SQL_WHERE + SQL_PRED_ID;
        List<T> entities = findByCriteria(sql, id);
        return extractSingleton(entities);
    }

    /**
     * Returns Entities that match specified criteria.
     *
     * @param sql SQL-Statement
     * @param criteria search criteria
     * @return List of found Entities
     */
    protected List<T> findByCriteria(String sql, Object ... criteria) throws DAOException {
        List<T> entities;
        try (PreparedStatement statement = prepareStatement(connection, sql, false, criteria);
             ResultSet resultSet = statement.executeQuery()) {
            entities = parseResultSet(resultSet);
        } catch (SQLException sqlEx) {
            getLOGGER().error("Find by criteria error: " + sqlEx.getLocalizedMessage() + ".   FULL SQL statement: " + sql);
            throw new DAOException(sqlEx);
        }
        return entities;
    }

    /**
     * Checks that there is only one entity in list and returns it. Returns null if list is empty.
     * @param entities list of entities to check
     * @return singleton entity
     * @throws DAOException if more then one entities in list.
     */
    protected T extractSingleton(List<T> entities) throws DAOException {
        if (entities.size() > 1) {
            throw new DAOException(DUPLICATE_ENTITY);
        }
        return entities.isEmpty() ? null : entities.get(0);
    }

    @Override
    public List<T> getAll() throws DAOException {

        List<T> entities;
        String sql = selectAllQuery() + (SQL_DEFAULT_SORT.length() > 0 ? SQL_ORDER + SQL_DEFAULT_SORT : "");

        try (PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {
            entities = parseResultSet(resultSet);
            return entities;
        } catch (SQLException ex) {
            getLOGGER().error("Get all error: " + ex.getLocalizedMessage() + ".   FULL SQL statement: " + sql);
            throw new DAOException(ex);
        }
    }

    @Override
    public void close() {

        if(closeConnection) {
            try {
                connection.close();
            } catch (SQLException e){
                getLOGGER().error("Failed to close connection ", e);
                throw new ConnectionException(e);
            }
        }
    }

    protected Logger getLOGGER() {
        return this.LOGGER;
    }
}
