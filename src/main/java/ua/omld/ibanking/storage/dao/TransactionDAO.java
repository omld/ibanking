/*
 * TransactionDAO.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao;

import ua.omld.ibanking.domain.Transaction;
import ua.omld.ibanking.storage.exception.DAOException;

import java.time.LocalDate;
import java.util.List;

public interface TransactionDAO extends GenericDAO<Long, Transaction> {

    List<Transaction> getLastTransactionsByAccountId(Long accountId, int count) throws DAOException;

    List<Transaction> getTransactionsByAccountIdForPeriod(Long accountId, LocalDate from, LocalDate to) throws DAOException;

    boolean assignToBankAccount(Long transactionId, Long accountId) throws DAOException;
}
