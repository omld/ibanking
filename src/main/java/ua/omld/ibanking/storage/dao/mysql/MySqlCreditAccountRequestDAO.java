/*
 * MySqlCreditAccountRequestDAO.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao.mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.domain.Client;
import ua.omld.ibanking.domain.CreditAccountRequest;
import ua.omld.ibanking.domain.CreditRequestStatus;
import ua.omld.ibanking.storage.dao.CreditAccountRequestDAO;
import ua.omld.ibanking.storage.exception.DAOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static ua.omld.ibanking.storage.dao.jdbc.JDBCUtils.localDateTimeToSqlTimestamp;
import static ua.omld.ibanking.storage.dao.jdbc.JDBCUtils.localDateToSqlDate;

public class MySqlCreditAccountRequestDAO extends MySqlAbstractDAO<Long, CreditAccountRequest> implements CreditAccountRequestDAO {

    static Logger LOGGER = LogManager.getLogger();

    private static final String DEFAULT_SORT = "`CREATION_DATETIME` DESC";
    private static final String SQL_PRED_BY_CLIENT = "CLIENT_ID = ?" + SQL_ORDER + DEFAULT_SORT;
    private static final String SQL_PRED_STATUS_NEW = "STATUS = 'NEW'" + SQL_ORDER + DEFAULT_SORT;

    {
        TABLE = "CreditAccountRequests";
        SQL_COLUMNS = new String[] { "AMOUNT", "APPROVED_AMOUNT", "CREATION_DATETIME", "TERM",
                "APPROVED_VALIDITY", "STATUS", "CLIENT_ID" };
        SQL_DEFAULT_SORT = DEFAULT_SORT;
    }

    MySqlCreditAccountRequestDAO(Connection connection, boolean closeConnection) {
        super(connection, closeConnection);
    }

    @Override
    public List<CreditAccountRequest> findAllRequestsByClientId(Long clientId) throws DAOException {

        String sqlQuery = selectAllQuery() + SQL_WHERE + SQL_PRED_BY_CLIENT;
        return  findByCriteria(sqlQuery, clientId);
    }

    @Override
    public List<CreditAccountRequest> findNewRequests() throws DAOException {

        String sqlQuery = selectAllQuery() + SQL_WHERE + SQL_PRED_STATUS_NEW;
        return  findByCriteria(sqlQuery);
    }

    @Override
    protected Object[] getValuesArray(CreditAccountRequest entity) {

        return new Object[] { entity.getAmount(), entity.getApprovedAmount(),
                localDateTimeToSqlTimestamp(entity.getCreationDateTime()),
                entity.getTerm().toTotalMonths(), localDateToSqlDate(entity.getApprovedValidity()),
                entity.getStatus().toString(), entity.getClient().getId() };
    }

    @Override
    protected List<CreditAccountRequest> parseResultSet(ResultSet rs) throws DAOException {

        List<CreditAccountRequest> result = new ArrayList<>();
        try {
            while (rs.next()) {
                LocalDate approvedValidity = null;
                Timestamp validityTimestamp = rs.getTimestamp("APPROVED_VALIDITY");
                if (Objects.nonNull(validityTimestamp)) {
                    approvedValidity= validityTimestamp.toLocalDateTime().toLocalDate();
                }
                Client client = Client.getBuilder()
                        .setId(rs.getLong("CLIENT_ID"))
                        .build();
                result.add( CreditAccountRequest.getBuilder()
                        .setId(rs.getLong("ID"))
                        .setAmount(rs.getBigDecimal("AMOUNT"))
                        .setApprovedAmount(rs.getBigDecimal("APPROVED_AMOUNT"))
                        .setCreationDateTime(rs.getTimestamp("CREATION_DATETIME").toLocalDateTime())
                        .setTerm(Period.ofMonths(rs.getInt("TERM")))
                        .setApprovedValidity(approvedValidity)
                        .setStatus(CreditRequestStatus.valueOf(rs.getString("STATUS")))
                        .setClient(client)
                        .build());
            }
        } catch (Exception e) {
            LOGGER.error("Error parsing CreditAccountRequests result set: " + e.getMessage());
            throw new DAOException(e);
        }
        return result;
    }
}
