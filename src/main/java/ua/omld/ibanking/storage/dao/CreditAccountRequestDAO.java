/*
 * CreditAccountRequestDAO.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao;

import ua.omld.ibanking.domain.CreditAccountRequest;
import ua.omld.ibanking.storage.exception.DAOException;

import java.util.List;

public interface CreditAccountRequestDAO extends GenericDAO<Long, CreditAccountRequest> {

    List<CreditAccountRequest> findAllRequestsByClientId(Long clientId) throws DAOException;

    List<CreditAccountRequest> findNewRequests() throws DAOException;
}
