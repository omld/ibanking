/*
 * UserDAO.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao;

import ua.omld.ibanking.domain.User;
import ua.omld.ibanking.storage.exception.DAOException;

/**
 * DAO for User Entity
 */
public interface UserDAO extends GenericDAO<Long, User> {
    /**
     * Find User by E-Mail in data storage.
     *
     * @param email email of User
     * @return found User
     */
    User findByEmail(String email) throws DAOException;

}
