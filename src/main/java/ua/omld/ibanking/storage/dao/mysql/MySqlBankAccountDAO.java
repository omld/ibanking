/*
 * MySqlBankAccountDAO.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao.mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.domain.BankAccount;
import ua.omld.ibanking.domain.CreditAccount;
import ua.omld.ibanking.domain.DepositAccount;
import ua.omld.ibanking.storage.dao.BankAccountDAO;
import ua.omld.ibanking.storage.exception.DAOException;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static ua.omld.ibanking.storage.dao.jdbc.JDBCUtils.localDateToSqlDate;

public class MySqlBankAccountDAO extends MySqlAbstractDAO<Long, BankAccount> implements BankAccountDAO {

    static Logger LOGGER = LogManager.getLogger();

    private static final String SQL_PRED_CLIENT = "CLIENT_ID = ?";

    {
        TABLE = "BankAccounts";
        SQL_COLUMNS = new String[] { "TYPE", "BALANCE", "INTEREST_RATE", "VALIDITY_DATE",
                "CREDIT_LIMIT", "CURRENT_DEBT", "ACCRUED_INTEREST" };
        SQL_DEFAULT_SORT = "'ID'";
    }

    MySqlBankAccountDAO(Connection connection, boolean closeConnection) {
        super(connection, closeConnection);
    }

    @Override
    public List<BankAccount> findBankAccountsByClientId(Long clientId) throws DAOException {

        String sqlQuery = selectAllQuery() + SQL_WHERE + SQL_PRED_CLIENT;
        return  findByCriteria(sqlQuery, clientId);
    }

    @Override
    public boolean assignBankAccountToClient(Long accountId, Long clientId) throws DAOException {

        String updateQuery = SQL_UPD + "`BankAccounts`" + SQL_SET + "`CLIENT_ID` = ?" + SQL_WHERE + "ID = ?";
        int affectedRows = updateByCriteria(updateQuery, clientId, accountId);
        if (affectedRows != 1) {
            throw new DAOException("Error assign bank account (" + accountId + ") to client (" + clientId + ")") ;
        }
        return true;
    }

    @Override
    protected Object[] getValuesArray(BankAccount entity) {
        String type = getAccountTypeString(entity);
        BigDecimal creditLimit = null;
        BigDecimal currentDebt = null;
        BigDecimal amountOfAccruedInterest = null;
        if (entity instanceof CreditAccount) {
            CreditAccount creditEntity = (CreditAccount) entity;
            creditLimit = creditEntity.getCreditLimit();
            currentDebt = creditEntity.getCurrentDebt();
            amountOfAccruedInterest = creditEntity.getAmountOfAccruedInterest();
        }
        return new Object[] {type, entity.getBalance(), entity.getInterestRate(),
                localDateToSqlDate(entity.getValidityDate()),
                creditLimit, currentDebt, amountOfAccruedInterest
        };
    }

    private String getAccountTypeString(BankAccount account) {

        String className = account.getClass().getSimpleName();
        int aIndex = className.lastIndexOf("Account");
        return className.toUpperCase().substring(0, aIndex);
    }

    @Override
    protected List<BankAccount> parseResultSet(ResultSet rs) throws DAOException {

        List<BankAccount> result = new ArrayList<>();
        try {
            while (rs.next()) {
                result.add( parseBankAccount(rs) );
            }
        } catch (Exception e) {
            LOGGER.error("Error parsing BankAccounts result set: " + e.getMessage());
            throw new DAOException(e);
        }
        return result;
    }

    private BankAccount parseBankAccount(ResultSet rs) throws SQLException {

        String type = rs.getString("TYPE");
        Long id = rs.getLong("ID");
        BigDecimal balance = rs.getBigDecimal("BALANCE");
        BigDecimal interestRate = rs.getBigDecimal("INTEREST_RATE");
        LocalDate validity = rs.getDate("VALIDITY_DATE").toLocalDate();
        BigDecimal creditLimit = rs.getBigDecimal("CREDIT_LIMIT");
        BigDecimal currentDebt = rs.getBigDecimal("CURRENT_DEBT");
        BigDecimal amountOfAccruedInterest = rs.getBigDecimal("ACCRUED_INTEREST");

        if (type.equals("DEPOSIT")) {
            return buildDeposit(id, balance, interestRate, validity);
        } else if (type.equals("CREDIT")) {
            return buildCredit(id, balance, interestRate, validity, creditLimit, currentDebt, amountOfAccruedInterest);
        }
        return null;
    }

    private DepositAccount buildDeposit(Long id, BigDecimal balance, BigDecimal interestRate, LocalDate validity) {

        return DepositAccount.getBuilder()
                .setId(id)
                .setBalance(balance)
                .setInterestRate(interestRate)
                .setValidityDate(validity)
                .build();
    }

    private CreditAccount buildCredit(Long id, BigDecimal balance, BigDecimal interestRate, LocalDate validity,
                                      BigDecimal creditLimit, BigDecimal currentDebt, BigDecimal amountOfAccruedInterest) {

        return CreditAccount.getBuilder()
                .setId(id)
                .setBalance(balance)
                .setInterestRate(interestRate)
                .setValidityDate(validity)
                .setCreditLimit(creditLimit)
                .setCurrentDebt(currentDebt)
                .setAmountOfAccruedInterest(amountOfAccruedInterest)
                .build();
    }
}
