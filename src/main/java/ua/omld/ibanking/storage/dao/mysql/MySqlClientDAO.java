/*
 * MySqlClientDAO.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao.mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.domain.Client;
import ua.omld.ibanking.storage.dao.ClientDAO;
import ua.omld.ibanking.storage.exception.DAOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import static ua.omld.ibanking.storage.dao.jdbc.JDBCUtils.localDateToSqlDate;

public class MySqlClientDAO extends MySqlAbstractDAO<Long, Client> implements ClientDAO {

    static Logger LOGGER = LogManager.getLogger();

    private static final String SQL_PRED_BY_USER = " ID = (SELECT CLIENT_ID FROM `Users` U WHERE U.ID = ?)";
    private static final String SQL_PRED_BY_BANK_ACCOUNT = " ID = (SELECT CLIENT_ID FROM `BankAccounts` BA WHERE BA.ID = ?)";
    private static final String SQL_PRED_BY_CREDIT_REQUEST = " ID = (SELECT CLIENT_ID FROM `CreditAccountRequests` CR WHERE CR.ID = ?)";

    {
        TABLE = "Clients";
        SQL_COLUMNS = new String[] { "NAME", "SURNAME", "PATRONYMIC", "BIRTH_DATE", "TAX_NUMBER" };
    }

    MySqlClientDAO(Connection connection, boolean closeConnection) {
        super(connection, closeConnection);
    }

    @Override
    public Client findByUserId(Long userId) throws DAOException {

        String sqlQuery = selectAllQuery() + SQL_WHERE + SQL_PRED_BY_USER;
        List<Client> foundClients = findByCriteria(sqlQuery, userId);
        return extractSingleton(foundClients);
    }

    @Override
    public Client findByBankAccountId(Long bankAccountId) throws DAOException {

        String sqlQuery = selectAllQuery() + SQL_WHERE + SQL_PRED_BY_BANK_ACCOUNT;
        List<Client> foundClients = findByCriteria(sqlQuery, bankAccountId);
        return extractSingleton(foundClients);
    }

    @Override
    public Client findByCreditRequestId(Long creditRequestId) throws DAOException {

        String sqlQuery = selectAllQuery() + SQL_WHERE + SQL_PRED_BY_CREDIT_REQUEST;
        List<Client> foundClients = findByCriteria(sqlQuery, creditRequestId);
        return extractSingleton(foundClients);
    }

    @Override
    protected Object[] getValuesArray(Client entity) {

        return new Object[] {entity.getName(), entity.getSurname(), entity.getPatronymic(),
                localDateToSqlDate(entity.getBirthDate()),
                entity.getTaxNumber()
        };
    }
    @Override
    protected List<Client> parseResultSet(ResultSet rs) throws DAOException {

        List<Client> result = new ArrayList<>();
        try {
            while (rs.next()) {
                result.add( Client.getBuilder()
                        .setId(rs.getLong("ID"))
                        .setName(rs.getString("NAME"))
                        .setSurname(rs.getString("SURNAME"))
                        .setPatronymic(rs.getString("PATRONYMIC"))
                        .setBirthDate(rs.getDate("BIRTH_DATE").toLocalDate())
                        .setTaxNumber(rs.getString("TAX_NUMBER"))
                        .build());
            }
        } catch (Exception e) {
            LOGGER.error("Error parsing Clients result set: " + e.getMessage());
            throw new DAOException(e);
        }
        return result;
    }

}
