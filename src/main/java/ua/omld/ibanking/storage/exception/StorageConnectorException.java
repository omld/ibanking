/*
 * StorageConnectorException.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.exception;

/**
 * Represents exception thrown during getting access to Data Storage
 */
public class StorageConnectorException extends Exception {

    public StorageConnectorException(String message) {
        super(message);
    }

    public StorageConnectorException(String message, Throwable cause) {
        super(message, cause);
    }

    public StorageConnectorException(Throwable cause) {
        super(cause);
    }
}
