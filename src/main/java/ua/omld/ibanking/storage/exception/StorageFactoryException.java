/*
 * StorageFactoryException.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.exception;

/**
 * Exception to be thrown when problem with storage factory appears.
 */
public class StorageFactoryException extends Exception {

    public StorageFactoryException(String message) {
        super(message);
    }

    public StorageFactoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
