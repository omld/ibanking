/*
 * JDBCStorageConnector.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.connection.jdbc;

import org.apache.commons.dbcp2.*;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.storage.connection.StorageConnector;
import ua.omld.ibanking.storage.exception.StorageConnectorException;
import ua.omld.ibanking.utils.ConfigurationManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class JDBCConnector implements StorageConnector<Connection> {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String URL_PATTERN = "jdbc";
    private static final String BAD_CONN_URL = "Bad connection URL: not JDBC.";

    private static volatile JDBCConnector instance;

    private DataSource dataSource;
    private ConfigurationManager configurationManager;

    private JDBCConnector() throws StorageConnectorException {
        try {
            configurationManager = ConfigurationManager.getInstance();
            Properties connectionProperties = getConnectionProperties();
            LOGGER.debug("Connection parameters loaded.");

            if (!connectionProperties.getProperty("url").toLowerCase().contains(URL_PATTERN)) {
                throw new StorageConnectorException(BAD_CONN_URL);
            }
            dataSource = setupDataSource(connectionProperties);
        } catch (RuntimeException rtEx) {
            LOGGER.debug("Can't initialize JDBCConnector. " + rtEx.getMessage());
            throw new StorageConnectorException(rtEx);
        }
    }

    private Properties getConnectionProperties() {

        Properties connectionProperties = new Properties();
        connectionProperties.setProperty("url", configurationManager.getDBUrl());
        connectionProperties.setProperty("driver", configurationManager.getDBDriver());
        connectionProperties.setProperty("user", configurationManager.getDBUser());
        connectionProperties.setProperty("password", configurationManager.getDBPassword());

        return  connectionProperties;
    }

    private DataSource setupDataSource(Properties properties) throws StorageConnectorException {
        try {
            Class.forName(properties.getProperty("driver"));

            // ConnectionFactory that the pool will use to create Connections.
            ConnectionFactory connectionFactory =
                    new DriverManagerConnectionFactory(properties.getProperty("url"), properties);

            // PoolableConnectionFactory, which wraps the "real" Connections created by
            // the ConnectionFactory with the classes that implement the pooling functionality.
            PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory, null);

            // ObjectPool that serves as the actual pool of connections.
            ObjectPool<PoolableConnection> connectionPool = new GenericObjectPool<>(poolableConnectionFactory);
            poolableConnectionFactory.setPool(connectionPool);

            // Create the PoolingDriver itself, passing in the object pool.
            PoolingDataSource<PoolableConnection> dataSource = new PoolingDataSource<>(connectionPool);

            return dataSource;
        } catch (ClassNotFoundException ex) {
            throw new StorageConnectorException(ex);
        }
    }

    public static JDBCConnector getInstance() throws StorageConnectorException {

        if (instance == null) {
            synchronized (JDBCConnector.class) {
                if (instance == null) {
                    instance = new JDBCConnector();
                }
            }
        }
        return instance;
    }

    /**
     * Returns connection to database over JDBC
     *
     * @return connection to database
     */
    @Override
    public Connection getConnection() throws StorageConnectorException {

        try {
            return dataSource.getConnection();
        } catch (SQLException sqlEx) {
            LOGGER.error("Error get connection: " + sqlEx);
            throw new StorageConnectorException(sqlEx);
        }
    }
}
