/*
 * JDBCConnectionWrapper.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.connection.jdbc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.storage.connection.ConnectionWrapper;
import ua.omld.ibanking.storage.exception.ConnectionException;

import java.sql.Connection;
import java.sql.SQLException;

public class JDBCConnectionWrapper implements ConnectionWrapper {

    private static final Logger LOGGER = LogManager.getLogger();
    private Connection connection;
    private boolean transactionStarted = false;

    public JDBCConnectionWrapper(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void startTransaction() {

        try{
            connection.setAutoCommit(false);
            transactionStarted = true;
            LOGGER.debug("Transaction started...");
        } catch (SQLException e){
            LOGGER.error("Error while beginning transaction ", e);
            throw new ConnectionException(e);
        }
    }

    @Override
    public void commitTransaction() {

        try{
            connection.commit();
            connection.setAutoCommit(true);
            transactionStarted = false;
            LOGGER.debug("Transaction was committed...");
        } catch (SQLException e) {
            LOGGER.error("Error while committing transaction ", e);
            throw new ConnectionException(e);
        }
    }

    @Override
    public void rollbackTransaction() {

        try{
            connection.rollback();
            connection.setAutoCommit(true);
            transactionStarted = false;
            LOGGER.debug("Transaction was rolled back... ");
        } catch (SQLException e) {
            LOGGER.error("Error while rolling back transaction ", e);
            throw new ConnectionException(e);
        }
    }

    @Override
    public void close() {

        try {
            if (transactionStarted) {
                rollbackTransaction();
            }
            connection.close();
            LOGGER.debug("Connection was closed...");
        } catch (SQLException e) {
            LOGGER.error("Error while closing transaction ", e);
            throw new ConnectionException(e);
        }
    }

    public Connection getConnection() {
        LOGGER.trace("In getConnection()");
        return connection;
    }
}
