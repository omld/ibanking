/*
 * StorageConnector.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.connection;

import ua.omld.ibanking.storage.exception.StorageConnectorException;

/**
 * Provides interface for entry point to data storage.
 * Concrete StorageConnector must be Singleton.
 *
 * @version 1.00
 */
public interface StorageConnector<Context> {

    /**
     * Returns connection to current data storage.
     *
     * @return connection to current data storage
     */
    Context getConnection() throws StorageConnectorException;
}
