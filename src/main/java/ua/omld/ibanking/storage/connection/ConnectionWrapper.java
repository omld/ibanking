/*
 * ConnectionWrapper.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.connection;

/**
 * Interface to wrapper of data source connection
 */
public interface ConnectionWrapper extends AutoCloseable {

    void startTransaction();

    void commitTransaction();

    void rollbackTransaction();

    void close();
}
