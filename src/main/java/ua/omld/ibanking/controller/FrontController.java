/*
 * FrontController.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.command.ActionCommandsFactory;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.view.ViewConstants.*;

import java.util.Arrays;


public class FrontController {

    private static final Logger LOGGER = LogManager.getLogger();
    private ActionCommandsFactory commandsFactory;

    private FrontController() {
        commandsFactory = ActionCommandsFactory.getInstance();
    }

    private static class InstanceHolder {
        private static final FrontController INSTANCE = new FrontController();
    }

    public static FrontController getInstance (){
        return InstanceHolder.INSTANCE;
    }

    public String processRequest(HttpModel httpModel) {

        setPaths(httpModel);
        try {
            ActionCommand command = commandsFactory.defineCommand(httpModel.getAction(), httpModel.getHttpMethod());
            LOGGER.debug("Defined Action command: " + command);
            return command.execute(httpModel);
        } catch (RuntimeException rtEx) {
            LOGGER.error("Error executing command: " + rtEx.getMessage() );
            LOGGER.error("Error StackTrace: \n" + Arrays.toString(rtEx.getStackTrace()) );
            return JSP.Errors.GLOBAL;
        }
    }

    private void setPaths(HttpModel httpModel) {

        httpModel.setRequestAttribute("adminPath", URLPaths.PREFIX_ADMIN);
        httpModel.setRequestAttribute("clientPath", URLPaths.PREFIX_CLIENT);
    }

}
