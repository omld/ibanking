/*
 * Utils.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Non-instantiable utility class.
 */
public class Utils {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String DEFAULT_LANG = "en";
    private static final String DEFAULT_COUNTRY = "US";

    private Utils() {
        throw new AssertionError();
    }

    /**
     * Makes {@link Locale} object according to given string.
     * If given string do not match any locale returns default locale.
     *
     * @param localeStr string representing locale
     * @return Locale object
     */
    public static Locale getLocale(String localeStr) {

        Locale locale = null;
        LOGGER.debug("localeStr = " + localeStr);
        if ( (localeStr != null) && !localeStr.isEmpty() ) {
            List<String> localeElements = new ArrayList<>();
            if (localeStr.contains("-")) {
                localeElements.addAll(Arrays.asList(localeStr.split("-")));
            } else if (localeStr.contains("_")) {
                localeElements.addAll(Arrays.asList(localeStr.split("_")));
            } else {
                localeElements.add(localeStr);
            }

            LOGGER.debug("localeElements = " + Arrays.toString(localeElements.toArray()));
            switch (localeElements.size()) {
                case 1:
                    locale = new Locale(localeElements.get(0));
                    break;
                case 2:
                    locale = new Locale(localeElements.get(0), localeElements.get(1));
                    break;
                case 3:
                    locale = new Locale(localeElements.get(0), localeElements.get(1), localeElements.get(2));
                    break;
            }
        }
        if (locale == null) {
            locale = new Locale(DEFAULT_LANG, DEFAULT_COUNTRY);
        }
        return locale;
    }

    public static Locale getLocale(String localeStr, String reserveLocaleStr) {

        String localeString = (localeStr != null) ? localeStr : reserveLocaleStr;
        return getLocale(localeString);
    }
}
