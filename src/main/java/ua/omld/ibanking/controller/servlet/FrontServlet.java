/*
 * FrontServlet.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.controller.FrontController;
import ua.omld.ibanking.view.ViewConstants.URLPaths;
import ua.omld.ibanking.view.ViewConstants.Fields;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.Objects;


@WebServlet(name = "FrontServlet", urlPatterns = {"/"})
public class FrontServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger();
    private FrontController controller;

    @Override
    public void init() throws ServletException {
        controller = FrontController.getInstance();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(HttpMethod.POST, req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(HttpMethod.GET, req, resp);
    }

    private void processRequest(HttpMethod method, HttpServletRequest request,
                                HttpServletResponse response)
            throws ServletException, IOException {

        HttpModel httpModel = getPreparedHttpModel(method, request, response);
        String page = controller.processRequest(httpModel);

        // post processing
        for (Map.Entry<String, Object> attributes : httpModel.getRequestAttributes().entrySet()) {
            request.setAttribute(attributes.getKey(), attributes.getValue());
        }

        // choose right response
        switch (httpModel.getResponseType()) {
            case FORWARD:
                LOGGER.debug("Forwarding to JSP: " + page);
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
                dispatcher.forward(request, response);
                break;
            case REDIRECT:
                LOGGER.debug("Redirect to path: " + page);
                page = prepareRedirectAddress(page);
                response.sendRedirect(page);
                break;
            case ERR_404:
                response.sendError(404);
                break;
            default:
                response.sendRedirect(URLPaths.MAIN);
                break;
        }
    }

    private HttpModel getPreparedHttpModel(HttpMethod method, HttpServletRequest request, HttpServletResponse response) {

        HttpModel httpModel = new HttpModel();
        httpModel.setHttpMethod(method);
        httpModel.setHttpSession(request.getSession());
        httpModel.setRequestParameters(request.getParameterMap());
        httpModel.setAction(getActionString(request));
        return httpModel;
    }

    // extract action string from request
    private String getActionString(HttpServletRequest request) {

        String action = request.getRequestURI();
        return action.replace(request.getContextPath(), "");
    }

    // if path is real URL it will be returned without changes
    // else it will be formatted to the relative URL
    private String prepareRedirectAddress(String pageURL) {
        try {
            URL url = new URL(pageURL);
            url.toURI();
        } catch (MalformedURLException | URISyntaxException ex) {
            pageURL = getServletContext().getContextPath() + pageURL;
        }
        return pageURL;
    }
}
