/*
 * HttpMethod.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.servlet;

/**
 * Enum for HTTP Method
 */
public enum HttpMethod {
    GET, POST
}
