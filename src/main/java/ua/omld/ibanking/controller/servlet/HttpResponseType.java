/*
 * HttpResponseType.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.servlet;

/**
 * Type of HTTP response
 */
public enum HttpResponseType {
    FORWARD, REDIRECT, ERR_404
}
