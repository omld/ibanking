/*
 * HttpModel.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.servlet;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * Contains objects needed to execute commands and return data to Front-end.
 */
public class HttpModel {
    private HttpMethod httpMethod;
    private HttpSession httpSession;
    private String action;
    private HttpResponseType responseType;
    private HttpServletResponse response;

    private Map<String, Object> requestAttributes = new HashMap<>();
    private Map<String, String[]> requestParameters = new HashMap<>();

    public HttpModel() {
        // default response
        responseType = HttpResponseType.FORWARD;
    }

    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(HttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    public HttpSession getHttpSession() {
        return httpSession;
    }

    public void setHttpSession(HttpSession httpSession) {
        this.httpSession = httpSession;
    }

    public Object getSessionAttribute(String name){
        return httpSession.getAttribute(name);
    }

    public void setSessionAttribute(String name, Object value){
        httpSession.setAttribute(name, value);
    }

    public void removeSessionAttribute(String name) {
        httpSession.removeAttribute(name);
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public HttpResponseType getResponseType() {
        return responseType;
    }

    public void setResponseType(HttpResponseType responseType) {
        this.responseType = responseType;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    public Map<String, Object> getRequestAttributes() {
        return requestAttributes;
    }

    public Object getRequestAttribute(String key) {
        return requestAttributes.get(key);
    }

    public void setRequestAttribute(String key, Object value) {
        requestAttributes.put(key, value);
    }

    public void setRequestAttributes(Map<String, Object> requestAttributes) {
        this.requestAttributes = requestAttributes;
    }

    public String getRequestParameter(String key) {
        String result = null;
        String[] params = requestParameters.get(key);
        if (params != null && params.length > 0) {
            result = params[0];
        }
        return result;
    }

    public Map<String, String[]> getRequestParameters() {
        return requestParameters;
    }

    public void setRequestParameters(Map<String, String[]> requestParameters) {
        this.requestParameters = requestParameters;
    }

}
