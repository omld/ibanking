/*
 * LocalizationFilter.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.controller.Utils;
import ua.omld.ibanking.view.ViewConstants;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

/**
 * Filter setting locale for user session
 */
@WebFilter(filterName = "LocalizationFilter", urlPatterns = {"/langChange/*"} )
public class LocalizationFilter implements Filter {

    private static final Logger LOGGER = LogManager.getLogger();

    private static final String HEADER_PREVIOUS = "referer";
    private FilterConfig filterConfig;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        LOGGER.debug("LocalizationFilter: doFilter()");

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession httpSession = request.getSession();

        LOGGER.debug("User desired locale: " + request.getParameter(ViewConstants.Parameters.LOCALE));

        Locale clientLocale = Utils.getLocale( request.getParameter(ViewConstants.Parameters.LOCALE),
                filterConfig.getServletContext().getInitParameter(ViewConstants.SysParameters.DEFAULT_LOCALE));
        httpSession.setAttribute(ViewConstants.Attributes.LOCALE, clientLocale.toLanguageTag());

        LOGGER.debug("Retrieved locale: " + clientLocale);

        String previousURL = request.getHeader(HEADER_PREVIOUS);
        response.sendRedirect(previousURL);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.debug("init LocalizationFilter");
        this.filterConfig = filterConfig;
    }

    @Override
    public void destroy() {
        this.filterConfig = null;
    }
}
