/*
 * SecurityFilter.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.filter;


import ua.omld.ibanking.domain.Role;
import ua.omld.ibanking.domain.User;
import ua.omld.ibanking.view.ViewConstants.URLPaths;
import ua.omld.ibanking.view.ViewConstants.Attributes;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Filter for detecting user type
 */
@WebFilter(filterName = "SecurityFilter", urlPatterns = {"/*"})
public class SecurityFilter implements Filter {

    private Set<String> allUsersUri = new HashSet<>();
    private Set<String> resourcesUri = new HashSet<>();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession httpSession = request.getSession();

        User user = (User) httpSession.getAttribute(Attributes.USER);
        String path = request.getServletPath();

        if ( uriMustBeFiltered(path) ) {
            if (Objects.isNull(user)) {
                response.sendRedirect(request.getContextPath() + URLPaths.LOGIN);
                return;
            } else if ( isClientWrongPath(user, path) ) {
                response.sendRedirect( request.getContextPath() + URLPaths.Client.DASHBOARD);
                return;
            } else if ( isAdminWrongPath(user, path) ) {
                response.sendRedirect( request.getContextPath() + URLPaths.Admin.DASHBOARD);
                return;
            } else if ( isBadRole(user) ) {
                httpSession.invalidate();
                response.sendRedirect( request.getContextPath() + URLPaths.MAIN);
                return;
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private boolean uriMustBeFiltered(String uri) {

        return !isResource(uri) && !allUsersUri.contains(uri);
    }

    private boolean isResource(String path) {

        boolean result = false;
        for (String uri: resourcesUri) {
            if (path.startsWith(uri)) {
                result = true;
            }
        }
        return result;
    }

    private boolean isClientWrongPath (User user, String path) {

        return user.getRole() == Role.CLIENT && !path.startsWith(URLPaths.PREFIX_CLIENT);
    }

    private boolean isAdminWrongPath (User user, String path) {

        return user.getRole() == Role.ADMIN && !path.startsWith(URLPaths.PREFIX_ADMIN);
    }

    private boolean isBadRole (User user) {

        return (user.getRole() != Role.ADMIN) && (user.getRole() != Role.CLIENT);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

        allUsersUri.add(URLPaths.LOGIN);
        allUsersUri.add(URLPaths.LOGOUT);
        allUsersUri.add(URLPaths.ERROR);
        allUsersUri.add(URLPaths.ERR404);

        resourcesUri.add(URLPaths.PREFIX_CSS);
        resourcesUri.add(URLPaths.PREFIX_JS);
    }

    @Override
    public void destroy() {

        allUsersUri = null;
        resourcesUri = null;
    }
}
