/*
 * SessionListener.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.listener;

import ua.omld.ibanking.controller.Utils;
import ua.omld.ibanking.view.ViewConstants;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Locale;

/**
 * Initializes default locale in user session.
 */
@WebListener
public class SessionListener implements ServletContextListener, HttpSessionListener {

    private static String DEFAULT_CONTEXT_LOCALE = "";

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        DEFAULT_CONTEXT_LOCALE =
                sce.getServletContext().getInitParameter(ViewConstants.SysParameters.DEFAULT_LOCALE);
    }

    @Override
    public void sessionCreated(HttpSessionEvent se) {

        initLocale(se.getSession());
    }

    private void initLocale(HttpSession httpSession) {

        Locale clientLocale = Utils.getLocale( DEFAULT_CONTEXT_LOCALE);
        httpSession.setAttribute(ViewConstants.Attributes.LOCALE, clientLocale.toLanguageTag());
    }
}
