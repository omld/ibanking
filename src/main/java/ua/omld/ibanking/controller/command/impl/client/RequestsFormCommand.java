/*
 * RequestsFormCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl.client;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.domain.*;
import ua.omld.ibanking.service.AccountRequestService;
import ua.omld.ibanking.service.BankAccountService;
import ua.omld.ibanking.service.ServiceFactory;
import ua.omld.ibanking.service.exception.BankAccountNotFoundException;
import ua.omld.ibanking.service.exception.SystemFaultException;
import ua.omld.ibanking.view.ViewConstants.*;

import java.util.List;

/**
 * Returns payments form
 */
public class RequestsFormCommand implements ActionCommand {

    private BankAccountService bankAccountService = ServiceFactory.getBankAccountService();
    private AccountRequestService accountRequestService = ServiceFactory.getAccountRequestService();

    @Override
    public String execute(HttpModel httpModel) {

        Client client = ((User) httpModel.getSessionAttribute(Attributes.USER)).getClient();
        try {
            if (creditIsNotPresent(client, httpModel)) {
                List<CreditAccountRequest> requests = accountRequestService.getAllRequestsForClient(client.getId());
                httpModel.setRequestAttribute(Attributes.REQUESTS, requests);
            }
        } catch (SystemFaultException sfEx) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
        }
        return JSP.Client.REQUESTS;
    }

    private boolean creditIsNotPresent(Client client, HttpModel httpModel) {

        boolean creditNotPresent = true;
        try {
            List<BankAccount> bankAccounts = bankAccountService.findAccountsByClient(client);
            if (bankAccounts.parallelStream().anyMatch(bankAccount -> bankAccount instanceof CreditAccount)) {
                creditNotPresent = false;
            }
            httpModel.setSessionAttribute(Attributes.CREDIT_PRESENT, !creditNotPresent);
        } catch (BankAccountNotFoundException nfEx) {
            httpModel.setRequestAttribute(Attributes.MESSAGES, Errors.ACCOUNT_NOT_FOUND);
        }
        return creditNotPresent;
    }
}
