/*
 * TransactionInfoCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl.client;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.domain.Transaction;
import ua.omld.ibanking.service.ServiceFactory;
import ua.omld.ibanking.service.TransactionsService;
import ua.omld.ibanking.service.exception.SystemFaultException;
import ua.omld.ibanking.view.ViewConstants.*;

/**
 * Returns information for given transaction
 */
public class TransactionInfoCommand implements ActionCommand {

    private TransactionsService transactionsService = ServiceFactory.getTransactionsService();

    @Override
    public String execute(HttpModel httpModel) {

        try {
            Long id = Long.valueOf(httpModel.getRequestParameter(Parameters.ID));
            Long accountId = Long.valueOf(httpModel.getRequestParameter(Parameters.ACCOUNT_ID));
            Transaction transaction = transactionsService.getTransactionById(id, accountId);
            httpModel.setRequestAttribute(Attributes.TRANSACTION, transaction);
        } catch (NumberFormatException nfEx) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
        } catch (SystemFaultException sfEx) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.TRANSACTION_NOT_FOUND);
        }
        return JSP.Client.TRANSACTION_INFO;
    }
}
