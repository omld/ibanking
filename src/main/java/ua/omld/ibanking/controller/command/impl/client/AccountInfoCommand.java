/*
 * AccountInfoCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl.client;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.domain.BankAccount;
import ua.omld.ibanking.domain.CreditAccount;
import ua.omld.ibanking.domain.DepositAccount;
import ua.omld.ibanking.service.BankAccountService;
import ua.omld.ibanking.service.ServiceFactory;
import ua.omld.ibanking.service.exception.BankAccountNotFoundException;
import ua.omld.ibanking.view.ViewConstants.*;

/**
 * Returns information for given account
 */
public class AccountInfoCommand implements ActionCommand {

    private BankAccountService bankAccountService = ServiceFactory.getBankAccountService();

    @Override
    public String execute(HttpModel httpModel) {

        try {
            Long id = Long.valueOf(httpModel.getRequestParameter(Parameters.ID));
            BankAccount bankAccount = bankAccountService.getBankAccountInfoById(id);
            httpModel.setRequestAttribute(Attributes.ACCOUNT, bankAccount);
            if (bankAccount instanceof CreditAccount) {
                return JSP.Client.CREDIT_INFO;
            } else if (bankAccount instanceof DepositAccount) {
                return JSP.Client.DEPOSIT_INFO;
            }
        } catch (NumberFormatException nfEx) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
        } catch (BankAccountNotFoundException nfEx) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.ACCOUNT_NOT_FOUND);
        }

        return JSP.Client.ACCOUNTS;
    }
}
