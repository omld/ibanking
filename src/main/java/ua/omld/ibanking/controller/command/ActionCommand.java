/*
 * ActionCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command;

import ua.omld.ibanking.controller.servlet.HttpModel;

/**
 * Interface for action commands for front-end
 */
public interface ActionCommand {

    /**
     * Executes command.
     * Returns JSP-path for forwarding or URI/URL for redirect to.
     *
     * @param httpModel http model
     * @return page to show to user
     */
    String execute(HttpModel httpModel);
}
