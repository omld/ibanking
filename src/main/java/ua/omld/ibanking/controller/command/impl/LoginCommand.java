/*
 * LoginCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.controller.servlet.HttpResponseType;
import ua.omld.ibanking.domain.User;
import ua.omld.ibanking.service.ServiceFactory;
import ua.omld.ibanking.service.UserService;
import ua.omld.ibanking.service.exception.IncorrectCredentialsException;
import ua.omld.ibanking.service.exception.SystemFaultException;
import ua.omld.ibanking.utils.validators.Patterns;
import ua.omld.ibanking.utils.validators.StringValidator;
import ua.omld.ibanking.view.ViewConstants.*;

import java.util.ArrayList;
import java.util.List;

public class LoginCommand implements ActionCommand {

    private UserService userService = ServiceFactory.getUserService();

    @Override
    public String execute(HttpModel httpModel) {

        List<String> errors = new ArrayList<>();
        String email = httpModel.getRequestParameter(Parameters.EMAIL);
        String password = httpModel.getRequestParameter(Parameters.PASS);

        if ( email != null && password != null) {
            if ( !(new StringValidator(Patterns.EMAIL).validate(email)) ) {
                errors.add(Errors.BAD_EMAIL);
            } else if ( !(new StringValidator(Patterns.PASSWORD).validate(password)) ) {
                errors.add(Errors.BAD_LOGIN);
            }
            if (errors.size() == 0) {
                try {
                    User user = userService.loginUser(email, password);
                    httpModel.setSessionAttribute(Attributes.USER, user);
                    httpModel.setResponseType(HttpResponseType.REDIRECT);
                    return URLPaths.MAIN;
                } catch (IncorrectCredentialsException icEx) {
                    httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_LOGIN);
                } catch (SystemFaultException sfEx) {
                    httpModel.getHttpSession().invalidate();
                    httpModel.setResponseType(HttpResponseType.REDIRECT);
                    return URLPaths.ERROR;
                }
            } else {
                httpModel.setRequestAttribute(Attributes.ERRORS, errors);
            }
        }
        return JSP.LOGIN;
    }
}
