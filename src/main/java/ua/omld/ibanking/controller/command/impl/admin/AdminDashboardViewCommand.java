/*
 * DashboardViewCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl.admin;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.view.ViewConstants.JSP;

/**
 * Returns main page for admin
 */
public class AdminDashboardViewCommand implements ActionCommand {

    @Override
    public String execute(HttpModel httpModel) {

        return JSP.Admin.DASHBOARD;
    }
}
