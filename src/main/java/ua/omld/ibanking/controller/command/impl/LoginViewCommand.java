/*
 * LoginViewCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.view.ViewConstants.JSP;

public class LoginViewCommand implements ActionCommand {

    @Override
    public String execute(HttpModel httpModel) {

        return JSP.LOGIN;
    }
}
