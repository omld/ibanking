/*
 * ActionCommandFactory.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.controller.command.impl.*;
import ua.omld.ibanking.controller.command.impl.admin.AdminDashboardViewCommand;
import ua.omld.ibanking.controller.command.impl.admin.AdminRequestProcessCommand;
import ua.omld.ibanking.controller.command.impl.admin.AdminRequestsCommand;
import ua.omld.ibanking.controller.command.impl.admin.AdminRequestsViewCommand;
import ua.omld.ibanking.controller.command.impl.client.*;
import ua.omld.ibanking.controller.servlet.HttpMethod;
import ua.omld.ibanking.view.ViewConstants.URLPaths;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static ua.omld.ibanking.controller.servlet.HttpMethod.POST;

/**
 * Factory for creating command object according to given action string.
 */
public class ActionCommandsFactory {

    private static final Logger LOGGER = LogManager.getLogger();

    private Map<String, Map<HttpMethod, ActionCommand>> commands;

    private static class InstanceHolder {
        private static final ActionCommandsFactory INSTANCE = new ActionCommandsFactory();
    }

    public static ActionCommandsFactory getInstance (){
        return InstanceHolder.INSTANCE;
    }

    private ActionCommandsFactory() {

        commands = new HashMap<>();
        initActionCommands();
    }

    private void initActionCommands() {

        // main
        register(URLPaths.LOGIN, new LoginViewCommand());
        register(URLPaths.LOGIN, POST, new LoginCommand());
        register(URLPaths.LOGOUT, new LogoutCommand());
        // admin
        register(URLPaths.Admin.DASHBOARD, new AdminDashboardViewCommand());
        register(URLPaths.Admin.REQUESTS, new AdminRequestsCommand());
        register(URLPaths.Admin.REQUEST_PROCESS, HttpMethod.POST, new AdminRequestProcessCommand());
        //client
        register(URLPaths.Client.DASHBOARD, new ClientDashboardViewCommand());
        register(URLPaths.Client.ACCOUNTS, new AccountsCommand());
        register(URLPaths.Client.TRANSACTION, new TransactionInfoCommand());
        register(URLPaths.Client.PAYMENTS, new PaymentsFormCommand());
        register(URLPaths.Client.PAYMENTS, HttpMethod.POST, new PaymentsProcessCommand());
        register(URLPaths.Client.TRANSFERS, new TransfersFormCommand());
        register(URLPaths.Client.TRANSFERS, HttpMethod.POST, new TransfersProcessCommand());
        register(URLPaths.Client.REQUESTS, new RequestsFormCommand());
        register(URLPaths.Client.REQUESTS, HttpMethod.POST, new MakeRequestCommand());
    }

    private void register(String action, ActionCommand command){
        register(action, HttpMethod.GET, command);
    }

    private void register(String action, HttpMethod httpMethod, ActionCommand command){

        Map <HttpMethod, ActionCommand> methodMap = findOrCreateMethodMap(action);
        methodMap.putIfAbsent(httpMethod, command);
    }

    private Map<HttpMethod, ActionCommand> findOrCreateMethodMap(String action) {

        Map<HttpMethod, ActionCommand> map = commands.getOrDefault(action, new HashMap<>());
        commands.putIfAbsent(action, map);
        return map;
    }

    /**
     * Defines command for the given action string
     *
     * @param action action
     * @return command object
     */
    public ActionCommand defineCommand (String action, HttpMethod method) {

        ActionCommand command = new NotFoundCommand();
        Map <HttpMethod, ActionCommand> methodMap = commands.get(action);
        if (Objects.nonNull(methodMap)) {
            command = methodMap.getOrDefault(method, command);
        }
        return command;
    }
}
