/*
 * RequestsViewCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl.admin;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.domain.CreditAccountRequest;
import ua.omld.ibanking.service.AccountRequestService;
import ua.omld.ibanking.service.ServiceFactory;
import ua.omld.ibanking.service.exception.SystemFaultException;
import ua.omld.ibanking.view.ViewConstants.*;

import java.util.List;

/**
 * Returns page for clients requests list
 */
public class AdminRequestsViewCommand implements ActionCommand {

    private AccountRequestService accountRequestService = ServiceFactory.getAccountRequestService();

    @Override
    public String execute(HttpModel httpModel) {
        try {
            List<CreditAccountRequest> requests = accountRequestService.getNewRequests();
            httpModel.setRequestAttribute(Attributes.REQUESTS, requests);
        } catch (SystemFaultException sfEx) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
        }
        return JSP.Admin.REQUESTS;
    }
}
