/*
 * TransfersFormCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl.client;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.domain.BankAccount;
import ua.omld.ibanking.domain.Client;
import ua.omld.ibanking.domain.User;
import ua.omld.ibanking.service.BankAccountService;
import ua.omld.ibanking.service.ServiceFactory;
import ua.omld.ibanking.service.exception.BankAccountNotFoundException;
import ua.omld.ibanking.view.ViewConstants.Attributes;
import ua.omld.ibanking.view.ViewConstants.JSP;
import ua.omld.ibanking.view.ViewConstants.Messages;

import java.util.List;

/**
 * Returns transfers form
 */
public class TransfersFormCommand implements ActionCommand {

    private BankAccountService bankAccountService = ServiceFactory.getBankAccountService();

    @Override
    public String execute(HttpModel httpModel) {

        Client client = ((User) httpModel.getSessionAttribute(Attributes.USER)).getClient();
        try {
            CommandsUtils.cleanUpTransferInfo(httpModel);
            List<BankAccount> bankAccounts = bankAccountService.findAccountsByClient(client);
            if (bankAccounts.size() == 1) {
                httpModel.setRequestAttribute(Attributes.MESSAGES, Messages.TRANSFER_ONE_ACCOUNT);
            } else {
                httpModel.setSessionAttribute(Attributes.ACCOUNTS, bankAccounts);
            }
        } catch (BankAccountNotFoundException nfEx) {
            httpModel.setRequestAttribute(Attributes.MESSAGES, Messages.ACCOUNTS_ABSENT);
        }
        return JSP.Client.TRANSFERS;
    }
}
