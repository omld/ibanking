/*
 * LogoutCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.controller.servlet.HttpResponseType;
import ua.omld.ibanking.domain.User;
import ua.omld.ibanking.view.ViewConstants.Attributes;
import ua.omld.ibanking.view.ViewConstants.URLPaths;

import java.util.Objects;

/**
 * Command for user logout
 */
public class LogoutCommand implements ActionCommand {

    @Override
    public String execute(HttpModel httpModel) {

        User user = (User) httpModel.getSessionAttribute(Attributes.USER);
        if (Objects.nonNull(user)) {
            httpModel.setSessionAttribute(Attributes.USER, null);
            httpModel.getHttpSession().invalidate();
        }
        httpModel.setResponseType(HttpResponseType.REDIRECT);
        return URLPaths.MAIN;
    }
}
