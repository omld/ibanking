/*
 * TransfersProcessCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.controller.Utils;
import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.domain.BankAccount;
import ua.omld.ibanking.service.ServiceFactory;
import ua.omld.ibanking.service.TransactionsService;
import ua.omld.ibanking.service.exception.SystemFaultException;
import ua.omld.ibanking.view.ViewConstants.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

/**
 * Checks info for transfers and process them
 */
public class TransfersProcessCommand implements ActionCommand {

    private static final Logger LOGGER = LogManager.getLogger();
    private TransactionsService transactionsService = ServiceFactory.getTransactionsService();

    @Override
    public String execute(HttpModel httpModel) {

        String action = httpModel.getRequestParameter(Parameters.ACTION);
        switch (action) {
            case Fields.TRANSFER_VERIFY:
                verifyTransfer(httpModel);
                break;
            case Fields.TRANSFER_PROCESS:
                processTransfer(httpModel);
                break;
            default:
                httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
                break;
        }
        return JSP.Client.TRANSFERS;
    }

    private void verifyTransfer(HttpModel httpModel) {

        try {
            Long accountOutId = Long.valueOf(httpModel.getRequestParameter(Parameters.ACCOUNT_OUT_ID));
            Long accountInId = Long.valueOf(httpModel.getRequestParameter(Parameters.ACCOUNT_IN_ID));
            BigDecimal amount = BigDecimal.valueOf(
                    Double.parseDouble(httpModel.getRequestParameter(Parameters.AMOUNT)));
            List<BankAccount> accounts = (List<BankAccount>) httpModel.getSessionAttribute(Attributes.ACCOUNTS);
            BankAccount outcomeAccount = accounts.stream().filter(x -> x.getId().equals(accountOutId)).findFirst().get();
            BankAccount incomeAccount = accounts.stream().filter(x -> x.getId().equals(accountInId)).findFirst().get();
            if (checkOneAccount(outcomeAccount, incomeAccount, httpModel)) {
                return;
            }
            BigDecimal fee = transactionsService.checkTransferAndGetFee(outcomeAccount, incomeAccount, amount);
            if (fee != null) {
                httpModel.setSessionAttribute(Attributes.ACCOUNT_OUT, outcomeAccount);
                httpModel.setSessionAttribute(Attributes.ACCOUNT_IN, incomeAccount);
                httpModel.setSessionAttribute(Attributes.AMOUNT, amount);
                httpModel.setSessionAttribute(Attributes.FEE, fee);
            } else {
                httpModel.setRequestAttribute(Attributes.MESSAGES, Messages.TRANSFER_PROHIBITED);
            }
        } catch (NumberFormatException nfEx) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
            LOGGER.warn("Can`t parse digital data.");
        } catch (RuntimeException rtEx) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.TRANSFER_ERROR);
        }
    }

    private boolean checkOneAccount(BankAccount outcomeAccount, BankAccount incomeAccount, HttpModel httpModel) {
        if (outcomeAccount.getId().equals(incomeAccount.getId())) {
            httpModel.setRequestAttribute(Attributes.MESSAGES, Messages.TRANSFER_PROHIBITED_ONE_ACCOUNT);
            return true;
        }
        return false;
    }

    private void processTransfer(HttpModel httpModel) {

        try {
            Locale clientLocale = Utils.getLocale((String) httpModel.getSessionAttribute(Attributes.LOCALE));
            BigDecimal amount = (BigDecimal) httpModel.getSessionAttribute(Attributes.AMOUNT);
            BankAccount outcomeAccount = (BankAccount) httpModel.getSessionAttribute(Attributes.ACCOUNT_OUT);
            BankAccount incomeAccount = (BankAccount) httpModel.getSessionAttribute(Attributes.ACCOUNT_IN);
            if (checkOneAccount(outcomeAccount, incomeAccount, httpModel)) {
                return;
            }
            if (transactionsService.transferMoney(outcomeAccount, incomeAccount, amount, clientLocale)) {
                httpModel.setRequestAttribute(Attributes.TRANSFER_COMPLETE, true);
                httpModel.setRequestAttribute(Attributes.MESSAGES, Messages.TRANSFER_COMPLETE);
            } else {
                httpModel.setRequestAttribute(Attributes.ERRORS, Errors.TRANSFER_ERROR);
            }
        } catch (NumberFormatException nfEx) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
            LOGGER.warn("Can`t parse transfer amount.");
        } catch (SystemFaultException sfEx) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.TRANSFER_ERROR);
        } finally {
            CommandsUtils.cleanUpTransferInfo(httpModel);
        }
    }
}
