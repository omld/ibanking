/*
 * Utils.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl.client;

import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.view.ViewConstants.Attributes;

class CommandsUtils {

    private CommandsUtils() {
        throw new AssertionError();
    }

    static void cleanUpPaymentInfo(HttpModel httpModel) {

        httpModel.removeSessionAttribute(Attributes.INVOICE);
        httpModel.removeSessionAttribute(Attributes.FEE);
        httpModel.removeSessionAttribute(Attributes.ACCOUNT);
    }

    static void cleanUpTransferInfo(HttpModel httpModel) {

        httpModel.removeSessionAttribute(Attributes.AMOUNT);
        httpModel.removeSessionAttribute(Attributes.FEE);
        httpModel.removeSessionAttribute(Attributes.ACCOUNT_OUT);
        httpModel.removeSessionAttribute(Attributes.ACCOUNT_IN);
    }
}
