/*
 * AccountsCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl.client;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.view.ViewConstants.Parameters;

import java.util.Objects;

/**
 * Redirects call to appropriate command
 */
public class AccountsCommand implements ActionCommand {

    @Override
    public String execute(HttpModel httpModel) {

        ActionCommand command;
        if (Objects.nonNull(httpModel.getRequestParameter(Parameters.ID))) {
            command = new AccountInfoCommand();
        } else {
            command = new AccountsViewCommand();
        }
        return command.execute(httpModel);
    }
}
