/*
 * VoidCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.controller.servlet.HttpResponseType;

import static ua.omld.ibanking.view.ViewConstants.URLPaths.ERR404;

/**
 * Void command for errors.
 */
public class NotFoundCommand implements ActionCommand {

    @Override
    public String execute(HttpModel httpModel) {

        httpModel.setResponseType(HttpResponseType.ERR_404);
        return ERR404;
    }
}
