/*
 * AdminRequestProcessCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl.admin;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.domain.CreditAccountRequest;
import ua.omld.ibanking.service.AccountRequestService;
import ua.omld.ibanking.service.ServiceFactory;
import ua.omld.ibanking.service.exception.CreditAccountExistException;
import ua.omld.ibanking.service.exception.CreditRequestIllegalException;
import ua.omld.ibanking.service.exception.SystemFaultException;
import ua.omld.ibanking.view.ViewConstants.*;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Returns page for clients requests list
 */
public class AdminRequestProcessCommand implements ActionCommand {

    private AccountRequestService accountRequestService = ServiceFactory.getAccountRequestService();

    @Override
    public String execute(HttpModel httpModel) {

        try {
            Long id = Long.valueOf(httpModel.getRequestParameter(Parameters.ID));
            CreditAccountRequest creditAccountRequest = accountRequestService.getRequestInfo(id);
            String action = httpModel.getRequestParameter(Parameters.ACTION);
            switch (action) {
                case Fields.REQUEST_APPROVE:
                    approveRequest(creditAccountRequest, httpModel);
                    break;
                case Fields.REQUEST_REJECT:
                    rejectRequest(creditAccountRequest, httpModel);
                    break;
                default:
                    httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
                    break;
            }

            CreditAccountRequest requestInfo = accountRequestService.getRequestInfo(id);
            httpModel.setRequestAttribute(Attributes.REQUEST_INFO, requestInfo);
        } catch (NumberFormatException | SystemFaultException ex) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
        }
        return JSP.Admin.REQUEST_INFO;
    }

    private void approveRequest(CreditAccountRequest request, HttpModel httpModel) {

        try {
            BigDecimal approvedAmount = BigDecimal.valueOf(
                    Double.parseDouble(httpModel.getRequestParameter(Parameters.APPROVED_AMOUNT)));
            LocalDate approvedValidity = LocalDate.parse(httpModel.getRequestParameter(Parameters.APPROVED_VALIDITY));
            request.setApprovedAmount(approvedAmount);
            request.setApprovedValidity(approvedValidity);
            accountRequestService.approveRequestAndCreateCreditAccount(request);
            httpModel.setRequestAttribute(Attributes.MESSAGES, Messages.REQUEST_APPROVED);
        } catch (CreditAccountExistException caeEx) {
            httpModel.setRequestAttribute(Attributes.MESSAGES, Messages.CREDIT_EXIST);
        } catch (CreditRequestIllegalException criEx) {
            httpModel.setRequestAttribute(Attributes.MESSAGES, Messages.REQUEST_PROHIBITED);
        } catch (SystemFaultException sfEx) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
        }
    }

    private void rejectRequest(CreditAccountRequest request, HttpModel httpModel) {

        accountRequestService.rejectRequestForCreditAccount(request);
        httpModel.setRequestAttribute(Attributes.MESSAGES, Messages.REQUEST_REJECTED);
    }
}
