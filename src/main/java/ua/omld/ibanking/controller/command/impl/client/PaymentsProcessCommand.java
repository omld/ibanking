/*
 * PaymentsCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.domain.BankAccount;
import ua.omld.ibanking.domain.Invoice;
import ua.omld.ibanking.service.ServiceFactory;
import ua.omld.ibanking.service.TransactionsService;
import ua.omld.ibanking.service.exception.SystemFaultException;
import ua.omld.ibanking.utils.validators.Patterns;
import ua.omld.ibanking.utils.validators.StringValidator;
import ua.omld.ibanking.view.ViewConstants.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Checks info for payments and process them
 */
public class PaymentsProcessCommand implements ActionCommand {

    private static final Logger LOGGER = LogManager.getLogger();
    private TransactionsService transactionsService = ServiceFactory.getTransactionsService();

    @Override
    public String execute(HttpModel httpModel) {

        String page = JSP.Client.PAYMENTS;
        String action = httpModel.getRequestParameter(Parameters.ACTION);
        switch (action) {
            case Fields.PAYMENT_VERIFY:
                verifyPayment(httpModel);
                break;
            case Fields.PAYMENT_PAY:
                payInvoice(httpModel);
                break;
            default:
                httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
                break;
        }
        return page;
    }

    private void verifyPayment(HttpModel httpModel) {

        Invoice invoice = buildInvoice(httpModel);
        httpModel.setSessionAttribute(Attributes.INVOICE, invoice);
        if ( Objects.isNull(httpModel.getRequestAttribute(Attributes.ERRORS)) ) {
            try {
                Long accountId = Long.valueOf(httpModel.getRequestParameter(Parameters.ACCOUNT_ID));
                List<BankAccount> accounts = (List<BankAccount>) httpModel.getSessionAttribute(Attributes.ACCOUNTS);
                BankAccount bankAccount = accounts.stream().filter(x -> x.getId().equals(accountId)).findFirst().get();
                BigDecimal fee = transactionsService.checkInvoiceAndGetFee(invoice, bankAccount);
                if (fee != null) {
                    httpModel.setSessionAttribute(Attributes.ACCOUNT, bankAccount);
                    httpModel.setSessionAttribute(Attributes.FEE, fee);
                } else {
                    httpModel.setRequestAttribute(Attributes.MESSAGES, Messages.PAYMENT_PROHIBITED);
                }
            } catch (NumberFormatException nfEx) {
                httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
            } catch (RuntimeException rtEx) {
                httpModel.setRequestAttribute(Attributes.ERRORS, Errors.PAYMENT_ERROR);
            }
        }
    }

    private Invoice buildInvoice(HttpModel httpModel) {

        Invoice invoice = new Invoice();
        try {
            BigDecimal amount = BigDecimal.valueOf(
                Double.parseDouble(httpModel.getRequestParameter(Parameters.AMOUNT)));
            invoice.setBeneficiaryName(httpModel.getRequestParameter(Parameters.BENEFICIARY_NAME));
            invoice.setBeneficiaryCode(httpModel.getRequestParameter(Parameters.BENEFICIARY_CODE));
            invoice.setBankCode(httpModel.getRequestParameter(Parameters.BENEFICIARY_BANK));
            invoice.setBankAccount(httpModel.getRequestParameter(Parameters.BENEFICIARY_ACCOUNT));
            invoice.setDescription(httpModel.getRequestParameter(Parameters.DESCRIPTION));
            invoice.setAmount(amount);
            checkInvoiceData(invoice, httpModel);
        } catch (NumberFormatException ex) {
            LOGGER.error("Can`t parse invoice amount.");
            invoice = null;
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_AMOUNT);
        }
        return invoice;
    }

    private void checkInvoiceData(Invoice invoice, HttpModel httpModel) {

        List<String> errors = new ArrayList<>();
        if ( !(new StringValidator(Patterns.PERSON_NAME).validate(invoice.getBeneficiaryName())) ) {
            errors.add(Errors.BAD_BENEFICIARY_NAME);
        }
        if ( !(new StringValidator(Patterns.PERSON_CODE).validate(invoice.getBeneficiaryCode())) ) {
            errors.add(Errors.BAD_BENEFICIARY_CODE);
        }
        if ( !(new StringValidator(Patterns.BANK_CODE).validate(invoice.getBankCode())) ) {
            errors.add(Errors.BAD_BANK_CODE);
        }
        if ( !(new StringValidator(Patterns.BANK_ACCOUNT_NUMBER).validate(invoice.getBankAccount())) ) {
            errors.add(Errors.BAD_ACCOUNT_NUMBER);
        }
        if ( !(new StringValidator(Patterns.INVOICE_DESCRIPTION).validate(invoice.getDescription())) ) {
            errors.add(Errors.BAD_INVOICE_DESCRIPTION);
        }
        if ( invoice.getAmount().compareTo(BigDecimal.ZERO) <= 0 ) {
            errors.add(Errors.BAD_AMOUNT);
        }
        if (errors.size() > 0) {
            httpModel.setRequestAttribute(Attributes.ERRORS, errors);
        }
    }

    private void payInvoice(HttpModel httpModel) {

        try {
            Invoice invoice = (Invoice) httpModel.getSessionAttribute(Attributes.INVOICE);
            checkInvoiceData(invoice, httpModel);
            if ( Objects.isNull(httpModel.getRequestAttribute(Attributes.ERRORS)) ) {
                BankAccount bankAccount = (BankAccount) httpModel.getSessionAttribute(Attributes.ACCOUNT);
                if (transactionsService.payInvoice(invoice, bankAccount)) {
                    httpModel.setRequestAttribute(Attributes.PAYMENT_COMPLETE, true);
                    httpModel.setRequestAttribute(Attributes.MESSAGES, Messages.PAYMENT_COMPLETE);
                } else {
                    httpModel.setRequestAttribute(Attributes.ERRORS, Errors.PAYMENT_ERROR);
                }
            }
        } catch (SystemFaultException sfEx) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.PAYMENT_ERROR);
        } finally {
            CommandsUtils.cleanUpPaymentInfo(httpModel);
        }
    }
}
