/*
 * MakeRequestCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl.client;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.domain.*;
import ua.omld.ibanking.service.AccountRequestService;
import ua.omld.ibanking.service.ServiceFactory;
import ua.omld.ibanking.service.exception.SystemFaultException;
import ua.omld.ibanking.view.ViewConstants.*;

import java.math.BigDecimal;
import java.time.Period;

/**
 * Returns payments form
 */
public class MakeRequestCommand implements ActionCommand {

    private AccountRequestService accountRequestService = ServiceFactory.getAccountRequestService();

    @Override
    public String execute(HttpModel httpModel) {

        try {
            Client client = ((User) httpModel.getSessionAttribute(Attributes.USER)).getClient();
            BigDecimal amount = BigDecimal.valueOf(
                    Double.parseDouble(httpModel.getRequestParameter(Parameters.AMOUNT)));
            Period term = Period.ofMonths(Integer.parseInt(httpModel.getRequestParameter(Parameters.TERM)));

            CreditAccountRequest request = accountRequestService.makeRequestForCreditAccount(client, amount, term);
            if (request != null) {
                httpModel.setRequestAttribute(Attributes.REQUEST_NUMBER, request.getId());
            }

        } catch (SystemFaultException sfEx) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
        }
        return JSP.Client.REQUEST_SENT;
    }

}
