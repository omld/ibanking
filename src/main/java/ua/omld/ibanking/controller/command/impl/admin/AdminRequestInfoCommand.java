/*
 * AdminRequestsInfoCommand.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.controller.command.impl.admin;

import ua.omld.ibanking.controller.command.ActionCommand;
import ua.omld.ibanking.controller.servlet.HttpModel;
import ua.omld.ibanking.domain.CreditAccountRequest;
import ua.omld.ibanking.service.AccountRequestService;
import ua.omld.ibanking.service.ServiceFactory;
import ua.omld.ibanking.service.exception.SystemFaultException;
import ua.omld.ibanking.view.ViewConstants;
import ua.omld.ibanking.view.ViewConstants.Attributes;
import ua.omld.ibanking.view.ViewConstants.Errors;
import ua.omld.ibanking.view.ViewConstants.JSP;

import java.util.List;

/**
 * Returns page for clients requests list
 */
public class AdminRequestInfoCommand implements ActionCommand {

    private AccountRequestService accountRequestService = ServiceFactory.getAccountRequestService();

    @Override
    public String execute(HttpModel httpModel) {

        try {
            Long id = Long.valueOf(httpModel.getRequestParameter(ViewConstants.Parameters.ID));
            CreditAccountRequest requestInfo = accountRequestService.getRequestInfo(id);
            httpModel.setRequestAttribute(Attributes.REQUEST_INFO, requestInfo);
        } catch (NumberFormatException | SystemFaultException ex) {
            httpModel.setRequestAttribute(Attributes.ERRORS, Errors.BAD_REQUEST);
        }
        return JSP.Admin.REQUEST_INFO;
    }
}
