/*
 * JDBCConnectorTest.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.connection.jdbc;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.omld.ibanking.storage.connection.StorageConnector;

import java.sql.Connection;

import static org.junit.Assert.*;

public class JDBCConnectorTest {

    private static StorageConnector connector;
    private Connection connection;

    @BeforeClass
    public static void setUpClass() throws Exception {
        connector = JDBCConnector.getInstance();
    }

    @Before
    public void setUp() throws Exception {
        connection = (Connection) connector.getConnection();
    }

    @Test
    public void getConnection() throws Exception {
        assertEquals(false, connection.isClosed());
        assertEquals(true, connection.getAutoCommit());
        assertEquals(true, connection.isValid(100));
    }

    @Test
    public void closeConnection() throws Exception {
        connection.close();
        assertEquals(true, connection.isClosed());
    }

}