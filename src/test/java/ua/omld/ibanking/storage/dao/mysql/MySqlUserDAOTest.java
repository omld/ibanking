/*
 * MySqlUserDAOTest.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao.mysql;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.omld.ibanking.domain.User;
import ua.omld.ibanking.domain.testdata.*;
import ua.omld.ibanking.storage.dao.UserDAO;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class MySqlUserDAOTest {

    private MySqlDAOFactory mySqlDAOFactory;
    private UserDAO userDAO;
    private User newUser;
    private User storedUser;
    private User storedAdmin;

    @BeforeClass
    public static void init() {
        MySqlCommon.initDB();
    }

    @Before
    public void setUp() throws Exception {
        mySqlDAOFactory = MySqlDAOFactory.getInstance();
        userDAO = mySqlDAOFactory.createUserDAO();
        newUser = TestdataUser.getNewUser();
        storedUser = TestdataUser.getStoredUser();
        storedAdmin = TestdataUser.getStoredAdmin();
    }

    @Test
    public void findByEmailAdmin() throws Exception {
        User foundUser = userDAO.findByEmail(storedAdmin.getEmail());
        assertEquals(storedAdmin, foundUser);
    }

    @Test
    public void getAll() throws Exception {
        List<User> foundUsers = userDAO.getAll();
        assertEquals(1, foundUsers.size() );
        assertEquals(storedAdmin, foundUsers.get(0) );
    }
}