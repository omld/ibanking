/*
 * MySqlAbstractDAOTest.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao.mysql;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import ua.omld.ibanking.domain.Entity;
import ua.omld.ibanking.storage.connection.jdbc.JDBCConnector;
import ua.omld.ibanking.storage.exception.DAOException;

import java.sql.ResultSet;
import java.util.List;

import static org.junit.Assert.assertEquals;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MySqlAbstractDAOTest {

    private static MySqlAbstractDAO testDao;

    @BeforeClass
    public static void setUp() throws Exception {

        testDao = new MySqlAbstractDAO(JDBCConnector.getInstance().getConnection(), true) {

            @Override
            protected List parseResultSet(ResultSet rs) throws DAOException {
                return null;
            }

            @Override
            protected Object[] getValuesArray(Entity entity) {
                return new Object[0];
            }
        };
        testDao.TABLE = "Table1";
    }

    @Test
    public void selectAllQueryTwoColumns() throws Exception {

        String [] arr = {"Column1", "Column2"};
        testDao.SQL_COLUMNS = arr;
        String queryFormat = "SELECT ID, %s, %s FROM %s";
        String query = String.format(queryFormat, arr[0], arr[1], testDao.TABLE);
        assertEquals(query, testDao.selectAllQuery());
    }

    @Test
    public void selectAllQueryOneColumn() throws Exception {

        String [] arr = {"Column1"};
        testDao.SQL_COLUMNS = arr;
        String queryFormat = "SELECT ID, %s FROM %s";
        String query = String.format(queryFormat, arr[0], testDao.TABLE);
        assertEquals(query, testDao.selectAllQuery());
    }

    @Test
    public void insertQueryTwoColumns() {

        String [] arr = {"Column1", "Column2"};
        testDao.SQL_COLUMNS = arr;
        String queryFormat = "INSERT INTO %s (%s, %s) VALUES (?, ?)";
        String query = String.format(queryFormat, testDao.TABLE, arr[0], arr[1]);
        assertEquals(query, testDao.insertQuery());
    }

    @Test
    public void insertQueryOneColumn() {

        String [] arr = {"Column1"};
        testDao.SQL_COLUMNS = arr;
        String queryFormat = "INSERT INTO %s (%s) VALUES (?)";
        String query = String.format(queryFormat, testDao.TABLE, arr[0]);
        assertEquals(query, testDao.insertQuery());
    }

    @Test
    public void updateQueryTwoColumns() {

        String [] arr = {"Column1", "Column2"};
        testDao.SQL_COLUMNS = arr;
        String queryFormat = "UPDATE %s SET %s = ?, %s = ? WHERE ID = ?";
        String query = String.format(queryFormat, testDao.TABLE, arr[0], arr[1]);
        assertEquals(query, testDao.updateQuery());
    }

    @Test
    public void updateQueryOneColumn() {

        String [] arr = {"Column1"};
        testDao.SQL_COLUMNS = arr;
        String queryFormat = "UPDATE %s SET %s = ? WHERE ID = ?";
        String query = String.format(queryFormat, testDao.TABLE, arr[0]);
        assertEquals(query, testDao.updateQuery());
    }

    @Test
    public void deleteQuery() {

        String queryFormat = "DELETE %s WHERE ID = ?";
        String query = String.format(queryFormat, testDao.TABLE);
        assertEquals(query, testDao.deleteQuery());
    }


}