/*
 * MySqlCommon.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao.mysql;

import ua.omld.ibanking.storage.connection.jdbc.JDBCConnector;

import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;

public class MySqlCommon {

    private static final String dbStructureFile = "src/test/sql/db_structure.sql";
    private static final String initAdminFile = "src/test/sql/init_admin.sql";

    public static void initDB() {

        try {
            Connection connection = JDBCConnector.getInstance().getConnection();
            ScriptRunner scriptRunner = new ScriptRunner(connection);
            scriptRunner.setLogWriter(null);
            Reader reader;
            // fill DB structure
            reader = new BufferedReader(new FileReader(dbStructureFile));
            scriptRunner.runScript(reader);
            // init admin user
            reader = new BufferedReader(new FileReader(initAdminFile));
            scriptRunner.runScript(reader);
        } catch (Exception e) {
            System.err.println("Failed to Execute '" + dbStructureFile
                    + "' The error is " + e.getMessage());
        }
    }
}
