/*
 * MySqlBankAccountDAOTest.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.storage.dao.mysql;

import org.junit.BeforeClass;
import org.junit.Test;
import ua.omld.ibanking.domain.CreditAccount;
import ua.omld.ibanking.domain.DepositAccount;
import ua.omld.ibanking.domain.testdata.CreditAccountsData;
import ua.omld.ibanking.domain.testdata.DepositAccountsData;
import ua.omld.ibanking.storage.dao.BankAccountDAO;

import static org.junit.Assert.assertEquals;

public class MySqlBankAccountDAOTest {

    private static MySqlDAOFactory mySqlDAOFactory;
    private static BankAccountDAO bankAccountDAO;
    private static DepositAccount depositAccount;
    private static CreditAccount creditAccount;

    @BeforeClass
    public static void init() throws Exception {
        MySqlCommon.initDB();
        mySqlDAOFactory = MySqlDAOFactory.getInstance();
        bankAccountDAO = mySqlDAOFactory.createBankAccountDAO();
        depositAccount = DepositAccountsData.ONE_NEW.getAccount();
        creditAccount = CreditAccountsData.ONE_NEW.getAccount();
    }

    @Test
    public void create() throws Exception {
        long depositId = bankAccountDAO.create(depositAccount);
        long creditId = bankAccountDAO.create(creditAccount);
        assertEquals(1L, depositId);
        assertEquals(2L, creditId);
    }

}