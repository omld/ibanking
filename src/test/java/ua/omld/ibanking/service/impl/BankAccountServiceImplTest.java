/*
 * BankAccountServiceImplTest.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service.impl;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.omld.ibanking.domain.BankAccount;
import ua.omld.ibanking.domain.Client;
import ua.omld.ibanking.domain.Transaction;
import ua.omld.ibanking.domain.testdata.CreditAccountsData;
import ua.omld.ibanking.domain.testdata.DepositAccountsData;
import ua.omld.ibanking.domain.testdata.TestdataClient;
import ua.omld.ibanking.service.exception.BankAccountNotFoundException;
import ua.omld.ibanking.storage.connection.ConnectionWrapper;
import ua.omld.ibanking.storage.dao.BankAccountDAO;
import ua.omld.ibanking.storage.dao.ClientDAO;
import ua.omld.ibanking.storage.dao.DAOFactory;
import ua.omld.ibanking.storage.dao.TransactionDAO;
import ua.omld.ibanking.utils.ConfigurationManager;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BankAccountServiceImplTest {

    private static DAOFactory mockDaoFactory;
    private static ConnectionWrapper mockConnectionWrapper;
    private static BankAccountServiceImpl bankAccountService;
    private static BankAccountDAO mockBankAccountDAO;
    private static TransactionDAO mockTransactionDAO;
    private static ClientDAO mockClientDAO;

    private BankAccount depositAccount;
    private BankAccount creditAccount;
    private List<BankAccount> bankAccountsEmpty;
    private List<BankAccount> bankAccountsOne;
    private List<BankAccount> bankAccountsTwo;
    private List<Transaction> transactionsEmpty;
    private Client testClient;

    @BeforeClass
    public static void setUpClass() throws Exception {

        setUpMocks();
        trainMockitoForClass();
    }

    private static void setUpMocks() {

        mockDaoFactory = mock(DAOFactory.class);
        mockConnectionWrapper = mock(ConnectionWrapper.class);
        bankAccountService = new BankAccountServiceImpl(mockDaoFactory);
        mockBankAccountDAO = mock(BankAccountDAO.class);
        mockTransactionDAO = mock(TransactionDAO.class);
        mockClientDAO = mock(ClientDAO.class);
    }

    private static void trainMockitoForClass() throws Exception {
        when(mockDaoFactory.getConnection()).thenReturn(mockConnectionWrapper);
        when(mockDaoFactory.createBankAccountDAO()).thenReturn(mockBankAccountDAO);
        when(mockDaoFactory.createBankAccountDAO(mockConnectionWrapper)).thenReturn(mockBankAccountDAO);
        when(mockDaoFactory.createTransactionDAO(mockConnectionWrapper)).thenReturn(mockTransactionDAO);
        when(mockDaoFactory.createClientDAO(mockConnectionWrapper)).thenReturn(mockClientDAO);
    }

    @Before
    public void setUpAndTrainMockito() throws Exception {

        setUpTestData();
        trainMockito();
    }

    private void setUpTestData() throws Exception {

        depositAccount = DepositAccountsData.ONE_STORED.getAccount();
        creditAccount = CreditAccountsData.ONE_STORED.getAccount();
        bankAccountsEmpty = new ArrayList<>();
        bankAccountsOne = new ArrayList<>();
        bankAccountsOne.add(depositAccount);
        bankAccountsTwo = new ArrayList<>();
        bankAccountsTwo.add(depositAccount);
        bankAccountsTwo.add(creditAccount);
        transactionsEmpty = new ArrayList<>();
        testClient = TestdataClient.getSavedClient();
    }

    private void trainMockito() throws Exception {

        when(mockBankAccountDAO.findById(depositAccount.getId())).thenReturn(depositAccount);
        when(mockBankAccountDAO.findById(creditAccount.getId())).thenReturn(creditAccount);
        when(mockTransactionDAO.getLastTransactionsByAccountId(depositAccount.getId(),
                ConfigurationManager.getInstance().getLastTransactionsCount())).thenReturn(transactionsEmpty);
        when(mockClientDAO.findByBankAccountId(depositAccount.getId())).thenReturn(testClient);
        when(mockClientDAO.findByBankAccountId(creditAccount.getId())).thenReturn(testClient);
    }

    @Test
    public void findOneAccountByClient() throws Exception {

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.LONG)).thenReturn(bankAccountsOne);

        List<BankAccount> checkBankAccounts = bankAccountService.findAccountsByClient(testClient);

        assertEquals(bankAccountsOne, checkBankAccounts);
    }

    @Test
    public void findTwoAccountsByClient() throws Exception {

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.LONG)).thenReturn(bankAccountsTwo);

        List<BankAccount> checkBankAccounts = bankAccountService.findAccountsByClient(testClient);

        assertEquals(bankAccountsTwo, checkBankAccounts);
    }

    @Test (expected = BankAccountNotFoundException.class)
    public void canNotFindAccountsByClient() throws Exception {

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.LONG)).thenReturn(bankAccountsEmpty);

        bankAccountService.findAccountsByClient(testClient);
    }

    @Test
    public void getBankAccountInfoById() {

        BankAccount checkAccount1 = bankAccountService.getBankAccountInfoById(depositAccount.getId());
        BankAccount checkAccount2 = bankAccountService.getBankAccountInfoById(creditAccount.getId());

        assertEquals(depositAccount, checkAccount1);
        assertEquals(creditAccount, checkAccount2);
    }

}