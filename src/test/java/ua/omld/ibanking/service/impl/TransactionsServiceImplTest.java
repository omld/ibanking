/*
 * TransactionsServiceImplTest.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service.impl;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import ua.omld.ibanking.domain.BankAccount;
import ua.omld.ibanking.domain.Client;
import ua.omld.ibanking.domain.Transaction;
import ua.omld.ibanking.domain.testdata.*;
import ua.omld.ibanking.service.exception.IllegalPeriodException;
import ua.omld.ibanking.storage.connection.ConnectionWrapper;
import ua.omld.ibanking.storage.dao.BankAccountDAO;
import ua.omld.ibanking.storage.dao.ClientDAO;
import ua.omld.ibanking.storage.dao.DAOFactory;
import ua.omld.ibanking.storage.dao.TransactionDAO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TransactionsServiceImplTest {

    private static DAOFactory mockDaoFactory;
    private static ConnectionWrapper mockConnectionWrapper;
    private static TransactionsServiceImpl mockTransactionsService;
    private static TransactionDAO mockTransactionDAO;
    private static BankAccountDAO mockBankAccountDAO;
    private static ClientDAO mockClientDAO;

    private BankAccount depositTwoAccount;
    private BankAccount creditAccount;
    private Client client;
    private List<Transaction> depositTwoTransactions;
    private List<Transaction> creditTransactions;


    @BeforeClass
    public static void setUpClass() throws Exception {

        setUpMocks();
        trainMockitoForClass();
    }

    private static void setUpMocks() {

        mockDaoFactory = mock(DAOFactory.class);
        mockConnectionWrapper = mock(ConnectionWrapper.class);
        mockTransactionsService = new TransactionsServiceImpl(mockDaoFactory);
        mockTransactionDAO = mock(TransactionDAO.class);
        mockBankAccountDAO = mock(BankAccountDAO.class);
        mockClientDAO = mock(ClientDAO.class);
    }

    private static void trainMockitoForClass() throws Exception {

        when(mockDaoFactory.getConnection()).thenReturn(mockConnectionWrapper);
        when(mockDaoFactory.createTransactionDAO()).thenReturn(mockTransactionDAO);
        when(mockDaoFactory.createTransactionDAO(mockConnectionWrapper)).thenReturn(mockTransactionDAO);
        when(mockDaoFactory.createBankAccountDAO(mockConnectionWrapper)).thenReturn(mockBankAccountDAO);
        when(mockDaoFactory.createClientDAO(mockConnectionWrapper)).thenReturn(mockClientDAO);
    }

    @Before
    public void setUpAndTrainMockito() throws Exception {

        setUpTestData();
        trainMockito();
    }

    private void setUpTestData() throws Exception {

        depositTwoAccount = DepositAccountsData.TWO_STORED.getAccount();
        creditAccount = CreditAccountsData.ONE_STORED.getAccount();
        client = TestdataClient.getSavedClient();
        depositTwoTransactions = TestdataTransactions.getTransactionsForDepositTwo();
        creditTransactions = TestdataTransactions.getTransactionsForCredit();
    }

    private void trainMockito() throws Exception {

        when(mockBankAccountDAO.findById(depositTwoAccount.getId())).thenReturn(depositTwoAccount);
        when(mockBankAccountDAO.findById(creditAccount.getId())).thenReturn(creditAccount);
        when(mockClientDAO.findByBankAccountId(depositTwoAccount.getId())).thenReturn(client);
        when(mockClientDAO.findByBankAccountId(depositTwoAccount.getId())).thenReturn(client);
        when(mockClientDAO.findByBankAccountId(creditAccount.getId())).thenReturn(client);
    }



    @Test
    public void getTransactionsForDepositOneByPeriodEmpty() throws Exception {

        List<Transaction> emptyTransactions = new ArrayList<>();
        LocalDate from = depositTwoTransactions.get(0).getCreationDateTime().toLocalDate().minusDays(3);
        LocalDate to = depositTwoTransactions.get(0).getCreationDateTime().toLocalDate().minusDays(2);
        when( mockTransactionDAO.getTransactionsByAccountIdForPeriod(depositTwoAccount.getId(), from, to) )
                .thenReturn(emptyTransactions);

        List<Transaction> checkTransactions =
                mockTransactionsService.getTransactionsByAccountForPeriod(depositTwoAccount, from, to);

        assertEquals(emptyTransactions, checkTransactions);
    }

    @Test (expected = IllegalPeriodException.class)
    public void getTransactionsForDepositOneIllegalPeriod() throws Exception {

        LocalDate from = depositTwoTransactions.get(0).getCreationDateTime().toLocalDate().minusMonths(2);
        LocalDate to = depositTwoTransactions.get(0).getCreationDateTime().toLocalDate().plusMonths(2);
        when( mockTransactionDAO.getTransactionsByAccountIdForPeriod(depositTwoAccount.getId(), from, to) )
                .thenReturn(depositTwoTransactions);

        mockTransactionsService.getTransactionsByAccountForPeriod(depositTwoAccount, from, to);
    }

    @Test
    public void getTransactionsForDepositOneByPeriod() throws Exception {

        LocalDate from = depositTwoTransactions.get(0).getCreationDateTime().toLocalDate();
        LocalDate to = depositTwoTransactions.get(0).getCreationDateTime().toLocalDate().plusDays(20);
        when( mockTransactionDAO.getTransactionsByAccountIdForPeriod(depositTwoAccount.getId(), from, to) )
                .thenReturn(depositTwoTransactions);

        List<Transaction> checkTransactions =
                mockTransactionsService.getTransactionsByAccountForPeriod(depositTwoAccount, from, to);

        assertEquals(depositTwoTransactions, checkTransactions);
    }

    @Test
    public void payBigInvoice() throws Exception {

        boolean result = mockTransactionsService.payInvoice(TestdataTransactions.getBigInvoice(), depositTwoAccount);

        assertFalse(result);
    }
}