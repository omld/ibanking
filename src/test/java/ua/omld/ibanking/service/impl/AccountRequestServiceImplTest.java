/*
 * AccountRequestServiceImplTest.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service.impl;

import org.junit.*;
import org.junit.runners.MethodSorters;
import ua.omld.ibanking.domain.BankAccount;
import ua.omld.ibanking.domain.CreditAccountRequest;
import ua.omld.ibanking.service.exception.*;
import ua.omld.ibanking.storage.connection.ConnectionWrapper;
import ua.omld.ibanking.storage.dao.*;
import ua.omld.ibanking.domain.testdata.*;
import ua.omld.ibanking.storage.exception.DAOException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AccountRequestServiceImplTest {

    private static DAOFactory mockDaoFactory;
    private static ConnectionWrapper mockConnectionWrapper;
    private static AccountRequestServiceImpl spyAccountRequestService;
    private static CreditAccountRequestDAO mockCreditAccountRequestDAO;
    private static BankAccountDAO mockBankAccountDAO;

    private CreditAccountRequest newRequest;
    private CreditAccountRequest savedRequest;
    private CreditAccountRequest fullRequest;
    private List<CreditAccountRequest> requests;
    private List<CreditAccountRequest> requestsEmpty;
    private BankAccount depositOneAccount;
    private BankAccount creditAccount;
    private List<BankAccount> bankAccountsEmpty;
    private List<BankAccount> bankAccountsDeposit;
    private List<BankAccount> bankAccountsDepositBig;
    private List<BankAccount> bankAccountsDepositCredit;

    @BeforeClass
    public static void setUpClass() throws Exception {

        setUpMocks();
        trainMockitoForClass();
    }

    private static void setUpMocks() {

        mockDaoFactory = mock(DAOFactory.class);
        mockConnectionWrapper = mock(ConnectionWrapper.class);
        spyAccountRequestService = spy(new AccountRequestServiceImpl(mockDaoFactory));
        mockCreditAccountRequestDAO = mock(CreditAccountRequestDAO.class);
        mockBankAccountDAO = mock(BankAccountDAO.class);
//        mockClientDAO = mock(ClientDAO.class);
    }

    private static void trainMockitoForClass() throws Exception {
        when(mockDaoFactory.getConnection()).thenReturn(mockConnectionWrapper);
        when(mockDaoFactory.createCreditAccountRequestDAO()).thenReturn(mockCreditAccountRequestDAO);
        when(mockDaoFactory.createCreditAccountRequestDAO(mockConnectionWrapper)).thenReturn(mockCreditAccountRequestDAO);
        when(mockDaoFactory.createBankAccountDAO()).thenReturn(mockBankAccountDAO);
        when(mockDaoFactory.createBankAccountDAO(mockConnectionWrapper)).thenReturn(mockBankAccountDAO);
//        when(mockDaoFactory.createClientDAO(mockConnectionWrapper)).thenReturn(mockClientDAO);
    }


    @Before
    public void setUpAndTrainMockito() throws Exception {

        setUpTestData();
        trainMockito();
    }

    private void setUpTestData() throws Exception {

        newRequest = TestdataCreditRequests.getNewRequest();
        savedRequest = TestdataCreditRequests.getSavedRequest();
        fullRequest = TestdataCreditRequests.getFullRequest();
        requests = new ArrayList<>();
        requests.add(savedRequest);
        requestsEmpty = new ArrayList<>();
        depositOneAccount = DepositAccountsData.ONE_STORED.getAccount();
        creditAccount = CreditAccountsData.ONE_STORED.getAccount();
        bankAccountsEmpty = new ArrayList<>();
        bankAccountsDeposit = new ArrayList<>();
        bankAccountsDeposit.add(depositOneAccount);
        bankAccountsDepositBig = new ArrayList<>();
        bankAccountsDepositBig.add(depositOneAccount);
        bankAccountsDepositBig.add(DepositAccountsData.TWO_STORED.getAccount());
        bankAccountsDepositCredit = new ArrayList<>();
        bankAccountsDepositCredit.add(depositOneAccount);
        bankAccountsDepositCredit.add(creditAccount);
    }

    private void trainMockito() throws Exception {

        when(mockCreditAccountRequestDAO.create(newRequest)).thenReturn(savedRequest.getId());
        when(mockBankAccountDAO.findById(depositOneAccount.getId())).thenReturn(depositOneAccount);
        when(mockBankAccountDAO.findById(creditAccount.getId())).thenReturn(creditAccount);
        when(spyAccountRequestService.buildRequest(newRequest.getClient(), newRequest.getAmount(), newRequest.getTerm()))
                .thenReturn(newRequest);
        when(mockBankAccountDAO.create(TestdataCreditRequests.getCreditForStandardApprove()))
                .thenReturn(TestdataCreditRequests.getCreditForStandardApproveStored().getId());
        when(mockBankAccountDAO.assignBankAccountToClient(
                TestdataCreditRequests.getCreditForStandardApproveStored().getId(),
                newRequest.getClient().getId())).thenReturn(true);
        when(mockBankAccountDAO.create(TestdataCreditRequests.getCreditForRequestWithBigApprovedAmount()))
                .thenReturn(TestdataCreditRequests.getCreditForRequestWithBigApprovedAmountStored().getId());
        when(mockBankAccountDAO.assignBankAccountToClient(
                TestdataCreditRequests.getCreditForRequestWithBigApprovedAmountStored().getId(),
                newRequest.getClient().getId())).thenReturn(true);
        when(mockBankAccountDAO.create(TestdataCreditRequests.getCreditForRequestWithLongApprovedValidity()))
                .thenReturn(TestdataCreditRequests.getCreditForRequestWithLongApprovedValidityStored().getId());
        when(mockBankAccountDAO.assignBankAccountToClient(
                TestdataCreditRequests.getCreditForRequestWithLongApprovedValidityStored().getId(),
                newRequest.getClient().getId())).thenReturn(true);
    }

    @Test
    public void makeRequestForClientWithoutBankAccounts() throws Exception {

        when(mockBankAccountDAO.findBankAccountsByClientId(newRequest.getClient().getId())).thenReturn(bankAccountsEmpty);

        CreditAccountRequest checkRequest = spyAccountRequestService.makeRequestForCreditAccount(newRequest.getClient(),
                newRequest.getAmount(), newRequest.getTerm());

        assertEquals(fullRequest, checkRequest);
    }

    @Test
    public void makeRequestForClientWithoutCreditAccount() throws Exception {

        when(mockBankAccountDAO.findBankAccountsByClientId(newRequest.getClient().getId())).thenReturn(bankAccountsDeposit);

        CreditAccountRequest checkRequest = spyAccountRequestService.makeRequestForCreditAccount(newRequest.getClient(),
                newRequest.getAmount(), newRequest.getTerm());

        assertEquals(fullRequest, checkRequest);
    }

    @Test (expected = CreditAccountExistException.class)
    public void makeRequestForClientWithCreditAccount() throws Exception {

        when(mockBankAccountDAO.findBankAccountsByClientId(newRequest.getClient().getId())).thenReturn(bankAccountsDepositCredit);
        spyAccountRequestService.makeRequestForCreditAccount(newRequest.getClient(),
                newRequest.getAmount(), newRequest.getTerm());
    }

    @Test
    public void getNewRequests() throws Exception {

        when(mockCreditAccountRequestDAO.findNewRequests()).thenReturn(requests);

        List<CreditAccountRequest> checkRequests = spyAccountRequestService.getNewRequests();

        assertEquals(requests, checkRequests);
    }

    @Test
    public void newRequestsNotFound() throws Exception {

        when(mockCreditAccountRequestDAO.findNewRequests()).thenReturn(requestsEmpty);

        List<CreditAccountRequest> checkRequests = spyAccountRequestService.getNewRequests();

        assertEquals(requestsEmpty, checkRequests);
    }

    @Test
    public void getAllRequests() throws Exception {

        when(mockCreditAccountRequestDAO.getAll()).thenReturn(requests);

        List<CreditAccountRequest> checkRequests = spyAccountRequestService.getAllRequests();

        assertEquals(requests, checkRequests);
    }

    @Test
    public void requestsNotFound() throws Exception {

        when(mockCreditAccountRequestDAO.getAll()).thenReturn(requestsEmpty);

        List<CreditAccountRequest> checkRequests = spyAccountRequestService.getAllRequests();

        assertEquals(requestsEmpty, checkRequests);
    }

    @Test
    public void approveRequestStandard() throws Exception {

        CreditAccountRequest request = TestdataCreditRequests.getRequestForStandardApprove();

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.getSavedClient().getId())).thenReturn(bankAccountsEmpty);

        spyAccountRequestService.approveRequestAndCreateCreditAccount(request);
    }

    @Test (expected = SystemFaultException.class)
    public void approveRequestStandardUpdateFault() throws Exception {

        CreditAccountRequest request = TestdataCreditRequests.getRequestForStandardApprove();

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.getSavedClient().getId())).thenReturn(bankAccountsEmpty);
        when(mockCreditAccountRequestDAO.update(request)).thenThrow(new DAOException("Update error!"));

        spyAccountRequestService.approveRequestAndCreateCreditAccount(request);
    }

    @Test (expected = CreditRequestIllegalException.class )
    public void approveRequestStandardBigAmount() throws Exception {

        CreditAccountRequest request = TestdataCreditRequests.getRequestWithBigApprovedAmount();

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.getSavedClient().getId())).thenReturn(bankAccountsEmpty);

        spyAccountRequestService.approveRequestAndCreateCreditAccount(request);
    }

    @Test (expected = CreditRequestIllegalException.class )
    public void approveRequestStandardLongValidity() throws Exception {

        CreditAccountRequest request = TestdataCreditRequests.getRequestWithLongApprovedValidity();

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.getSavedClient().getId())).thenReturn(bankAccountsEmpty);

        spyAccountRequestService.approveRequestAndCreateCreditAccount(request);
    }

    @Test
    public void approveRequestSmallDeposit() throws Exception {

        CreditAccountRequest request = TestdataCreditRequests.getRequestForStandardApprove();

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.getSavedClient().getId())).thenReturn(bankAccountsDeposit);

        spyAccountRequestService.approveRequestAndCreateCreditAccount(request);
    }

    @Test (expected = CreditRequestIllegalException.class )
    public void approveRequestSmallDepositBigAmount() throws Exception {

        CreditAccountRequest request = TestdataCreditRequests.getRequestWithBigApprovedAmount();

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.getSavedClient().getId())).thenReturn(bankAccountsDeposit);

        spyAccountRequestService.approveRequestAndCreateCreditAccount(request);
    }

    @Test (expected = CreditRequestIllegalException.class )
    public void approveRequestSmallDepositLongValidity() throws Exception {

        CreditAccountRequest request = TestdataCreditRequests.getRequestWithLongApprovedValidity();

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.getSavedClient().getId())).thenReturn(bankAccountsDeposit);

        spyAccountRequestService.approveRequestAndCreateCreditAccount(request);
    }

    @Test
    public void approveRequestBigDeposit() throws Exception {

        CreditAccountRequest request = TestdataCreditRequests.getRequestForStandardApprove();

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.getSavedClient().getId())).thenReturn(bankAccountsDepositBig);

        spyAccountRequestService.approveRequestAndCreateCreditAccount(request);
    }

    @Test
    public void approveRequestBigDepositBigAmount() throws Exception {

        CreditAccountRequest request = TestdataCreditRequests.getRequestWithBigApprovedAmount();

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.getSavedClient().getId())).thenReturn(bankAccountsDepositBig);

        spyAccountRequestService.approveRequestAndCreateCreditAccount(request);
    }

    @Test
    public void approveRequestBigDepositLongValidity() throws Exception {

        CreditAccountRequest request = TestdataCreditRequests.getRequestWithLongApprovedValidity();

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.getSavedClient().getId())).thenReturn(bankAccountsDepositBig);

        spyAccountRequestService.approveRequestAndCreateCreditAccount(request);
    }

    @Test (expected = CreditAccountExistException.class)
    public void approveRequestForClientWithCredit() throws Exception {

        CreditAccountRequest request = TestdataCreditRequests.getRequestForStandardApprove();

        when(mockBankAccountDAO.findBankAccountsByClientId(TestdataClient.getSavedClient().getId())).thenReturn(bankAccountsDepositCredit);

        spyAccountRequestService.approveRequestAndCreateCreditAccount(request);
    }

    @Test
    public void rejectRequestForCreditAccount() throws Exception {

        when(mockCreditAccountRequestDAO.findById(savedRequest.getId())).thenReturn(savedRequest);

        spyAccountRequestService.rejectRequestForCreditAccount(savedRequest);
        CreditAccountRequest checkRequest = mockCreditAccountRequestDAO.findById(savedRequest.getId());

        assertEquals(savedRequest, checkRequest);
    }
}