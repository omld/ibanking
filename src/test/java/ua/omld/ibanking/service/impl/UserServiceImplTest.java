/*
 * UserServiceImplTest.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.service.impl;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.omld.ibanking.domain.Client;
import ua.omld.ibanking.domain.User;
import ua.omld.ibanking.domain.testdata.*;
import ua.omld.ibanking.service.exception.IncorrectCredentialsException;
import ua.omld.ibanking.storage.connection.ConnectionWrapper;
import ua.omld.ibanking.storage.dao.ClientDAO;
import ua.omld.ibanking.storage.dao.DAOFactory;
import ua.omld.ibanking.storage.dao.UserDAO;
import ua.omld.ibanking.storage.exception.DAOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class UserServiceImplTest {

    private static DAOFactory mockDaoFactory;
    private static ConnectionWrapper mockConnectionWrapper;
    private static UserServiceImpl userService;
    private static UserDAO mockUserDAO;
    private static ClientDAO mockClientDAO;

    private User testUser;
    private Client testClient;
    private static String emailCauseDAOException;

    @BeforeClass
    public static void setUpClass() throws Exception {

        setUpMocks();
        trainMockitoForClass();
    }

    private static void setUpMocks() {

        mockDaoFactory = mock(DAOFactory.class);
        mockConnectionWrapper = mock(ConnectionWrapper.class);
        userService = new UserServiceImpl(mockDaoFactory);
        mockUserDAO = mock(UserDAO.class);
        mockClientDAO = mock(ClientDAO.class);
    }

    private static void trainMockitoForClass() throws Exception {

        when(mockDaoFactory.getConnection()).thenReturn(mockConnectionWrapper);
        when(mockDaoFactory.createUserDAO(mockConnectionWrapper)).thenReturn(mockUserDAO);
        when(mockDaoFactory.createClientDAO(mockConnectionWrapper)).thenReturn(mockClientDAO);
    }

    @Before
    public void setUpAndTrainMockito() throws Exception {

        setUpTestData();
        trainMockito();
    }

    private void setUpTestData() throws Exception {

        testUser = TestdataUser.getStoredUser();
        testClient = TestdataClient.getSavedClient();
        emailCauseDAOException = "dao@exception.com";
    }

    private void trainMockito() throws Exception {

        when(mockUserDAO.findByEmail(testUser.getEmail())).thenReturn(testUser);
        when(mockClientDAO.findByUserId(testUser.getId())).thenReturn(testClient);
        doThrow(new DAOException("Database error")).when(mockUserDAO).findByEmail(emailCauseDAOException);
    }

    @Test
    public void successfulUserLogin() throws Exception {

        User checkUser = userService.loginUser(testUser.getEmail(), TestdataUser.USER_PASSWORD);
        assertEquals(testUser, checkUser);
    }

    @Test (expected = IncorrectCredentialsException.class)
    public void incorrectLogin() throws Exception {

        userService.loginUser(testUser.getEmail(), "wrong_password");
    }

    @Test (expected = IncorrectCredentialsException.class)
    public void testExceptionFromDAO() throws Exception {

        userService.loginUser(emailCauseDAOException, "any_password");
    }

    @AfterClass
    public static void checkMockito() throws Exception {

        verify(mockDaoFactory, times(3)).getConnection();
        verify(mockDaoFactory, times(3)).createUserDAO(mockConnectionWrapper);
        verify(mockUserDAO, times(2)).findByEmail(TestdataUser.getStoredUser().getEmail());
        verify(mockClientDAO, times(1)).findByUserId(TestdataUser.getStoredUser().getId());
        verify(mockConnectionWrapper, never()).startTransaction();
        verify(mockConnectionWrapper, never()).commitTransaction();
        verify(mockConnectionWrapper, times(3)).close();
    }
}