/*
 * CreditAccountTest.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Formatter;

import static org.junit.Assert.*;

public class CreditAccountTest {

    private static final Long ID = 200L;
    private static final BigDecimal BALANCE = BigDecimal.valueOf(5000.0);
    private static final BigDecimal LIMIT = BigDecimal.valueOf(8000.0);
    private static final BigDecimal RATE = BigDecimal.valueOf(15.0);
    private static final BigDecimal INTEREST_AMOUNT = BigDecimal.valueOf(200.0);
    private static final BigDecimal DEBT = BigDecimal.valueOf(3000.0);
    private static final LocalDate VALIDITY = LocalDate.of(2020, 9, 1);

    @Test
    public void testCreditAccountBuilder() {
        CreditAccount creditAccount = CreditAccount.getBuilder()
                .setId(ID)
                .setBalance(BALANCE)
                .setCreditLimit(LIMIT)
                .setInterestRate(RATE)
                .setValidityDate(VALIDITY)
                .setAmountOfAccruedInterest(INTEREST_AMOUNT)
                .setCurrentDebt(DEBT)
                .build();

        Formatter formatter = new Formatter();
        formatter.format("%010d",ID);

        assertEquals(ID, creditAccount.getId());
        assertEquals(BALANCE, creditAccount.getBalance());
        assertEquals(LIMIT, creditAccount.getCreditLimit());
        assertEquals(RATE, creditAccount.getInterestRate());
        assertEquals(INTEREST_AMOUNT, creditAccount.getAmountOfAccruedInterest());
        assertEquals(DEBT, creditAccount.getCurrentDebt());
        assertEquals(VALIDITY, creditAccount.getValidityDate());
        assertEquals(formatter.toString(), creditAccount.getNumber());
    }
}