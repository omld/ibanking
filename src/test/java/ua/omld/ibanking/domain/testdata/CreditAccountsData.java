/*
 * CreditAccountsData.java
 * Copyright (c) 2019 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain.testdata;

import ua.omld.ibanking.domain.CreditAccount;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Test data for deposit accounts
 */
public enum CreditAccountsData {

    ONE_STORED(2L, new BigDecimal(5000.0), new BigDecimal(10.0), LocalDate.of(2019, 10, 5),
            new BigDecimal(8000.0), new BigDecimal(3000.0), new BigDecimal(300.0)),
    ONE_NEW(new BigDecimal(8000.0), new BigDecimal(10.0), LocalDate.of(2019, 10, 5),
            new BigDecimal(8000.0)),
    ;

    private final CreditAccount account;

    CreditAccountsData(Long id, BigDecimal balance, BigDecimal interestRate, LocalDate validityDate,
                       BigDecimal creditLimit, BigDecimal currentDebt, BigDecimal accuredInterest) {

        this.account = CreditAccount.getBuilder().setId(id).setBalance(balance).setInterestRate(interestRate)
                .setValidityDate(validityDate).setCreditLimit(creditLimit).setCurrentDebt(currentDebt)
                .setAmountOfAccruedInterest(accuredInterest).build();
    }

    CreditAccountsData(BigDecimal balance, BigDecimal interestRate, LocalDate validityDate, BigDecimal creditLimit ) {

        this.account = CreditAccount.getBuilder().setBalance(balance).setInterestRate(interestRate)
                .setValidityDate(validityDate).setCreditLimit(creditLimit).build();
    }

    public CreditAccount getAccount() {

        return account;
    }
}
