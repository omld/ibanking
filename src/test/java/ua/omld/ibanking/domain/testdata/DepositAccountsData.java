/*
 * DepositAccountsData.java
 * Copyright (c) 2019 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain.testdata;

import ua.omld.ibanking.domain.DepositAccount;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Test data for deposit accounts
 */
public enum DepositAccountsData {

    ONE_STORED(1L, new BigDecimal(2000.0), new BigDecimal(10.0), LocalDate.of(2020, 12, 15)),
    ONE_NEW(new BigDecimal(2000.0), new BigDecimal(10.0), LocalDate.of(2020, 12, 15)),
    TWO_STORED(102L, new BigDecimal(3000.0), new BigDecimal(11.0), LocalDate.of(2021, 10, 2)),
    ;

    private final DepositAccount account;

    DepositAccountsData(Long id, BigDecimal balance, BigDecimal interestRate, LocalDate validityDate) {

        this.account = DepositAccount.getBuilder().setId(id).setBalance(balance).setInterestRate(interestRate)
                .setValidityDate(validityDate).build();
    }

    DepositAccountsData(BigDecimal balance, BigDecimal interestRate, LocalDate validityDate) {

        this.account = DepositAccount.getBuilder().setBalance(balance).setInterestRate(interestRate)
                .setValidityDate(validityDate).build();
    }

    public DepositAccount getAccount() {

        return account;
    }
}
