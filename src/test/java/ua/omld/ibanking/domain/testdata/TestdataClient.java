/*
 * TestClient.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain.testdata;

import java.time.LocalDate;

public interface TestdataClient {

    /**
     * Info for Client
     */
    Long LONG = 7L;
    String NAME = "Orest";
    String SURNAME = "Subtelnyy";
    String PATRONYMIC = "Yuriyovych";
    String TAX_NUMBER = "0123456789";
    LocalDate BIRTH_DATE = LocalDate.of(1988, 2, 12);

    static ua.omld.ibanking.domain.Client getSavedClient() {
        return ua.omld.ibanking.domain.Client.getBuilder()
                .setId(LONG)
                .setName(NAME)
                .setSurname(SURNAME)
                .setPatronymic(PATRONYMIC)
                .setBirthDate(BIRTH_DATE)
                .setTaxNumber(TAX_NUMBER)
                .build();
    }
}

