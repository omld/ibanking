/*
 * TestUser.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain.testdata;


import ua.omld.ibanking.domain.Role;
import ua.omld.ibanking.domain.User;
import ua.omld.ibanking.service.utils.PasswordUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface TestdataUser {

    /**
     * Info for User
     */
    Long USER_ID = 5L;
    String USER_EMAIL = "user@example.com";
    String USER_PASSWORD = "simple";
    LocalDate USER_REGISTERED = LocalDate.of(2018, 7, 30);
    LocalDateTime USER_PASSWORD_CHANGED = LocalDateTime.of(2018, 8, 3, 0, 0);
    Role USER_ROLE = Role.CLIENT;
    ua.omld.ibanking.domain.Client USER_CLIENT = TestdataClient.getSavedClient();

    /**
     * Info for fake User
     */
    Long FAKE_USER_ID = 2000L;

    /**
     * Info for Admin
     */
    Long ADMIN_ID = 1L;
    String ADMIN_EMAIL = "admin@ibank.ua";
    String ADMIN_PASSWORD = "Admin1";
    String ADMIN_STORED_PASSWORD = "170b7bb8e75d57d05d95c9552f7c90fca462d36777eb0f358223d420c02da958";
    LocalDate ADMIN_REGISTERED = LocalDate.of(2018, 8, 13);
    LocalDateTime ADMIN_PASSWORD_CHANGED = LocalDateTime.of(2018, 8, 13, 12, 0, 0);
    Role ADMIN_ROLE = Role.ADMIN;

    static User getNewUser() throws Exception {
        String encryptedPassword = PasswordUtils.hashPassword(USER_PASSWORD, USER_EMAIL, USER_PASSWORD_CHANGED);

        return User.getBuilder()
                .setEmail(USER_EMAIL)
                .setPassword(encryptedPassword)
                .setRegistrationDate(USER_REGISTERED)
                .setPasswordChangeDateTime(USER_PASSWORD_CHANGED)
                .setRole(USER_ROLE)
                .setClient(USER_CLIENT)
                .build();
    }

    static User getStoredUser() throws Exception {
        String encryptedPassword = PasswordUtils.hashPassword(USER_PASSWORD, USER_EMAIL, USER_PASSWORD_CHANGED);

        return User.getBuilder()
                .setId(USER_ID)
                .setEmail(USER_EMAIL)
                .setPassword(encryptedPassword)
                .setRegistrationDate(USER_REGISTERED)
                .setPasswordChangeDateTime(USER_PASSWORD_CHANGED)
                .setRole(USER_ROLE)
                .setClient(USER_CLIENT)
                .build();
    }

    static User getFakeUser () {
        return User.getBuilder()
                .setId(FAKE_USER_ID)
                .setEmail(USER_EMAIL)
                .setPassword("")
                .setRegistrationDate(USER_REGISTERED)
                .setPasswordChangeDateTime(USER_PASSWORD_CHANGED)
                .setRole(USER_ROLE)
                .build();
    }

    static User getStoredAdmin() throws Exception {

        return User.getBuilder()
                .setId(ADMIN_ID)
                .setEmail(ADMIN_EMAIL)
                .setPassword(ADMIN_STORED_PASSWORD)
                .setRegistrationDate(ADMIN_REGISTERED)
                .setPasswordChangeDateTime(ADMIN_PASSWORD_CHANGED)
                .setRole(ADMIN_ROLE)
                .build();
    }

}
