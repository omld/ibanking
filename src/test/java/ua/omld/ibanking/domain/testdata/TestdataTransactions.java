/*
 * TestdataTransactions.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain.testdata;

import ua.omld.ibanking.domain.*;
import ua.omld.ibanking.utils.ConfigurationManager;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public interface TestdataTransactions {

    Long TRANS_2_ID = 27L;
    LocalDateTime TRANS_2_CREATE_DATE_TIME = LocalDateTime.of(2018, 8, 9, 12, 0);
    LocalDateTime TRANS_2_EXECUTION_DATE_TIME = LocalDateTime.of(2018, 8, 9, 12, 5);

    Long TRANS_3_ID = 32L;
    LocalDateTime TRANS_3_CREATE_DATE_TIME = LocalDateTime.of(2018, 8, 10, 12, 0);
    LocalDateTime TRANS_3_EXECUTION_DATE_TIME = LocalDateTime.of(2018, 8, 10, 12, 5);

    Long TRANS_4_ID = 33L;
    LocalDateTime TRANS_4_CREATE_DATE_TIME = LocalDateTime.of(2018, 8, 12, 10, 0);
    LocalDateTime TRANS_4_EXECUTION_DATE_TIME = LocalDateTime.of(2018, 8, 12, 10, 5);

    Long TRANS_5_ID = 34L;
    LocalDateTime TRANS_5_CREATE_DATE_TIME = LocalDateTime.of(2018, 8, 13, 11, 0);
    LocalDateTime TRANS_5_EXECUTION_DATE_TIME = LocalDateTime.of(2018, 8, 13, 12, 30);

    Long TRANS_6_ID = 71L;
    LocalDateTime TRANS_6_CREATE_DATE_TIME = LocalDateTime.of(2018, 8, 14, 12, 0);
    LocalDateTime TRANS_6_EXECUTION_DATE_TIME = LocalDateTime.of(2018, 8, 14, 12, 5);

    String PAY_TO_PERSON_NAME = "Home Electronics, ltd";
    String PAY_TO_PERSON_CODE = "00234587";
    String PAY_TO_PERSON_ACCOUNT = "260510467858";
    String PAY_TO_PERSON_BANK_CODE = "332453";

    BigDecimal TRANS_REPLENISH_AMOUNT = new BigDecimal(2000.0);
    BigDecimal TRANS_MOVE_AMOUNT = new BigDecimal(1000.0);
    BigDecimal TRANS_PAY_AMOUNT = new BigDecimal(2480.0);
    BigDecimal TRANS_WITHDRAWAL_AMOUNT = new BigDecimal(1500.0);

    BigDecimal TRANS_INNER_FEE = new BigDecimal(0.0);
    BigDecimal TRANS_PAY_FEE = new BigDecimal(5.0);
    BigDecimal TRANS_WITHDRAWAL_FEE = new BigDecimal(15.0);

    String REPLENISH_DESCRIPTION = "Replenish account with cash.";
    String PAY_DESCRIPTION = "Pay for electronics according to invoice #20 (2018.08.09).";
    String WITHDRAWAL_DESCRIPTION = "Withdraw cash from account.";
    String MOVE_BETWEEN_ACCOUNTS_DESCRIPTION = "Money transfer between own accounts";


    static TransactionPerson senderForReplenish() throws Exception {

        TransactionPerson person = new TransactionPerson();
        person.setName(TestdataClient.getSavedClient().fullName());
        person.setPersonCode(TestdataClient.getSavedClient().getTaxNumber());
        person.setBankAccountNumber(null);
        person.setBankCode(ConfigurationManager.getInstance().getBankCode());

        return person;
    }

    static TransactionPerson beneficiaryForWithdrawal() throws Exception {
        return senderForReplenish();
    }

    static TransactionPerson getInnerBeneficiary() throws Exception {

        TransactionPerson person = new TransactionPerson();
        person.setName(TestdataClient.getSavedClient().fullName());
        person.setPersonCode(TestdataClient.getSavedClient().getTaxNumber());
        person.setBankCode(ConfigurationManager.getInstance().getBankCode());

        return person;
    }

    static TransactionPerson ownerForDepositTwo() throws Exception {

        TransactionPerson person = getInnerBeneficiary();
        person.setBankAccountNumber(DepositAccountsData.TWO_STORED.getAccount().getNumber());
        return person;
    }

    static TransactionPerson ownerForCredit() throws Exception {

        TransactionPerson person = getInnerBeneficiary();
        person.setBankAccountNumber(CreditAccountsData.ONE_STORED.getAccount().getNumber());
        return person;
    }

    static TransactionPerson getOuterBeneficiary() throws Exception {

        TransactionPerson person = new TransactionPerson();
        person.setName(PAY_TO_PERSON_NAME);
        person.setPersonCode(PAY_TO_PERSON_CODE);
        person.setBankCode(PAY_TO_PERSON_BANK_CODE);
        person.setBankAccountNumber(PAY_TO_PERSON_ACCOUNT);

        return person;
    }

    static Invoice getInvoice() throws Exception {

        Invoice invoice = new Invoice();
        invoice.setDescription(PAY_DESCRIPTION);
        invoice.setAmount(TRANS_PAY_AMOUNT);
        invoice.setBeneficiary(getOuterBeneficiary());

        return invoice;
    }

    static Invoice getBigInvoice() throws Exception {

        Invoice bigInvoice = new Invoice();
        bigInvoice.setDescription(PAY_DESCRIPTION);
        bigInvoice.setAmount(new BigDecimal(50000.0));
        bigInvoice.setBeneficiary(getOuterBeneficiary());

        return bigInvoice;
    }

    static List<Transaction> getTransactionsForDepositTwo() throws Exception {

        List<Transaction> transactions = new ArrayList<>();
        // replenish
        transactions.add(Transaction.getBuilder()
                .setId(TRANS_3_ID)
                .setCreationDateTime(TRANS_3_CREATE_DATE_TIME)
                .setExecutionDateTime(TRANS_3_EXECUTION_DATE_TIME)
                .setAmount(TRANS_REPLENISH_AMOUNT)
                .setFee(TRANS_INNER_FEE)
                .setSender(senderForReplenish())
                .setBeneficiary(ownerForDepositTwo())
                .setStatus(TransactionStatus.COMPLETED)
                .setType(TransactionType.REPLENISH)
                .setDescription(REPLENISH_DESCRIPTION)
                .build()
        );
        // replenish
        transactions.add(Transaction.getBuilder()
                .setId(TRANS_4_ID)
                .setCreationDateTime(TRANS_4_CREATE_DATE_TIME)
                .setExecutionDateTime(TRANS_4_EXECUTION_DATE_TIME)
                .setAmount(TRANS_REPLENISH_AMOUNT)
                .setFee(TRANS_INNER_FEE)
                .setSender(senderForReplenish())
                .setBeneficiary(ownerForDepositTwo())
                .setStatus(TransactionStatus.COMPLETED)
                .setType(TransactionType.REPLENISH)
                .setDescription(REPLENISH_DESCRIPTION)
                .build()
        );
        // transfer to credit
        transactions.add(Transaction.getBuilder()
                .setId(TRANS_5_ID)
                .setCreationDateTime(TRANS_5_CREATE_DATE_TIME)
                .setExecutionDateTime(TRANS_5_EXECUTION_DATE_TIME)
                .setAmount(TRANS_MOVE_AMOUNT)
                .setFee(TRANS_INNER_FEE)
                .setSender(ownerForDepositTwo())
                .setBeneficiary(ownerForCredit())
                .setStatus(TransactionStatus.COMPLETED)
                .setType(TransactionType.TRANSFER)
                .setDescription(MOVE_BETWEEN_ACCOUNTS_DESCRIPTION)
                .build()
        );

        return transactions;
    }


    static List<Transaction> getTransactionsForCredit() throws Exception {

        List<Transaction> transactions = new ArrayList<>();
        // pay
        transactions.add(Transaction.getBuilder()
                .setId(TRANS_2_ID)
                .setCreationDateTime(TRANS_2_CREATE_DATE_TIME)
                .setExecutionDateTime(TRANS_2_EXECUTION_DATE_TIME)
                .setAmount(TRANS_PAY_AMOUNT)
                .setFee(TRANS_PAY_FEE)
                .setSender(ownerForCredit())
                .setBeneficiary(getOuterBeneficiary())
                .setStatus(TransactionStatus.COMPLETED)
                .setType(TransactionType.PAYMENT)
                .setDescription(PAY_DESCRIPTION)
                .build()
        );
        // transfer from deposit 2
        transactions.add(Transaction.getBuilder()
                .setId(TRANS_5_ID)
                .setCreationDateTime(TRANS_5_CREATE_DATE_TIME)
                .setExecutionDateTime(TRANS_5_EXECUTION_DATE_TIME)
                .setAmount(TRANS_MOVE_AMOUNT)
                .setFee(TRANS_INNER_FEE)
                .setSender(ownerForDepositTwo())
                .setBeneficiary(ownerForCredit())
                .setStatus(TransactionStatus.COMPLETED)
                .setType(TransactionType.TRANSFER)
                .setDescription(MOVE_BETWEEN_ACCOUNTS_DESCRIPTION)
                .build()
        );
        // withdrawal
        transactions.add(Transaction.getBuilder()
                .setId(TRANS_6_ID)
                .setCreationDateTime(TRANS_6_CREATE_DATE_TIME)
                .setExecutionDateTime(TRANS_6_EXECUTION_DATE_TIME)
                .setAmount(TRANS_WITHDRAWAL_AMOUNT)
                .setFee(TRANS_WITHDRAWAL_FEE)
                .setSender(ownerForCredit())
                .setBeneficiary(beneficiaryForWithdrawal())
                .setStatus(TransactionStatus.COMPLETED)
                .setType(TransactionType.WITHDRAWAL)
                .setDescription(WITHDRAWAL_DESCRIPTION)
                .build()
        );

        return transactions;
    }

}
