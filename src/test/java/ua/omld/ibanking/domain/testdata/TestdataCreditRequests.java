/*
 * TestdataCreditRequests.java
 * Copyright (c) 2018 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain.testdata;

import ua.omld.ibanking.domain.Client;
import ua.omld.ibanking.domain.CreditAccount;
import ua.omld.ibanking.domain.CreditAccountRequest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

public interface TestdataCreditRequests {

    /**
     * Info for CreditAccountRequest
     */
    Long ID = 210L;
    Client CLIENT = TestdataClient.getSavedClient();
    BigDecimal AMOUNT = new BigDecimal(10000.0);
    LocalDateTime CREATION_TIME = LocalDateTime.of(2018, 8, 5, 12,30);
    Period TERM = Period.ofYears(3);

    BigDecimal STANDARD_APPROVED_AMOUNT = new BigDecimal(5000.0);
    LocalDate STANDARD_APPROVED_VALIDITY = LocalDate.of(2020, 8, 1);

    BigDecimal BIG_APPROVED_AMOUNT = new BigDecimal(12000.0);
    LocalDate LONG_APPROVED_VALIDITY = LocalDate.of(2021, 8, 1);

    BigDecimal CURRENT_INTEREST_RATE = new BigDecimal(18.0);

    static CreditAccountRequest getNewRequest() throws Exception {
        CreditAccountRequest newRequest = new CreditAccountRequest();
        newRequest.setClient(CLIENT);
        newRequest.setCreationDateTime(CREATION_TIME);
        newRequest.setAmount(AMOUNT);
        newRequest.setTerm(TERM);
        return newRequest;
    }

    static CreditAccountRequest getSavedRequest() throws Exception {
        CreditAccountRequest savedRequest = new CreditAccountRequest();
        savedRequest.setId(ID);
        savedRequest.setCreationDateTime(CREATION_TIME);
        savedRequest.setAmount(AMOUNT);
        savedRequest.setTerm(TERM);
        return savedRequest;
    }

    static CreditAccountRequest getFullRequest() throws Exception {
        CreditAccountRequest savedRequest = new CreditAccountRequest();
        savedRequest.setId(ID);
        savedRequest.setClient(CLIENT);
        savedRequest.setCreationDateTime(CREATION_TIME);
        savedRequest.setAmount(AMOUNT);
        savedRequest.setTerm(TERM);
        return savedRequest;
    }

    static CreditAccountRequest getRequestForStandardApprove() throws Exception {
        return getRequestForApprove(STANDARD_APPROVED_AMOUNT, STANDARD_APPROVED_VALIDITY);
    }

    static CreditAccountRequest getRequestWithBigApprovedAmount() throws Exception {
        return getRequestForApprove(BIG_APPROVED_AMOUNT, STANDARD_APPROVED_VALIDITY);
    }

    static CreditAccountRequest getRequestWithLongApprovedValidity() throws Exception {
        return getRequestForApprove(STANDARD_APPROVED_AMOUNT, LONG_APPROVED_VALIDITY);
    }

    static CreditAccountRequest getRequestForApprove(BigDecimal amount, LocalDate validity) throws Exception {
        CreditAccountRequest forApproveRequest = new CreditAccountRequest();
        forApproveRequest.setId(ID);
        forApproveRequest.setClient(CLIENT);
        forApproveRequest.setCreationDateTime(CREATION_TIME);
        forApproveRequest.setAmount(AMOUNT);
        forApproveRequest.setTerm(TERM);
        forApproveRequest.setApprovedAmount(amount);
        forApproveRequest.setApprovedValidity(validity);
        return forApproveRequest;
    }

    static CreditAccount getCreditForStandardApprove() throws Exception {
        return CreditAccount.getBuilder()
                .setInterestRate(CURRENT_INTEREST_RATE)
                .setValidityDate(STANDARD_APPROVED_VALIDITY)
                .setBalance(STANDARD_APPROVED_AMOUNT)
                .setCreditLimit(STANDARD_APPROVED_AMOUNT)
                .setCurrentDebt(BigDecimal.ZERO)
                .setAmountOfAccruedInterest(BigDecimal.ZERO)
                .build();
    }

    static CreditAccount getCreditForStandardApproveStored() throws Exception {
        return CreditAccount.getBuilder()
                .setId(1000L)
                .setInterestRate(CURRENT_INTEREST_RATE)
                .setValidityDate(STANDARD_APPROVED_VALIDITY)
                .setBalance(STANDARD_APPROVED_AMOUNT)
                .setCreditLimit(STANDARD_APPROVED_AMOUNT)
                .setCurrentDebt(BigDecimal.ZERO)
                .setAmountOfAccruedInterest(BigDecimal.ZERO)
                .build();
    }

    static CreditAccount getCreditForRequestWithBigApprovedAmount() throws Exception {
        return CreditAccount.getBuilder()
                .setInterestRate(CURRENT_INTEREST_RATE)
                .setValidityDate(STANDARD_APPROVED_VALIDITY)
                .setBalance(BIG_APPROVED_AMOUNT)
                .setCreditLimit(BIG_APPROVED_AMOUNT)
                .setCurrentDebt(BigDecimal.ZERO)
                .setAmountOfAccruedInterest(BigDecimal.ZERO)
                .build();
    }

    static CreditAccount getCreditForRequestWithBigApprovedAmountStored() throws Exception {
        return CreditAccount.getBuilder()
                .setId(1001L)
                .setInterestRate(CURRENT_INTEREST_RATE)
                .setValidityDate(STANDARD_APPROVED_VALIDITY)
                .setBalance(BIG_APPROVED_AMOUNT)
                .setCreditLimit(BIG_APPROVED_AMOUNT)
                .setCurrentDebt(BigDecimal.ZERO)
                .setAmountOfAccruedInterest(BigDecimal.ZERO)
                .build();
    }

    static CreditAccount getCreditForRequestWithLongApprovedValidity() throws Exception {
        return CreditAccount.getBuilder()
                .setInterestRate(CURRENT_INTEREST_RATE)
                .setValidityDate(LONG_APPROVED_VALIDITY)
                .setBalance(STANDARD_APPROVED_AMOUNT)
                .setCreditLimit(STANDARD_APPROVED_AMOUNT)
                .setCurrentDebt(BigDecimal.ZERO)
                .setAmountOfAccruedInterest(BigDecimal.ZERO)
                .build();
    }

    static CreditAccount getCreditForRequestWithLongApprovedValidityStored() throws Exception {
        return CreditAccount.getBuilder()
                .setId(1002L)
                .setInterestRate(CURRENT_INTEREST_RATE)
                .setValidityDate(LONG_APPROVED_VALIDITY)
                .setBalance(STANDARD_APPROVED_AMOUNT)
                .setCreditLimit(STANDARD_APPROVED_AMOUNT)
                .setCurrentDebt(BigDecimal.ZERO)
                .setAmountOfAccruedInterest(BigDecimal.ZERO)
                .build();
    }

}
