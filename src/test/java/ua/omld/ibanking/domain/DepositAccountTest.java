/*
 * DepositAccountTest.java
 * Copyright (c) 2019 Oleksii Kostetskyi.
 *
 * This software is licensed under GPL v2
 */

package ua.omld.ibanking.domain;

import org.junit.Test;

import static org.junit.Assert.*;
import static ua.omld.ibanking.domain.testdata.DepositAccountsData.ONE_STORED;
import static ua.omld.ibanking.domain.testdata.DepositAccountsData.TWO_STORED;

public class DepositAccountTest {

    @Test
    public void getBuilder_multipleUse() {

        DepositAccount.DepositAccountBuilder builder = DepositAccount.getBuilder();

        DepositAccount depositAccount1 = builder.setId(ONE_STORED.getAccount().getId())
                .setBalance(ONE_STORED.getAccount().getBalance())
                .setInterestRate(ONE_STORED.getAccount().getInterestRate())
                .setValidityDate(ONE_STORED.getAccount().getValidityDate()).build();

        DepositAccount depositAccount2 = builder.setId(TWO_STORED.getAccount().getId())
                .setBalance(TWO_STORED.getAccount().getBalance())
                .setInterestRate(TWO_STORED.getAccount().getInterestRate())
                .setValidityDate(TWO_STORED.getAccount().getValidityDate()).build();

        assertEquals(ONE_STORED.getAccount(), depositAccount1);
        assertEquals(TWO_STORED.getAccount(), depositAccount2);
    }
}