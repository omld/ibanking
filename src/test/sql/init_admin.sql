USE ibanking_test;

/* Administrator user:
 *    e-mail - 'admin@ibank.ua'
 *    password - 'Admin1'
 *    password change datetime - '2018-08-13 12:00:00'
 */
INSERT INTO `Users` (`EMAIL`, `PASSWORD`, `REG_DATE`, `PASS_CHANGE_DATETIME`, `ROLE`) VALUES
  ('admin@ibank.ua', '170b7bb8e75d57d05d95c9552f7c90fca462d36777eb0f358223d420c02da958',
   '2018-08-13', '2018-08-13 12:00:00', 'ADMIN')
;